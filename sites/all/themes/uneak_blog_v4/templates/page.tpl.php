<?php
/**
 * @file
 * Adaptivetheme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * Adaptivetheme supplied variables:
 * - $site_logo: Themed logo - linked to front with alt attribute.
 * - $site_name: Site name linked to the homepage.
 * - $site_name_unlinked: Site name without any link.
 * - $hide_site_name: Toggles the visibility of the site name.
 * - $visibility: Holds the class .element-invisible or is empty.
 * - $primary_navigation: Themed Main menu.
 * - $secondary_navigation: Themed Secondary/user menu.
 * - $primary_local_tasks: Split local tasks - primary.
 * - $secondary_local_tasks: Split local tasks - secondary.
 * - $tag: Prints the wrapper element for the main content.
 * - $is_mobile: Bool, requires the Browscap module to return TRUE for mobile
 *   devices. Use to test for a mobile context.
 * - *_attributes: attributes for various site elements, usually holds id, class
 *   or role attributes.
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Core Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * Adaptivetheme Regions:
 * - $page['leaderboard']: full width at the very top of the page
 * - $page['menu_bar']: menu blocks placed here will be styled horizontal
 * - $page['secondary_content']: full width just above the main columns
 * - $page['content_aside']: like a main content bottom region
 * - $page['tertiary_content']: full width just above the footer
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see adaptivetheme_preprocess_page()
 * @see adaptivetheme_process_page()
 */
 
 
 
 
 
 
 
 
global $default_layers, $main_content;
$system_main = $page['content']['system_main'];


//print_r($system_main);

//
//	INIT VARS
//
if (isset($node)) {
	$node_content = $system_main['nodes'][$node->nid];

} else if (isset($system_main['nodes'])) {
	$node_content = reset($system_main['nodes']);
	$first_field = reset($node_content);
	$node = $first_field['#object'];
	hide($page['content']['system_main']['nodes'][$node->nid]);
		
} else if (isset($system_main['#theme']) && $system_main['#theme'] == "user_profile") {
	$user = $system_main['#account'];
	$user_content = $system_main;

} else if (isset($system_main['profile2'])) {
	$profile_content = reset($system_main['profile2']);
	$first_field = reset($profile_content);
	$profile = $first_field['#object'];
	
	$user = user_load($profile->uid);
}



//
//

$en_baseline = "";
$en_created = "";
$en_title = "";
$en_body = "";


if (isset($node)) {
	
	if (isset($node->field_layer)) $default_layers->merge(new Layers($node->field_layer));
	$main_content->layers = $default_layers;
	if (isset($node->field_color['und'])) $main_content->color = $node->field_color['und'][0]['jquery_colorpicker'];

	// $en_cover
	if (isset($node->field_image['und'][0]['uri'])) $en_cover = file_create_url($node->field_image['und'][0]['uri']);
	
	//	$en_body
	if (isset($node_content['body'])) $en_body .= render($node_content['body']);
	
	// $en_icon, en_title, en_body
	if ($node->nid == 124) {
		$en_icon = render($system_main['term_entete']['en_icon']);
		$en_title = render($system_main['term_entete']['taxo_breadcrumb']);
		$en_body .= render($system_main['term_entete']['en_desc']);
		
		hide($page['content']['system_main']['term_entete']);

	} else {
		$en_title = $node->title;
		
		if ($node->type != "article") {
			if (isset($node->field_client['und'])) {
				foreach ($node->field_client['und'] as $field_client) {
					$term = taxonomy_term_load($field_client["tid"]);
					$en_icon = render(taxonomy_term_view($term, 'image'));
				}
			}
		
		} else {
			$en_icon = get_rollover_image_user(user_load($node->uid));
		}
	}
	//
	
	//	en_links
	if (isset($node_content['field_link'])) $en_links = render($node_content['field_link']);
	
	//	en_baseline
	if (isset($node_content['field_type'])) $en_baseline .= render($node_content['field_type']);
	if (isset($node_content['field_level'])) $en_baseline .= render($node_content['field_level']);
	
	//	en_created
	if (isset($node->created) && $node->type == "article") $en_created = format_date($node->created, 'long');
	
	//	en_social
	if (count($node_content['links']['node']['#links'])) $en_social = render($node_content['links']['node']);
            
            
					
} else if (isset($user)) {
	
	$en_icon = get_rollover_image_user($user);
	if (isset($user->field_image['und'][0]['uri'])) $en_cover = file_create_url($user->field_image['und'][0]['uri']);
	
	if (isset($user->field_nom['und'][0]['safe_value'])) $en_title = render($user->field_nom['und'][0]['safe_value']);
	if (isset($user->name)) $en_body = '[ '.$user->name.' ]';
	
	$en_social = profile_menu($user->uid, (isset($user_content)) ? true : false);
	
}






 
 
?>
<div id="page" class="<?php print $classes; ?>">
      
    <div id="background-sky-container">
		<?php print $main_content->buildLayers(); ?>
         
        <div id="page-header">
            <div id="page-header-wrapper">
                <div id="block-header">
                    
                    <?php print render($page['leaderboard']); ?>
            
                    <header id="header" class="layout-block clearfix" role="banner">
                        <div id="header-wrapper">
                            <?php if ($site_logo || $site_name || $site_slogan): ?>
                                <div id="branding" class="branding-elements">
                                    <?php if ($site_logo): ?><div id="logo"><?php print $site_logo; ?></div><?php endif; ?>
                                    <?php if ($site_name || $site_slogan): ?>
                                        <hgroup id="name-and-slogan"<?php print $hgroup_attributes; ?>>
                                            <?php
                                                if ($site_name):
                                                $site_frontpage = variable_get('site_frontpage', url());
                                             ?>
                                                <a href="/<?php print $site_frontpage; ?>"><h1 <?php print $site_name_attributes; ?>><span class="text-format-h3">Uneak</span><?php print variable_get('site_name'); ?></h1></a>
                                            <?php endif; ?>
                                            
                                            <?php if ($site_slogan): ?><h2 <?php print $site_slogan_attributes; ?>><?php print $site_slogan; ?></h2><?php endif; ?>
                                        </hgroup>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <?php print render($page['header']); ?>
                        </div>
                    </header>
                    
                    <div id="navigation" class="layout-block">
                        <div id="navigation-wrapper">
                            <div id="top-menu">
                                <div id="top-menu-wrapper">
                                    <?php print render($page['menu_bar']); ?>
                                    <?php if ($primary_navigation): print $primary_navigation; endif; ?>
                                    <?php if ($secondary_navigation): print $secondary_navigation; endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
        
        
        <?php
			if (isset($node) || isset($user)) {
		?>
            <div class='node-row-1 clearfix'>
                <div class='node-cover'>
                    <div id="cover-wrapper">
                        <div id="cover-image"><?php if (isset($en_cover)) { ?><div id="cover-div-background" class="image-cover" style="background-image:url('<?php print $en_cover; ?>');">&nbsp;</div><?php } ?></div>
                    </div>
                    <div class='page-breadcrumbs'>
                        <div id="breadcrumb-container">
                            <?php if ($breadcrumb): print $breadcrumb; endif; ?>
                        </div>
                    </div>
                </div>
                <div class='node-more'>
                    <div class='node-client'><?php if (isset($en_icon)) print $en_icon; ?></div>
                    <div class='node-links'><?php if (isset($en_links)) print $en_links; ?></div>
                </div>
            </div>
            
        </div>
        
    
        <div class='node-row-2 clearfix'>
            <div class='node-info'>
                <div class='node-title'><?php print get_content_col1("", "", $en_title, $en_baseline, $en_created, ""); ?></div>
            </div>
            
            
            <div class='node-desc'>
                <div id="message-help">
                    <!-- Messages and Help -->
                        <?php if($is_admin): ?>
<?php print $messages; ?>
<?php endif; ?>
                    <?php print render($page['help']); ?>
                    
                    <!-- region: Secondary Content -->
                    <?php print render($page['secondary_content']); ?>
                    
                    <!-- region: Highlighted -->
                    <?php print render($page['highlighted']); ?>
                </div>
                
                <?php if ($primary_local_tasks || $secondary_local_tasks || $action_links): ?>
                    <div id="tasks">
                        <?php if ($primary_local_tasks): ?><ul class="tabs primary clearfix"><?php print render($primary_local_tasks); ?></ul><?php endif; ?>
                        <?php if ($secondary_local_tasks): ?><ul class="tabs secondary clearfix"><?php print render($secondary_local_tasks); ?></ul><?php endif; ?>
                        <?php if ($action_links = render($action_links)): ?><ul class="action-links clearfix"><?php print $action_links; ?></ul><?php endif; ?>
                    </div>
                <?php endif; ?>
                
                <div class='node-body'><?php if (isset($en_body)) print $en_body; ?></div>
            </div>
        </div>
        
        <div class='node-row-3 clearfix'>
            <div class='node-social'><?php if (isset($en_social)) print $en_social; ?></div>
        </div>
        
        
	<?php } else { ?>
    
	</div>
	
	<?php } ?>
    
    <div class='node-row-4 clearfix'>
        <<?php print $tag; ?> id="main-content">
            
            <?php if ($content = render($page['content'])): ?>
                <div id="content">
                	<div id="content-wrapper" <?php if (!isset($node) && !isset($user)) print 'class=" not-node"'; ?>>
                    	<?php print $content; ?>
					</div>
                    <?php print render($page['tertiary_content']); ?>
                </div>
            <?php endif; ?>
            
            
            <div id="sidebar-group">
            	<?php //if (isset($node) && $node->type != "page") { ?>
                	<?php $sidebar_first = render($page['sidebar_first']); print $sidebar_first; ?>
                <?php //} else { ?>
                	<?php //$sidebar_second = render($page['sidebar_second']); print $sidebar_second; ?>
                <?php //} ?>
                
            </div>
    
        </<?php print $tag; ?>>
	</div>
	

	<?php print render($page['content_aside']); ?>

	<?php if ($page['footer']): ?>
		<footer<?php print $footer_attributes; ?>>
			<?php print render($page['footer']); ?>
		</footer>
	<?php endif; ?>
    
</div>
