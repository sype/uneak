<?php
/**
 * @file
 * Adaptivetheme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Adaptivetheme variables:
 * - $is_mobile: Bool, requires the Browscap module to return TRUE for mobile
 *   devices. Use to test for a mobile context.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 */
 

hide($user_profile["user_picture"]);
hide($user_profile["field_image"]);
hide($user_profile["field_nom"]);
hide($user_profile["field_body"]);
hide($user_profile["field_link"]);
hide($user_profile["summary"]);


?>
<article class="profile clearfix"<?php print $attributes; ?>>
    <div class='pofile-row clearfix'>
		<div class='chapter-title'><h2 class="text-format-h3-bold"><?php print t("Profile"); ?></h2></div>
        <?php print render($user_profile['field_body']); ?>
		<div class='chapter-links'>
			<?php
                $cmpt = 0;
                $link_item = array();
                while(isset($user_profile['field_link'][$cmpt])) {
                    $link_item[] = array( 'data' => render($user_profile['field_link'][$cmpt]) );
                    $cmpt++;
                }
                if (count($link_item) != 0) {
                    $type = 'ul';
                    $attributes = array('class' => 'chapter-link-items');
                    print render(theme('block_item_list', array('items' => $link_item, 'title' => '', 'type' => $type, 'attributes' => $attributes)));
                }
            ?>        
        </div>
        <div class='profile-adds'><?php print render($user_profile); ?></div>
    </div>

</article>
