<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
 
 drupal_add_css();
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
    <head>
		<?php print $print['head']; ?>
        <?php print $print['base_href']; ?>
        <title><?php print $print['title']; ?></title>
        <?php print $print['scripts']; ?>
        <?php print $print['sendtoprinter']; ?>
        <?php print $print['robots_meta']; ?>
        <?php print $print['favicon']; ?>
        <?php print $print['css']; ?>
		<style>@import url("/<?php print path_to_theme()."/css/print.css"; ?>");</style>        
    </head>

    <body class="print-version html not-front logged-in two-sidebars page-user page-user- page-user-1 page-user-print domain-blog-uneak-fr toolbar toolbar-drawer">
        <div id="page" class="page">
            <div id="columns" class="columns container clearfix">
                <div id="content-column" class="content-column" role="main">
                    <div class="content-inner">
                        <section id="main-content">
                            <div id="content" class="region">
                                <div id="block-system-main" class="block block-system no-title" >  
                                    <?php print $print['content']; ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <?php print $print['footer_scripts']; ?>
    </body>
</html>
