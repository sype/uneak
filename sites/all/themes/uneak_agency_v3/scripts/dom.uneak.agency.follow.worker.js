
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

follow_list = [];
follow_value = [];
target_value = [];
target_style = [];



interval = setInterval(self.transitionFollow, 20);

function transitionFollow(event) {
	
	for( var i = self.follow_list.length-1; i >= 0; i-- ) {
		if (self.target_value[self.follow_list[i].target_id] != self.follow_value[self.follow_list[i].follow_id]) {
			diff = self.follow_value[self.follow_list[i].follow_id] - self.target_value[self.follow_list[i].target_id];
			if (Math.abs(diff) < 0.1) {
				self.target_value[self.follow_list[i].target_id] = self.follow_value[self.follow_list[i].follow_id];
			} else {
				self.target_value[self.follow_list[i].target_id] = self.target_value[self.follow_list[i].target_id] + diff * self.follow_list[i].speed;
			}
			self.follow_list[i].func(self.follow_list[i].target_id, self.target_value[self.follow_list[i].target_id]);
		}
	}
	
}


function messageHandler(event) {
	var m = event.data;

	//this.postMessage(new self.WorkerMessage('console', m.cmd));

	switch (m.cmd) {
			
		case 'addFollow':
            self.addFollow(m.p.target_id, m.p.follow_id, m.p.init_val, m.p.function_id, m.p.speed);
            break;
			
        case 'removeFollow':
			self.removeFollow(m.p);
            break;
		
		case 'follow':
			self.follow(m.p.follow_id, m.p.value);
			break;
		
    }
	
}

function follow(follow_id, value) {
	self.follow_value[follow_id] = value;
}

function removeFollow(target_id) {
	for( var i = self.follow_list.length-1; i >= 0; i-- ) if (self.follow_list[i].target_id == target_id) self.follow_list.splice(i, 1);
}

function addFollow(target_id, follow_id, init_val, function_id, speed) {
	self.follow_value[follow_id] = init_val;
	self.target_value[target_id] = init_val;
	self.target_style[target_id] = [];
	
	self.follow_list.push({target_id:target_id, follow_id:follow_id, func:self[function_id], speed:speed});
};








//
function f_4deg(target_id, value) {
	var style = [];
	style.left = -Math.round(value) + 'px';
	style.top = Math.round(value * 0.0676470588235) + 'px';
	self.styleLayer(target_id, style);
}	

function f_top_negatif(target_id, value) {
	var style = [];
	style.top = -Math.round(value) + 'px';
	self.styleLayer(target_id, style);
}

function f_4deg_top_negatif(target_id, value) {
	var style = [];
	style.top = -Math.round(value) + 'px';
	style.left = '0px';
	self.styleLayer(target_id, style);
}	




function styleLayer(target_id, style) {
	var dispatchedStyle = {};		
	var change = false;
	for (i in style) {
		if (!self.target_style[target_id][i] || style[i] != self.target_style[target_id][i]) {
			self.target_style[target_id][i] = style[i];
			dispatchedStyle[i] = style[i];
			change = true;
		}
	}
	if (change) this.postMessage(new self.WorkerMessage("styleLayer", {target_id:target_id, style:dispatchedStyle}));
}	






function WorkerMessage(cmd, parameter) {
	this.cmd = cmd;
	this.p = parameter;
}

//
this.addEventListener('message', messageHandler, false);

this.postMessage(new self.WorkerMessage('ready'));