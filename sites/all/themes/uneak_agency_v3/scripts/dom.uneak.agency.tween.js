
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

function UneakAgencyTween(theme_path, uneakAgency) {


	var self = this;
	this.isWorkers = Modernizr.webworkers;
	this.uneakAgency = uneakAgency;
	
	//
	//
	this.boot = function(theme_path) {
		
		this.tween_follow_list = [];
		
		setTimeout( function() {
			if (self.isWorkers) {
				self.tweenWorker = new Worker(theme_path+"/scripts/dom.uneak.agency.tween.worker.js");
				UneakEvent.addEventListener(self.tweenWorker, "message", self.tweenWorkerMessageHandler);
			} else {
				console.log("TWEEN BOOT");
				self.tweenProcess = new UneakAgencyTweenProcess(self);
				self.uneakAgency.tweenReady();
			}
		},20);
		
		
	};
	
	
	this.tweenWorkerMessageHandler = function(event) {
		var m = event.data;
		
//		console.log("tweenWorkerMessageHandler:"+m.cmd);
		
		switch (m.cmd) {
			case 'ready':
				self.uneakAgency.tweenReady();
				break;
				
			case 'styleLayer':
				self.styleLayer(m.p.tween_id, m.p.tween_follow_id, m.p.style);
				break;
				
			case 'tween_complete':
				self.tweenComplete(m.p.tween_id, m.p.tween_follow_id);
				break;
				
			case 'console':
				console.log(m.p);
				break;
		}
		
	};
	
	
	this.styleLayer = function(tween_id, tween_follow_id, style) {
		for( var i = this.tween_follow_list.length-1; i >= 0; i-- ) {
			if (this.tween_follow_list[i].tween_id == tween_id && this.tween_follow_list[i].tween_follow_id == tween_follow_id) {
				
				this.tween_follow_list[i].style = UneakEvent.concatObject(this.tween_follow_list[i].style, style);
//				console.log(this.tween_follow_list[i].style);
				break;
			}
		}
	};
	
	this.tweenComplete = function(tween_id, tween_follow_id) {
		for( var i = this.tween_follow_list.length-1; i >= 0; i-- ) {
			if (this.tween_follow_list[i].tween_id == tween_id && this.tween_follow_list[i].tween_follow_id == tween_follow_id) {
				if (this.tween_follow_list[i].internal_complete_function) {
//					console.log("TWEEN COMPLETE "+this.tween_follow_list[i].tween_id);
					this.tween_follow_list[i].internal_complete_function(this.tween_follow_list[i].target);
				}
				this.tween_follow_list[i].isComplete = true;
				break;
			}
		}
	};
	
	this.addTween = function(tween_id, init_val, function_refresh_id) {
		if (this.isWorkers) {
			this.tweenWorker.postMessage(new this.WorkerMessage('addTween', {tween_id:tween_id, init_val:init_val, function_refresh_id:function_refresh_id}));
		} else {
			this.tweenProcess.addTween(tween_id, init_val, function_refresh_id);
		}
	};
	
	this.tween = function(tween_id, tween_follow_id, target, to, speed, function_complete_id, internal_complete_function) { 
		var tween_follow_index = -1;
		for( var i = this.tween_follow_list.length-1; i >= 0; i-- ) if (this.tween_follow_list[i].tween_id == tween_id && this.tween_follow_list[i].tween_follow_id == tween_follow_id) { tween_follow_index = i; break; }
		if (tween_follow_index == -1) {
			tween_follow_index = this.tween_follow_list.push({tween_id:tween_id, tween_follow_id:tween_follow_id, target:target, style:{}, isComplete:false, internal_complete_function:internal_complete_function}) - 1;
		} else {
//			if (this.tween_follow_list[tween_follow_index].isComplete) console.log(tween_follow_id+" EXISTE et isComplete = true");
			this.tween_follow_list[tween_follow_index].internal_complete_function = internal_complete_function;
			this.tween_follow_list[tween_follow_index].target = target;
			this.tween_follow_list[tween_follow_index].isComplete = false;
		}

		if (this.isWorkers) {
			this.tweenWorker.postMessage(new this.WorkerMessage('tween', {tween_id:tween_id, tween_follow_id:tween_follow_id, to:to, speed:speed, function_complete_id:function_complete_id}));
		} else {
			this.tweenProcess.tween(tween_id, tween_follow_id, to, speed, function_complete_id);
		}
	
	};
	
	this.initialiseColor = function() {
		if (this.isWorkers) {
			this.tweenWorker.postMessage(new this.WorkerMessage('initialiseColor', null));
		} else {
			this.tweenProcess.initialiseColor();
		}
	};

	this.tweenValue = function(tween_id, value) {
		if (this.isWorkers) {
			this.tweenWorker.postMessage(new this.WorkerMessage('tweenValue', {tween_id:tween_id, value:value}));
		} else {
			this.tweenProcess.tweenValue(tween_id, value);
		}
	};

	this.refresh = function() {
		for( var i = this.tween_follow_list.length-1; i >= 0; i-- ) {
			for (var style in this.tween_follow_list[i].style) {
				if (style != "loadingOpacity" && style != "layerOpacity") {
					this.tween_follow_list[i].target.style[style] = this.tween_follow_list[i].style[style];
				} else {
					if (style == "loadingOpacity") this.tween_follow_list[i].target.element.style.opacity = this.tween_follow_list[i].style[style];
					if (style == "layerOpacity" && this.tween_follow_list[i].target.layer) this.tween_follow_list[i].target.layer.style.opacity = this.tween_follow_list[i].style[style];
				}
			}
			this.tween_follow_list[i].style = {};
			if (this.tween_follow_list[i].isComplete) {
				//console.log("refresh complete "+this.tween_follow_list[i].tween_id + ":"+this.tween_follow_list[i].isComplete);
				this.tween_follow_list.splice(i, 1);
			}
		}
	};
	
	this.WorkerMessage = function(cmd, parameter) {
		this.cmd = cmd;
		this.p = parameter;
	}	
	
	this.boot(theme_path);
}; 








/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

function UneakAgencyTweenProcess(uneakAgencyTween) {

	var self = this;
	this.uneakAgencyTween = uneakAgencyTween;
		
	this.tween_list = [];
	this.tween_follow_list = [];
	
	this.interval = window.setInterval(function(){self.transitionTween();}, 20);
//	console.log("process interval "+this.interval);
	
	this.transitionTween = function() {
		var size = self.tween_follow_list.length-1;
		
		if (self.tweenLength != self.tween_follow_list.length) {
			var tweenList = "";
			for( var i = size; i >= 0; i-- ) tweenList += " " + self.tween_follow_list[i].tween_id;
//			console.log("process self transitionTween "+self.tween_follow_list.length+" : " + tweenList);
			self.tweenLength = self.tween_follow_list.length;
		}
		
		
		for( var i = size; i >= 0; i-- ) {
			
//			console.log("process "+self.tween_follow_list[i].tween_id);
			if (self.tween_follow_list[i].tween.value != self.tween_follow_list[i]["to"]) {
				diff = self.tween_follow_list[i]["to"] - self.tween_follow_list[i].tween.value;
				if (Math.abs(diff) < 0.1) {
					self.tween_follow_list[i].tween.value = self.tween_follow_list[i]["to"];
				} else {
					self.tween_follow_list[i].tween.value = self.tween_follow_list[i].tween.value + diff * self.tween_follow_list[i]["speed"];
				}
				self.tween_follow_list[i].tween.func(i, self.tween_follow_list[i].tween.value);		
			} else {
				if (self.tween_follow_list[i]["function_complete_id"]) self.tween_follow_list[i]["function_complete_id"](i);
				self.uneakAgencyTween.tweenComplete(self.tween_follow_list[i].tween_id, self.tween_follow_list[i].tween_follow_id);
				//self.tween_follow_list.splice(i, 1);
			}
			
		}
		
	}
		
	this.addTween = function(tween_id, init_val, function_refresh_id) {
		this.tween_list.push({tween_id:tween_id, value:init_val, style:{}, func:this[function_refresh_id]});
	};
	
	this.tweenValue = function(tween_id, value) {
		for( var i = this.tween_list.length-1; i >= 0; i-- ) if (this.tween_list[i].tween_id == tween_id) { this.tween_list[i].value = value; break; }
	};
	
	this.tween = function(tween_id, tween_follow_id, to, speed, function_complete_id) {
		var tween_index = -1;
		for( var i = this.tween_list.length-1; i >= 0; i-- ) if (this.tween_list[i].tween_id == tween_id) { tween_index = i; break; }
		
		var tween = [];
		tween["tween_id"] = tween_id;
		tween["tween"] = this.tween_list[tween_index];
		tween["tween_follow_id"] = tween_follow_id;
		tween["to"] = to;
		tween["speed"] = speed;
		tween["function_complete_id"] = this[function_complete_id];
		tween["style"] = {};
		
		var tween_follow_index = -1;
		for( var i = this.tween_follow_list.length-1; i >= 0; i-- ) if (this.tween_follow_list[i].tween_id == tween_id) { tween_follow_index = i; break; }

		if (tween_follow_index == -1) {
//			console.log("add a tween "+tween_id + " " + tween_follow_id);
			this.tween_follow_list.push(tween);
		} else {
//			console.log("update a tween "+tween_id + " " + tween_follow_id);
			this.tween_follow_list[tween_follow_index] = tween;
		}
	};
	
	
	
	
	
	
	//
	
	this.f_opacity = function(tween_follow_index, value) {
		var style = {};
		style.opacity = value;
		self.styleLayer(tween_follow_index, style);
	}
	
	this.f_top_positif = function(tween_follow_index, value) {
		var style = {};
		style.top = Math.round(value) + 'px';
		self.styleLayer(tween_follow_index, style);
	}
	
	this.f_4deg = function(tween_follow_index, value) {
		var style = {};
		style.left = -Math.round(value) + 'px';
		style.top = Math.round(value * 0.0676470588235) + 'px';
		self.styleLayer(tween_follow_index, style);
	}	
	
	this.f_top_negatif = function(tween_follow_index, value) {
		var style = {};
		style.top = -Math.round(value) + 'px';
		self.styleLayer(tween_follow_index, style);
	}
	
	this.f_top_positif = function(tween_follow_index, value) {
		var style = {};
		style.top = Math.round(value) + 'px';
		self.styleLayer(tween_follow_index, style);
	}
	
	this.f_left_positif = function(tween_follow_index, value) {
		var style = {};
		style.left = Math.round(value) + 'px';
		self.styleLayer(tween_follow_index, style);
	}
	
	
	this.f_backgroundColor = function(tween_follow_index, value) {
		var style = {};
		style.backgroundColor = "rgb(" + Math.round(self.tween_list[self.rIndex].value) + "," + Math.round(self.tween_list[self.gIndex].value) + "," + Math.round(self.tween_list[self.bIndex].value) + ")";
		self.styleLayer(tween_follow_index, style);
	}
	
	
	this.initialiseColor = function() {
		for( var i = self.tween_list.length-1; i >= 0; i-- ) {
			if (self.tween_list[i].tween_id == "colorR") self.rIndex = i;
			if (self.tween_list[i].tween_id == "colorG") self.gIndex = i;
			if (self.tween_list[i].tween_id == "colorB") self.bIndex = i;
		}
	}
	
	
	this.f_bottom_positif = function(tween_follow_index, value) {
		var style = {};
		style.bottom = Math.round(value) + 'px';
		self.styleLayer(tween_follow_index, style);
	}
	
	
	this.f_displayNone = function(tween_follow_index) {
		var style = {};
		style.display = "none";
		self.styleLayer(tween_follow_index, style);
	};
	
	
	this.f_layer_opacity = function(tween_follow_index, value) {
		var style = {};
		style.layerOpacity = value;
		self.styleLayer(tween_follow_index, style);
	}
	
	this.f_loading_opacity = function(tween_follow_index, value) {
		var style = {};
		style.loadingOpacity = value;
		self.styleLayer(tween_follow_index, style);
	}
	
	
	this.styleLayer = function(tween_follow_index, style) {
		var dispatchedStyle = {};		
		var change = false;
		for (i in style) {
			if (!this.tween_follow_list[tween_follow_index]["style"][i] || style[i] != this.tween_follow_list[tween_follow_index]["style"][i]) {
				this.tween_follow_list[tween_follow_index]["style"][i] = style[i];
				dispatchedStyle[i] = style[i];
				change = true;
			}
		}
		if (change) this.uneakAgencyTween.styleLayer(this.tween_follow_list[tween_follow_index]["tween"].tween_id, this.tween_follow_list[tween_follow_index]["tween_follow_id"], dispatchedStyle);
	}	

}; 