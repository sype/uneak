
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

function UneakAgencyTooltip(container, theme_path) {
	
	var self = this;
	
	//
	//	FIRST CALL
	//
	this.initialize = function(container, theme_path) {
		
		//	TOOLTIP
		this.tiptip_holder = document.createElement("div"); this.tiptip_holder.setAttribute('id', 'tiptip_holder'); this.tiptip_holder.setAttribute('style', 'max-width:400px');
		this.tiptip_content = document.createElement("div"); this.tiptip_content.setAttribute('id', 'tiptip_content');
		this.tiptip_title = document.createElement("div"); this.tiptip_title.setAttribute('id', 'tiptip_title');
		this.tiptip_title_span = document.createElement("span"); this.tiptip_title_span.setAttribute('id', 'tiptip_title_span');
		this.tiptip_description = document.createElement("div"); this.tiptip_description.setAttribute('id', 'tiptip_description');
		this.tiptip_description_span = document.createElement("span"); this.tiptip_description_span.setAttribute('id', 'tiptip_description_span');
		this.tiptip_arrow = document.createElement("div"); this.tiptip_arrow.setAttribute('id', 'tiptip_arrow');
		this.tiptip_arrow_inner = document.createElement("div"); this.tiptip_arrow_inner.setAttribute('id', 'tiptip_arrow_inner');
		
		this.tiptip_title.appendChild(this.tiptip_title_span);
		this.tiptip_description.appendChild(this.tiptip_description_span);
		this.tiptip_arrow.appendChild(this.tiptip_arrow_inner);
		this.tiptip_holder.appendChild(this.tiptip_arrow);
		this.tiptip_content.appendChild(this.tiptip_title);
		this.tiptip_content.appendChild(this.tiptip_description);
		this.tiptip_holder.appendChild(this.tiptip_content);
		
		container.appendChild(this.tiptip_holder);

		document.Tween.addTween("tooltip", 0, "f_opacity");
		this.tiptip_holder.style.opacity = 0;
	};
	
	
	this.addTooltip = function(obj, options) {
		var defaults = { 
			keepAlive: false,
			maxWidth: "200px",
			edgeOffset: 3,
			defaultPosition: "bottom",
			delay: 400,
			fadeIn: 200,
			fadeOut: 200,
			titleAttribute: "title",
			descriptionAttribute: "data-description",
	  	};
		obj.tooltip = [];
		obj.tooltip.opts = UneakEvent.concatObject(defaults, options);
		
		obj.tooltip.title = obj.getAttribute(obj.tooltip.opts.titleAttribute);
		obj.tooltip.description = obj.getAttribute(obj.tooltip.opts.descriptionAttribute);
					
		if(obj.tooltip.title || obj.tooltip.description ){
			if(!obj.tooltip.opts.contentTitle){
				obj.removeAttribute(obj.tooltip.opts.titleAttribute); //remove original Attribute
			}
			if(!obj.tooltip.opts.contentDescription){
				obj.removeAttribute(obj.tooltip.opts.descriptionAttribute); //remove original Attribute
			}
			
			UneakEvent.addEventListener(obj, "mouseover", this.activeTooltip);
			//if (obj.tooltip.opts.keepAlive) UneakEvent.addEventListener(this.tiptip_holder, "mouseout", this.deactiveTooltipip);
		}				
	};
	
	
	this.removeTooltip = function(obj) {
		if(obj.tooltip.title || obj.tooltip.description ){
			UneakEvent.removeEventListener(obj, "mouseover", this.activeTooltip);
			UneakEvent.removeEventListener(this.tiptip_holder, "mouseover", this.holderTooltipipEnterEvent);
			if (this.currentAliveObject) UneakEvent.removeEventListener(this.currentAliveObject, "mouseout", this.timeOutTooltipip);
			UneakEvent.removeEventListener(this.tiptip_holder, "mouseout", this.holderTooltipipLeaveEvent);
		}				
	};
	
	
	
	this.contains = function(container, maybe) {
		return container.contains ? container.contains(maybe) : !!(container.compareDocumentPosition(maybe) & 16);
	}
	
	this.mouseEnterLeave = function(elem, type, method) {
		var mouseEnter = type === 'mouseenter',
			ie = mouseEnter ? 'fromElement' : 'toElement',
			method2 = function (e) {
				e = e || window.event;
				var target = e.target || e.srcElement,
					related = e.relatedTarget || e[ie];
				if ((elem === target || this.contains(elem, target)) &&
					!this.contains(elem, related)) {
						method();
				}
			};
		type = mouseEnter ? 'mouseover' : 'mouseout';
		UneakEvent.addEventListener(elem, type, method2);
		return method2;
	}


	this.holderTooltipipEnter = function() {
		UneakEvent.removeEventListener(self.tiptip_holder, "mouseover", self.holderTooltipipEnterEvent);
		if (this.tooltipTimeOut) clearTimeout(this.tooltipTimeOut);
		if (this.currentAliveObject) UneakEvent.removeEventListener(this.currentAliveObject, "mouseout", this.timeOutTooltipip);
		UneakEvent.removeEventListener(this.tiptip_holder, "mouseout", this.holderTooltipipLeaveEvent);
		UneakEvent.addEventListener(this.tiptip_holder, "mouseout", this.holderTooltipipLeaveEvent);
	}
	
	
	this.holderTooltipipLeave = function() {
		UneakEvent.removeEventListener(this.tiptip_holder, "mouseout", this.holderTooltipipLeaveEvent);
		this.deactiveTooltipip();
	}
	
	
	this.holderTooltipipLeaveEvent = function(event) {
		var event = window.event || event;
		var target = event.target || event.srcElement,
			related = event.relatedTarget || event.toElement;
		if ((self.tiptip_holder === target || self.contains(self.tiptip_holder, target)) && !self.contains(self.tiptip_holder, related)) {
			self.holderTooltipipLeave();
		}
	};
	
	this.holderTooltipipEnterEvent = function(event) {
		var event = window.event || event;
		var target = event.target || event.srcElement,
			related = event.relatedTarget || event.fromElement;
		if ((self.tiptip_holder === target || self.contains(self.tiptip_holder, target)) && !self.contains(self.tiptip_holder, related)) {
			self.holderTooltipipEnter();
		}
	};
	
	
	this.activeTooltip = function(event) {
		var event = window.event || event;

		if (self.tooltipTimeOut) clearTimeout(self.tooltipTimeOut);
		if (self.currentAliveObject) UneakEvent.removeEventListener(self.currentAliveObject, "mouseout", self.timeOutTooltipip);
		UneakEvent.removeEventListener(self.tiptip_holder, "mouseout", self.timeOutTooltipip);
		
		self.currentAliveObject = this;
		
		if (!this.tooltip.opts.keepAlive) {
			UneakEvent.removeEventListener(this, "mouseout", self.deactiveTooltipip);
			UneakEvent.addEventListener(this, "mouseout", self.deactiveTooltipip);
		} else {
			UneakEvent.removeEventListener(self.currentAliveObject, "mouseout", self.timeOutTooltipip);
			UneakEvent.addEventListener(self.currentAliveObject, "mouseout", self.timeOutTooltipip);
			
			UneakEvent.removeEventListener(self.tiptip_holder, "mouseover", self.holderTooltipipEnterEvent);
			UneakEvent.addEventListener(self.tiptip_holder, "mouseover", self.holderTooltipipEnterEvent);
		}
		
		while (self.tiptip_title_span.hasChildNodes()) self.tiptip_title_span.removeChild(self.tiptip_title_span.lastChild);
		while (self.tiptip_description_span.hasChildNodes()) self.tiptip_description_span.removeChild(self.tiptip_description_span.lastChild);

		if (this.tooltip.title && this.tooltip.title != "null") {
			self.tiptip_title_span.innerHTML = this.tooltip.title;
			self.tiptip_title.style.display = "block";
		} else {
			self.tiptip_title.style.display = "none";
		}
		if (this.tooltip.description && this.tooltip.description != "null") {
			self.tiptip_description_span.innerHTML = this.tooltip.description;
			self.tiptip_description.style.display = "block";
		} else {
			self.tiptip_description.style.display = "none";
		}
		self.tiptip_holder.style.display = "block";
		self.tiptip_holder.removeAttribute("class");
		self.tiptip_holder.style.margin = 0;
		//this.tiptip_holder.hide().removeAttr("class").css("margin","0");
		self.tiptip_arrow.removeAttribute("style");
		
		var coord = self.getCoord(this);
		
		var top = coord.top;
		var left = coord.left;
		var org_width = coord.width;
		var org_height = coord.height;
		var tip_w = self.tiptip_holder.offsetWidth;
		var tip_h = self.tiptip_holder.offsetHeight;		
		
		var w_compare = Math.round((org_width - tip_w) / 2);
		var h_compare = Math.round((org_height - tip_h) / 2);
		var marg_left = Math.round(left + w_compare);
		var marg_top = Math.round(top + org_height + this.tooltip.opts.edgeOffset);
		var t_class = "";
		var arrow_top = "";
		var arrow_left = Math.round(tip_w - 12) / 2;

		if(this.tooltip.opts.defaultPosition == "bottom"){
			t_class = "_bottom";
		} else if(this.tooltip.opts.defaultPosition == "top"){ 
			t_class = "_top";
		} else if(this.tooltip.opts.defaultPosition == "left"){
			t_class = "_left";
		} else if(this.tooltip.opts.defaultPosition == "right"){
			t_class = "_right";
		}
		
		var right_compare = (w_compare + left) < 0;
		var left_compare = (tip_w + left) > document.documentElement.clientWidth;
		
		if((right_compare && w_compare < 0) || (t_class == "_right" && !left_compare) || (t_class == "_left" && left < (tip_w + this.tooltip.opts.edgeOffset + 5))){
			t_class = "_right";
			arrow_top = Math.round(tip_h - 13) / 2;
			arrow_left = -12;
			marg_left = Math.round(left + org_width + this.tooltip.opts.edgeOffset);
			marg_top = Math.round(top + h_compare);
		} else if((left_compare && w_compare < 0) || (t_class == "_left" && !right_compare)){
			t_class = "_left";
			arrow_top = Math.round(tip_h - 13) / 2;
			arrow_left =  Math.round(tip_w);
			marg_left = Math.round(left - (tip_w + this.tooltip.opts.edgeOffset + 5));
			marg_top = Math.round(top + h_compare);
		}

		var top_compare = (top + org_height + this.tooltip.opts.edgeOffset + tip_h + 8) > document.documentElement.clientHeight;
		var bottom_compare = ((top + org_height) - (this.tooltip.opts.edgeOffset + tip_h + 8)) < 0;
		
		if(top_compare || (t_class == "_bottom" && top_compare) || (t_class == "_top" && !bottom_compare)){
			if(t_class == "_top" || t_class == "_bottom"){
				t_class = "_top";
			} else {
				t_class = t_class+"_top";
			}
			arrow_top = tip_h;
			marg_top = Math.round(top - (tip_h + 5 + this.tooltip.opts.edgeOffset));
		} else if(bottom_compare | (t_class == "_top" && bottom_compare) || (t_class == "_bottom" && !top_compare)){
			if(t_class == "_top" || t_class == "_bottom"){
				t_class = "_bottom";
			} else {
				t_class = t_class+"_bottom";
			}
			arrow_top = -12;						
			marg_top = Math.round(top + org_height + this.tooltip.opts.edgeOffset);
		}
	
		if(t_class == "_right_top" || t_class == "_left_top"){
			marg_top = marg_top + 5;
		} else if(t_class == "_right_bottom" || t_class == "_left_bottom"){		
			marg_top = marg_top - 5;
		}
		if(t_class == "_left_top" || t_class == "_left_bottom"){	
			marg_left = marg_left + 5;
		}
		
		self.tiptip_arrow.style.marginLeft = arrow_left+"px";
		self.tiptip_arrow.style.marginTop = arrow_top+"px";
		
		self.tiptip_holder.style.marginLeft = marg_left+"px";
		self.tiptip_holder.style.marginTop = marg_top+"px";
		self.tiptip_holder.setAttribute("class", "tip"+t_class);
		
		
		self.tiptip_holder.style.display = "block";
		document.Tween.tween("tooltip", "tiptip_holder", self.tiptip_holder, 1, 0.4);
	};
	
	
	this.timeOutTooltipip = function(event) {
		var event = window.event || event;
		UneakEvent.removeEventListener(this, "mouseout", self.timeoutTooltipip);
		if (self.tooltipTimeOut) clearTimeout(self.tooltipTimeOut);
		self.tooltipTimeOut = setTimeout(self.deactiveTooltipip, 1000);
		
	};
	
	
	this.deactiveTooltipip = function(event) {
		UneakEvent.removeEventListener(this, "mouseout", self.deactiveTooltipip);
		if (self.tooltipTimeOut) clearTimeout(self.tooltipTimeOut);
		document.Tween.tween("tooltip", "tiptip_holder", self.tiptip_holder, 0, 0.4, "f_displayNone");
	};	
	
	
	this.getCoord = function(obj, offsetLeft, offsetTop) {
        var orig = obj;
        var left = 0;
        var top = 0;
        if (offsetLeft) left = offsetLeft;
        if (offsetTop) top = offsetTop;
        if (obj.offsetParent) {
			left += obj.offsetLeft;
			top += obj.offsetTop;
			while (obj = obj.offsetParent) {
				left += (obj.offsetLeft - obj.scrollLeft + obj.clientLeft);
				top += (obj.offsetTop - obj.scrollTop + obj.clientTop);
			}
        }
        return {left:left, top:top, width: orig.offsetWidth, height: orig.offsetHeight};
	}
	

	
	//
	this.initialize(container, theme_path);
}; 