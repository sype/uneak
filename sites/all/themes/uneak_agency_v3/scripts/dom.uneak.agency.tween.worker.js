
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

var tween_list = [];
var tween_follow_list = [];
var interval;

function transitionTween(event) {
	
	var size = self.tween_follow_list.length-1;
//	this.postMessage(new self.WorkerMessage('console', "transitionTween "+ self.tween_follow_list.length));
	if (self.tween_follow_list.length == 0) {
		clearInterval(self.interval);
		self.interval = null;
		return;
	}
	
	for( var i = size; i >= 0; i-- ) {
		//if (self.tween_follow_list[i].tween_id == "layer_LAYER-4") 
		//this.postMessage(new self.WorkerMessage('console', i+":"+self.tween_follow_list[i].tween_id + " / ("+self.tween_follow_list[i]["to"]+") :" + self.tween_follow_list[i].tween.value));
		
//		if (!self.tween_follow_list[i].tween) this.postMessage(new self.WorkerMessage('console', i+":"+self.tween_follow_list[i].tween_id + " / ("+self.tween_follow_list[i]["to"]+") :"));
		
		//this.postMessage(new self.WorkerMessage('console', "update pos "+ self.tween_follow_list[i]["tween_id"]+"/"+self.tween_follow_list[i]["tween_follow_id"]));
		
/*		
		this.postMessage(new self.WorkerMessage('console', "transitionTween ("+i+")------------------"));
		this.postMessage(new self.WorkerMessage('console', "transitionTween ("+i+") "+ self.tween_follow_list[i]["tween_id"]+"/"+self.tween_follow_list[i]["tween_follow_id"]));
		this.postMessage(new self.WorkerMessage('console', "transitionTween ("+i+") self.tween_follow_list[i].tween.value:"+self.tween_follow_list[i].tween.value));
		this.postMessage(new self.WorkerMessage('console', "transitionTween ("+i+") self.tween_follow_list[i][to]:"+self.tween_follow_list[i]["to"]));
		this.postMessage(new self.WorkerMessage('console', "transitionTween ("+i+") self.tween_follow_list[i][speed]:"+self.tween_follow_list[i]["speed"]));
		this.postMessage(new self.WorkerMessage('console', "transitionTween ("+i+") self.tween_follow_list[i].tween.func:"+self.tween_follow_list[i].tween.func));
		this.postMessage(new self.WorkerMessage('console', "transitionTween ("+i+") self.tween_follow_list[i][function_complete_id]:"+self.tween_follow_list[i]["function_complete_id"]));
*/		
		if (self.tween_follow_list[i].tween.value != self.tween_follow_list[i]["to"]) {
			diff = self.tween_follow_list[i]["to"] - self.tween_follow_list[i].tween.value;
			if (Math.abs(diff) < 0.1) {
				self.tween_follow_list[i].tween.value = self.tween_follow_list[i]["to"];
			} else {
				self.tween_follow_list[i].tween.value = self.tween_follow_list[i].tween.value + diff * self.tween_follow_list[i]["speed"];
			}
			self.tween_follow_list[i].tween.func(i, self.tween_follow_list[i].tween.value);		
		} else {
			if (self.tween_follow_list[i]["function_complete_id"]) self.tween_follow_list[i]["function_complete_id"](i);
			this.postMessage(new self.WorkerMessage("tween_complete", {tween_id:self.tween_follow_list[i].tween_id, tween_follow_id:self.tween_follow_list[i].tween_follow_id }));
			self.tween_follow_list.splice(i, 1);
		}
		
	}
	
}


function messageHandler(event) {
	var m = event.data;

//	this.postMessage(new self.WorkerMessage('console', 'messageHandler:'+m.cmd));

	switch (m.cmd) {
					
		case 'addTween':
            self.addTween(m.p.tween_id, m.p.init_val, m.p.function_refresh_id);
            break;
			
		case 'tween':
            self.tween(m.p.tween_id, m.p.tween_follow_id, m.p.to, m.p.speed, m.p.function_complete_id);
            break;
			
		case 'tweenValue':
            self.tweenValue(m.p.tween_id, m.p.value);
            break;
			
		case 'initialiseColor':
            self.initialiseColor();
            break;
			
    }
	
}



function addTween(tween_id, init_val, function_refresh_id) {
	self.tween_list.push({tween_id:tween_id, value:init_val, style:[], func:self[function_refresh_id]});
};

function tweenValue(tween_id, value) {
	for( var i = self.tween_list.length-1; i >= 0; i-- ) if (self.tween_list[i].tween_id == tween_id) { self.tween_list[i].value = value; break; }
};

function tween(tween_id, tween_follow_id, to, speed, function_complete_id) {
//	this.postMessage(new self.WorkerMessage('console', "tween "+tween_id+"/"+tween_follow_id));
	
	var tween_index = -1;
	for( var i = self.tween_list.length-1; i >= 0; i-- ) if (self.tween_list[i].tween_id == tween_id) { tween_index = i; break; }
	//if (tween_index == -1) return;
	
//	this.postMessage(new self.WorkerMessage('console', "tween index "+tween_index));
	
	var tween = [];
	tween["tween_id"] = tween_id;
	tween["tween"] = self.tween_list[tween_index];
	tween["tween_follow_id"] = tween_follow_id;
	tween["to"] = to;
	tween["speed"] = speed;
	tween["function_complete_id"] = self[function_complete_id];
	tween["style"] = [];
	
	var tween_follow_index = -1;
	for( var i = self.tween_follow_list.length-1; i >= 0; i-- ) if (self.tween_follow_list[i].tween_id == tween_id) { tween_follow_index = i; break; }
	if (tween_follow_index == -1) {
//		this.postMessage(new self.WorkerMessage('console', "tween NEW "));
		self.tween_follow_list.push(tween);
	} else {
//		this.postMessage(new self.WorkerMessage('console', "tween UPDATE "));
		self.tween_follow_list[tween_follow_index] = tween;
	}
	
//	this.postMessage(new self.WorkerMessage('console', "self.interval "+self.interval));
	if (!self.interval) self.interval = setInterval(self.transitionTween, 20);

};






//

function f_opacity(tween_follow_index, value) {
	var style = [];
	style.opacity = value;
	self.styleLayer(tween_follow_index, style);
}

function f_top_positif(tween_follow_index, value) {
	var style = [];
	style.top = Math.round(value) + 'px';
	self.styleLayer(tween_follow_index, style);
}

function f_4deg(tween_follow_index, value) {
	var style = [];
	style.left = -Math.round(value) + 'px';
	style.top = Math.round(value * 0.0676470588235) + 'px';
	self.styleLayer(tween_follow_index, style);
}	

function f_top_negatif(tween_follow_index, value) {
	var style = [];
	style.top = -Math.round(value) + 'px';
	self.styleLayer(tween_follow_index, style);
}

function f_top_positif(tween_follow_index, value) {
	var style = [];
	style.top = Math.round(value) + 'px';
	self.styleLayer(tween_follow_index, style);
}

function f_left_positif(tween_follow_index, value) {
	var style = [];
	style.left = Math.round(value) + 'px';
	self.styleLayer(tween_follow_index, style);
}


function f_backgroundColor(tween_follow_index, value) {
	var style = [];
	style.backgroundColor = "rgb(" + Math.round(self.tween_list[self.rIndex].value) + "," + Math.round(self.tween_list[self.gIndex].value) + "," + Math.round(self.tween_list[self.bIndex].value) + ")";
	self.styleLayer(tween_follow_index, style);
}


function initialiseColor() {
	for( var i = self.tween_list.length-1; i >= 0; i-- ) {
		if (self.tween_list[i].tween_id == "colorR") self.rIndex = i;
		if (self.tween_list[i].tween_id == "colorG") self.gIndex = i;
		if (self.tween_list[i].tween_id == "colorB") self.bIndex = i;
	}
}


function f_bottom_positif(tween_follow_index, value) {
	var style = [];
	style.bottom = Math.round(value) + 'px';
	self.styleLayer(tween_follow_index, style);
}


function f_displayNone(tween_follow_index) {
	var style = [];
	style.display = "none";
	self.styleLayer(tween_follow_index, style);
};


function f_layer_opacity(tween_follow_index, value) {
	var style = [];
	style.layerOpacity = value;
	self.styleLayer(tween_follow_index, style);
}

function f_loading_opacity(tween_follow_index, value) {
	var style = [];
	style.loadingOpacity = value;
	self.styleLayer(tween_follow_index, style);
}






function styleLayer(tween_follow_index, style) {
	var dispatchedStyle = {};		
	var change = false;
	
	
	for (i in style) {
//		this.postMessage(new self.WorkerMessage('console', i+":"+style[i]));
//		this.postMessage(new self.WorkerMessage('console', "test 1:"+(!self.tween_follow_list[tween_follow_index]["style"][i])));
//		this.postMessage(new self.WorkerMessage('console', "test 2:"+(style[i] != self.tween_follow_list[tween_follow_index]["style"][i])));
		
		
		if (!self.tween_follow_list[tween_follow_index]["style"][i] || style[i] != self.tween_follow_list[tween_follow_index]["style"][i]) {
			self.tween_follow_list[tween_follow_index]["style"][i] = style[i];
			dispatchedStyle[i] = style[i];
			
			change = true;
		}
	}
	
	if (change) {
		this.postMessage(new self.WorkerMessage("styleLayer", {tween_id:self.tween_follow_list[tween_follow_index]["tween"].tween_id, tween_follow_id:self.tween_follow_list[tween_follow_index]["tween_follow_id"], style:dispatchedStyle}));
	}
	
}	






function WorkerMessage(cmd, parameter) {
	this.cmd = cmd;
	this.p = parameter;
}

//
this.addEventListener('message', messageHandler, false);

this.postMessage(new self.WorkerMessage('ready'));