
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

function UneakAgency(theme_path) {

	var self = this;
	this.loading = [];
	this.mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
	this.repere_y = document.documentElement.clientHeight / 2;
	this.col2Size = 0;
	this.isBuild = false;
	
	
	//
	//	INITIALIZE
	//
	try { document.createEvent("TouchEvent"); this.isTouchDevice = true; } catch(e) { this.isTouchDevice = false; }
	//this.isTouchDevice = true;
	this.amplitude = (this.isTouchDevice) ? 8 : 2;
	this.presentCol = 0;
	

	//
	//	FIRST CALL
	//
	this.boot = function(theme_path) {
	
		this.theme_path = theme_path;
		//
		//	DOCUMENT ELEMENTS
		//
		this.page = document.getElementById("page");
		this.background_sky_container = document.getElementById("background-sky-container");
		this.page_content = document.getElementById("page-content");
		this.header = document.getElementById("header");
		this.page_header_wrapper = document.getElementById("page-header-wrapper");
		this.page_content_wrapper = document.getElementById("page-content-wrapper");
		this.main_content_node = document.getElementById("main-content-node");
		this.branding = document.getElementById("branding");
		this.top_menu = document.getElementById("top-menu");
		this.top_menu_wrapper = document.getElementById("top-menu-wrapper");
		this.node_header = document.getElementById("node-header");
		this.breadcrumb_container = document.getElementById("breadcrumb-container");
		this.cover = document.getElementById("cover");
		this.cover_background = document.getElementById("cover-background");
		this.cover_foreground = document.getElementById("cover-foreground");
		this.cover_image = document.getElementById("cover-image");
		this.loading_animation_wrapper = document.getElementById("loading-animation-wrapper");
		this.rubrique_title = document.getElementById("rubrique-title");
		this.rubrique_sous_title = document.getElementById("rubrique-sous-title");
		this.footer = document.getElementById("footer");
		this.footer_container = document.getElementById("footer-container");
		this.background_sky_color = document.getElementById("background-sky-color");
		this.background_sky_0 = document.getElementById("background-sky-0");
		this.background_sky_1 = document.getElementById("background-sky-1");
		this.background_sky_2 = document.getElementById("background-sky-2");
		this.background_sky_3 = document.getElementById("background-sky-3");
		this.background_sky_4 = document.getElementById("background-sky-4");
		
		
		this.col1root = this.page_content_wrapper;
		this.col2root = this.page_content_wrapper;
			
		
		// HIDE ELEMENT FOR LOADING
		this.background_sky_0.style.display = "none";
		this.background_sky_1.style.display = "none";
		this.background_sky_2.style.display = "none";
		this.background_sky_3.style.display = "none";
		this.background_sky_4.style.display = "none";
		
		this.page_content_wrapper.style.display = "none";
		this.footer.style.display = "none";




		
		//	NODE-TITLE
		
		if (!this.isTouchDevice) {
			//	CREATE NODE CONTENT-CLONE
			this.content_clone = document.createElement("div"); this.content_clone.className = 'content-clone';
			this.content_clone_wrapper = document.createElement("div"); this.content_clone_wrapper.className = 'content-clone-wrapper';
			this.content_clone.appendChild(this.content_clone_wrapper);
			this.page_content.appendChild(this.content_clone);
		
			//	CREATE NODE SCROLLBARS
			var scrollbars = document.createElement("div"); scrollbars.className = 'scrollbars';
			this.scrollbar_vertical = document.createElement("div"); this.scrollbar_vertical.className = 'scrollbar scrollbar-vertical';
			this.scrollbar_vertical.style.opacity = 0;
			this.scrollbar_vertical_dragger_container = document.createElement("div"); this.scrollbar_vertical_dragger_container.className = 'scrollbar-dragger-container scrollbar-vertical-dragger-container';
			this.scrollbar_vertical_dragger_bar = document.createElement("div"); this.scrollbar_vertical_dragger_bar.className = 'scrollbar-dragger-bar scrollbar-vertical-dragger-bar';
			this.scrollbar_vertical_dragger_rail = document.createElement("div"); this.scrollbar_vertical_dragger_rail.className = 'scrollbar-dragger-rail scrollbar-vertical-dragger-rail';
			this.scrollbar_vertical_dragger_rail.appendChild(this.scrollbar_vertical_dragger_bar);
			this.scrollbar_vertical_dragger_container.appendChild(this.scrollbar_vertical_dragger_rail);
			this.scrollbar_vertical.appendChild(this.scrollbar_vertical_dragger_container);
			scrollbars.appendChild(this.scrollbar_vertical);
			//
			this.page.appendChild(scrollbars);
			//
			this.page_content.style.overflow = "hidden";
		}
			
		this.createLoading("MAIN");
		this.startLoading("MAIN");
			
		document.Tween = new UneakAgencyTween(this.theme_path, this);
	};
	
	this.tweenReady = function() {
		if (!this.isTouchDevice) document.Follow = new UneakAgencyFollow(self.theme_path, self); else self.followReady();
	}
	
	this.followReady = function() {
		document.Parallax = new UneakAgencyParallax(self.theme_path, self);
	}
	
	this.parallaxReady = function() {
		if (!this.isTouchDevice) document.Tooltip = new UneakAgencyTooltip(self.page, self);
		self.loadDocument();
	}
	
			
	
	
	this.loadDocument = function() {

		this.animate();
	
		if (!this.isTouchDevice) {
			document.Tween.addTween("verticalDragger", 0, "f_top_positif");
			document.Tween.addTween("scrollbar_vertical", 0, "f_opacity");
		}
		
	
		//	CREATE LAYER	
		document.Parallax.createLayer(this.background_sky_0, "LAYER-0", true);
		document.Parallax.createLayer(this.background_sky_1, "LAYER-1", true);
		document.Parallax.createLayer(this.background_sky_2, "LAYER-2", true);
		document.Parallax.createLayer(this.background_sky_3, "LAYER-3", true);
		document.Parallax.createLayer(this.background_sky_4, "LAYER-4", true);
		document.Parallax.createLayer(this.footer, "FOOTER", true);
	

		//	INITIALISE BACKGROUND COLOR
		if (this.background_sky_color.style.backgroundColor) {
			rgbArray = this.background_sky_color.style.backgroundColor.split(",");
			document.Tween.addTween("colorR", parseInt(rgbArray[0].replace(/[^0-9.]/g, "")), "f_backgroundColor");
			document.Tween.addTween("colorG", parseInt(rgbArray[1].replace(/[^0-9.]/g, "")), "f_backgroundColor");
			document.Tween.addTween("colorB", parseInt(rgbArray[2].replace(/[^0-9.]/g, "")), "f_backgroundColor");
			document.Tween.initialiseColor();
		}


		this.readyStateCheckInterval = setInterval(function() {
			if (document.readyState === "complete") {
				clearInterval(self.readyStateCheckInterval);
				self.initialize();
			}
		}, 20);
		
	};
	
	
	

	
	
    this.initialize = function() {

		//	START MUSIC
		self.audioElement = new Audio();
		var source = document.createElement('source');
		if (self.audioElement.canPlayType('audio/mpeg;')) {
			source.type= 'audio/mpeg';
			source.src= self.theme_path+"/music/music.mp3";
		} else {
			source.type= 'audio/ogg';
			source.src= self.theme_path+"/music/music.ogg";
		}
		self.audioElement.appendChild(source);
		self.audioElement.play();
	
		self.musicIsPlaying = true;

		//	SHOW HIDDEN ELEMENT FOR LOADING
		self.background_sky_0.style.removeProperty("display");;
		self.background_sky_1.style.removeProperty("display");
		self.background_sky_2.style.removeProperty("display");
		self.background_sky_3.style.removeProperty("display");
		self.background_sky_4.style.removeProperty("display");
		self.footer.style.removeProperty("display");
		
		
		
		//	INITIALIZE NAVIGATION MODE
		self.initNavigation();
			
		//	MUSIC NODE
		self.music = document.createElement("div"); self.music.className = (self.musicIsPlaying) ? 'music on' : 'music off';
		UneakEvent.addEventListener(self.music, 'click', self.musicClick);

		//	BUILD
		this.page_content_wrapper.style.removeProperty("display");
//		this.main_content_node_wrapper.style.display = "none";
		self.build();
		
		//
		//	LISTENER
		//
		
		if (!self.isTouchDevice) {
			UneakEvent.addEventListener(document, "mousemove", self.mouseMove);
		} else {
			self.lastOrientation = {gamma:0, beta:0};
			if ("ondeviceorientation" in window) {
				UneakEvent.addEventListener(window, 'deviceorientation', self.onDeviceOrientationChange);
			} else if ("ondevicemotion" in window) {
				UneakEvent.addEventListener(window, 'devicemotion', self.onDeviceMotionChange);
			}
			var accel = window.setInterval(self.refreshOrientation, 20);
		}
		
		UneakEvent.addEventListener(window, 'resize', self.resfreshResize);
	};
	
	
	
	
	
	this.build = function() {
		
		this.stopLoading("MAIN");
		
		this.main_content_node_wrapper = document.getElementById("main-content-node-wrapper");
		document.Tween.addTween("main_content", -document.documentElement.clientHeight, "f_top_negatif");
		
		
		if (!self.isTouchDevice) {		
			//	BUILD NODE CONTENT-CLONE
			this.clone_col1_foreground = document.createElement("div"); this.clone_col1_foreground.className = 'clone-col clone-col1 clone-col-foreground clone-col1-foreground';
			this.clone_col1_foreground_wrapper = document.createElement("div"); this.clone_col1_foreground_wrapper.className = 'clone-col-wrapper clone-col1-wrapper clone-col-foreground clone-col-foreground-wrapper clone-col1-foreground-wrapper';
			document.Tween.addTween("col1", -document.documentElement.clientHeight, "f_top_negatif");
			this.clone_col1_foreground_wrapper.style.top = -document.documentElement.clientHeight + "px";
			this.clone_col2_foreground = document.createElement("div"); this.clone_col2_foreground.className = 'clone-col clone-col2 clone-col-foreground clone-col2-foreground';
			this.clone_col2_foreground_wrapper = document.createElement("div"); this.clone_col2_foreground_wrapper.className = 'clone-col-wrapper clone-col2-wrapper clone-col-foreground clone-col-foreground-wrapper clone-col2-foreground-wrapper';
			document.Tween.addTween("col2", -document.documentElement.clientHeight, "f_top_negatif");
			this.clone_col2_foreground_wrapper.style.top = -document.documentElement.clientHeight + "px";
			this.content_clone_wrapper.appendChild(this.clone_col1_foreground);
			this.content_clone_wrapper.appendChild(this.clone_col2_foreground);		
			this.clone_col1_foreground.appendChild(this.clone_col1_foreground_wrapper);	
			this.clone_col2_foreground.appendChild(this.clone_col2_foreground_wrapper);	
			
			//	INITIALIZE COLS/SCROLLBAR POSITION
			this.verticalDraggerPosition = 0;
	
			this.scrollbar_vertical_dragger_bar.style.top = "0px";
			this.clone_col2_foreground_wrapper.style.top = "0px";
			
			this.col1root = this.clone_col1_foreground_wrapper;
			this.col2root = this.clone_col2_foreground_wrapper;
		
		}
		
						
		//	FOOTER MUSIC
		var footer_menu = document.getElementById("block-menu-menu-officiel-sous-menu");
		footer_menu.parentNode.insertBefore(this.music, footer_menu);
		
		
	
		
		
		
		//	INITIALIZE COL/ROW DATA
		
		this.col1fore = [];
		this.col2fore = [];
		this.elementData = [];
		
		
		//	ROOT ELEMENT DATA
		this.elementData["title"] = this.main_content_node_wrapper.getAttribute("data-title");
		this.elementData["baseline"] = this.main_content_node_wrapper.getAttribute("data-baseline");
		this.elementData["color"] = {r:36, g:108, b:160};
		if (this.main_content_node_wrapper.getAttribute("data-color-r")) {
			this.elementData["color"].r = this.main_content_node_wrapper.getAttribute("data-color-r");
			this.elementData["color"].g = this.main_content_node_wrapper.getAttribute("data-color-g");
			this.elementData["color"].b = this.main_content_node_wrapper.getAttribute("data-color-b");
		}
		this.elementData["cover"] = this.main_content_node_wrapper.getAttribute("data-cover");
		this.elementData["layer"] = [];
		if (this.main_content_node_wrapper.getAttribute("data-layer")) {
			this.elementData["layer"] = eval("(" + this.main_content_node_wrapper.getAttribute("data-layer") + ")");
			for (var z in this.elementData["layer"]) this.elementData["layer"][z].distance = this.elementData["layer"][z].distance / this.amplitude;
		}
		//
		
		
		
		
		var rub_id = 0;
		var group_id = 0;

		//		
		this.col1fore[rub_id] = [];
		this.col1fore[rub_id][group_id] = [];

		if (!this.isTouchDevice) {
			var col1header = this.header.cloneNode(true);
			col1header.setAttribute("data-id-rubrique", 0);
			col1header.setAttribute("data-id-group", 0);
			if (!this.isTouchDevice) this.clone_col1_foreground_wrapper.appendChild(col1header);
			this.col1fore[rub_id][group_id].push({clone:col1header, height:0, top:0});
		}
		
		


		//
		this.col2fore[rub_id] = [];
		this.col2fore[rub_id][group_id] = [];
		
		var col2header = this.page_header_wrapper;
		col2header.setAttribute("data-id-rubrique", 0);
		col2header.setAttribute("data-id-group", 0);
		
		var col2header = this.page_header_wrapper.cloneNode(true);
		if (!this.isTouchDevice) {
			var cloneFront = col2header.cloneNode(true);
			this.clone_col2_foreground_wrapper.appendChild(cloneFront);
		} else {
			var cloneFront = col2header;
		}

		this.col2fore[rub_id][group_id].push({clone:cloneFront, height:0, top:0});
		
		
		//
		this.elementData[rub_id] = this.elementData;	
		this.elementData[rub_id][group_id] = this.elementData;

		rub_id++;

		
		
		
		//
		var nbRubriques = this.main_content_node_wrapper.children.length;
		for(var i = 0; i < nbRubriques; i++) {
			
			this.col1fore[rub_id] = [];
			this.col2fore[rub_id] = [];
			this.elementData[rub_id] = this.get_elementData(this.elementData, this.main_content_node_wrapper.children[i]);
			
			var rubrique_class = this.main_content_node_wrapper.children[i].className;
			
//
			
			group_id = 0;
			var nbGroup = this.main_content_node_wrapper.children[i].children.length;
			for(var j = 0; j < nbGroup; j++) {
				
				
				
				this.col1fore[rub_id][group_id] = [];
				this.col2fore[rub_id][group_id] = [];
				this.elementData[rub_id][group_id] = this.get_elementData(this.elementData[rub_id], this.main_content_node_wrapper.children[i].children[j]);

				var group_class = this.main_content_node_wrapper.children[i].children[j].className;

				var group_col_1 = this.getElementByClass(this.main_content_node_wrapper.children[i].children[j].children[0], "group-col-1");
				var group_col_2 = this.getElementByClass(this.main_content_node_wrapper.children[i].children[j].children[0], "group-col-2");
				
				//
				if (group_col_1) {
					group_col_1.setAttribute("data-id-rubrique", i);
					group_col_1.setAttribute("data-id-group", j);
					
					if (!this.isTouchDevice) {
						var cloneFront = group_col_1.cloneNode(true);
						cloneFront.classList.add('out');
						this.clone_col1_foreground_wrapper.appendChild(cloneFront);
					} else {
						var cloneFront = group_col_1;
					}
										
					this.col1fore[rub_id][group_id].push({clone:cloneFront, height:0, top:0});
				}
				
				//
				if (group_col_2) {
					group_col_2.setAttribute("data-id-rubrique", i);
					group_col_2.setAttribute("data-id-group", j);
					
					var group_col_2_bck = this.getElementByClass(this.main_content_node_wrapper.children[i].children[j].children[0], "group-col-2-background");
					var group_col_3 = this.getElementByClass(this.main_content_node_wrapper.children[i].children[j].children[1], "group-col-3");
					
					if (!this.isTouchDevice) {
						
						var cloneFront = document.createElement("div");
						if (j != nbGroup-1) {
							cloneFront.className = group_class;
						} else {
							cloneFront.className = rubrique_class;
						}
						
						var group_content = document.createElement("div");
						group_content.className = "clone-group-content";
						cloneFront.appendChild(group_content);
						
						if (group_col_2_bck) group_content.appendChild(group_col_2_bck.cloneNode(true));
						group_content.appendChild(group_col_2.cloneNode(true));
						if (group_col_3) cloneFront.appendChild(group_col_3.cloneNode(true));
						
						this.clone_col2_foreground_wrapper.appendChild(cloneFront);
						
					} else {
						var cloneFront = group_col_2;
					}
	
					this.col2fore[rub_id][group_id].push({clone:cloneFront, height:0, top:0});
						
				}
				
				group_id ++;
			}
			
			rub_id++;
		}
		
		
		//return;
		
		
		//
		
		
		if (this.isTouchDevice) {
			this.main_content_node_wrapper.style.removeProperty("display");
			this.followScroll();
		} else {
			this.page_content_wrapper.style.display = "none";
			this.main_content_node_wrapper.parentNode.removeChild(this.main_content_node_wrapper);
			document.Tween.tween("col1", "clone_col1_foreground_wrapper", this.clone_col1_foreground_wrapper, 0, 0.4);
			document.Tween.tween("col2", "clone_col2_foreground_wrapper", this.clone_col2_foreground_wrapper, 0, 0.1, null, this.followScroll);
		} 
		
		//
		
		
		if (this.xhr) {
			this.updateDocument(this.page_content, true);
			this.updateDocument(this.footer, true);
		}
		
		Shadowbox.setup();
		
	};
	
	
	
	
	
	this.get_elementData = function(parent_data, current_element) {
		var ret_array = [];
		
		if (current_element.getAttribute("data-title")) {
			ret_array["title"] = current_element.getAttribute("data-title");
		} else {
			ret_array["title"] = parent_data["title"];
		}
		 
		if (current_element.getAttribute("data-baseline")) {
			ret_array["baseline"] = current_element.getAttribute("data-baseline")
		} else {
			ret_array["baseline"] = parent_data["baseline"];
		}
		
		ret_array["color"] = {};
		if (current_element.getAttribute("data-color-r")) {
			ret_array["color"].r = current_element.getAttribute("data-color-r");
			ret_array["color"].g = current_element.getAttribute("data-color-g");
			ret_array["color"].b = current_element.getAttribute("data-color-b");
		} else {
			ret_array["color"] = parent_data["color"];
		}
		
		if (current_element.getAttribute("data-cover")) {
			ret_array["cover"] = current_element.getAttribute("data-cover");
		} else {
			ret_array["cover"] = parent_data["cover"];
		}
		
		if (current_element.getAttribute("data-layer")) {
			ret_array["layer"] = eval("(" + current_element.getAttribute("data-layer") + ")");
			for (var z in ret_array["layer"]) ret_array["layer"][z].distance = ret_array["layer"][z].distance / this.amplitude;
		} else {
			ret_array["layer"] = parent_data["layer"];
		}
		//
		
		return ret_array;
	}
	
	
	

	
	
	this.followScroll = function() {
		self.isBuild = true;
		
		if (!self.isTouchDevice) {
			document.Follow.addFollow("clone_col1_foreground_wrapper", self.clone_col1_foreground_wrapper, "scrollCol1Position", 0, "f_top_negatif", 0.05);
			document.Follow.addFollow("clone_col2_foreground_wrapper", self.clone_col2_foreground_wrapper, "scrollCol2Position", 0, "f_top_negatif", 0.1);
			self.checkHeightInterval = setInterval(self.checkHeight, 2000);
		}

		self.currentRubrique = -1;
		self.currentGroup = -1;
		self.resfreshResize();
	};		
	
	
	
	
	
	this.activateScroll = function() {
		if (self.isTouchDevice) {
			UneakEvent.removeEventListener(self.page_content, "scroll", self.mainContentScroll);
			
			UneakEvent.addEventListener(self.page_content, "scroll", self.mainContentScroll);
				
		} else {
			UneakEvent.removeEventListener(document, self.mousewheelevt, self.scrollbarVerticalWheel);
			UneakEvent.removeEventListener(self.scrollbar_vertical_dragger_bar, "mousedown", self.scrollbarVerticalClick);
			UneakEvent.removeEventListener(self.scrollbar_vertical_dragger_rail, "mousedown", self.scrollbarVerticalRailClick);
			
			UneakEvent.addEventListener(document, self.mousewheelevt, self.scrollbarVerticalWheel);
			UneakEvent.addEventListener(self.scrollbar_vertical_dragger_bar, "mousedown", self.scrollbarVerticalClick);
			UneakEvent.addEventListener(self.scrollbar_vertical_dragger_rail, "mousedown", self.scrollbarVerticalRailClick);
			
			//this.scrollbar_vertical.style.removeProperty("display");
			document.Tween.tween("scrollbar_vertical", "scrollbar_vertical", this.scrollbar_vertical, 1, 0.5);

		}		
	};
	
	
	
	
	this.desactivateScroll = function() {
		if (self.isTouchDevice) {
			UneakEvent.removeEventListener(self.page_content, "scroll", self.mainContentScroll);
	
		} else {
			UneakEvent.removeEventListener(document, self.mousewheelevt, self.scrollbarVerticalWheel);
			UneakEvent.removeEventListener(self.scrollbar_vertical_dragger_bar, "mousedown", self.scrollbarVerticalClick);
			UneakEvent.removeEventListener(self.scrollbar_vertical_dragger_rail, "mousedown", self.scrollbarVerticalRailClick);
			
			//this.scrollbar_vertical.style.display = "none";
			document.Tween.tween("scrollbar_vertical", "scrollbar_vertical", this.scrollbar_vertical, 0, 0.5);
		}	
	};
	
	
			
				

	
	
	
	
	this.updateDocument = function(root, parseLink) {
		root = root || document.body;
		
		if (self.hasClass(root, "stop-ajax-link")) parseLink = false;
		
		if (parseLink && this.xhr && root.tagName.toUpperCase() == "A") {
			if (root.getAttribute("href")) {
				if ((root.getAttribute("rel") && !root.getAttribute("rel").match(new RegExp('shadowbox'))) || (!root.getAttribute("rel") && !this.isExternal(root.getAttribute("href")))) {
					UneakEvent.addEventListener(root, 'click', this.onLinkClick);
				}
			}
		}
		
		if (!this.isTouchDevice && root.hasAttribute("title")) {
			if (root.className.match(new RegExp('(\\s|^)tooltip-keepalive(\\s|$)'))) {
				document.Tooltip.addTooltip(root, {keepAlive:true});
			} else {
				document.Tooltip.addTooltip(root);
			}
		}
		
		var children = root.children;
		for (var i = children.length; i--;) this.updateDocument(children[i], parseLink);
	};
	

	this.unBuild = function() {
		this.isBuild = false;
		
		while (this.rubrique_title.hasChildNodes()) this.rubrique_title.removeChild(this.rubrique_title.lastChild);
		while (this.rubrique_sous_title.hasChildNodes()) this.rubrique_sous_title.removeChild(this.rubrique_sous_title.lastChild);
		
		this.desactivateScroll();
		
		if (!this.isTouchDevice) {
			
			document.Follow.removeFollow("clone_col1_foreground_wrapper");
			document.Follow.removeFollow("clone_col2_foreground_wrapper");
			
			document.Tween.tweenValue("col1", document.Follow.followValue("scrollCol1Position"));
			document.Tween.tweenValue("col2", document.Follow.followValue("scrollCol2Position"));

			document.Tween.tween("col1", "clone_col1_foreground_wrapper", this.clone_col1_foreground_wrapper, -document.documentElement.clientHeight, 0.3, null, this.clearCols);
			document.Tween.tween("col2", "clone_col2_foreground_wrapper", this.clone_col2_foreground_wrapper, -document.documentElement.clientHeight, 0.1);
			
			clearInterval(this.checkHeightInterval);
			
		} else {
			this.main_content_node_wrapper.style.display = "none";
			this.clearCols();
		}
		
		
	};
	
	
	this.clearCols = function() {
		
		if (!self.isTouchDevice) {
			while (self.content_clone_wrapper.hasChildNodes()) self.content_clone_wrapper.removeChild(self.content_clone_wrapper.lastChild);
		}
		
		console.log("clearCols:"+self.currentUrl);
		self.xhr.open("GET", self.currentUrl, true);
		self.xhr.send();
		
		self.startLoading("MAIN");
	};
	

	//
	this.createLoading = function(id, layer) {
		if (layer) {
			var opts = {
				lines: 15, // The number of lines to draw
				length: 1, // The length of each line
				width: 3, // The line thickness
				radius: 7, // The radius of the inner circle
				corners: 1, // Corner roundness (0..1)
				rotate: 0, // The rotation offset
				color: '#CCC', // #rgb or #rrggbb
				speed: 2, // Rounds per second
				trail: 50, // Afterglow percentage
				shadow: false, // Whether to render a shadow
				hwaccel: false, // Whether to use hardware acceleration
				className: 'spinner', // The CSS class to assign to the spinner
				zIndex: 2e9, // The z-index (defaults to 2000000000)
				top: 'auto', // Top position relative to parent in px
				left: 'auto' // Left position relative to parent in px
			};
			
		} else {
			var opts = {
				lines: 15, // The number of lines to draw
				length: 4, // The length of each line
				width: 2, // The line thickness
				radius: 4, // The radius of the inner circle
				corners: 1, // Corner roundness (0..1)
				rotate: 0, // The rotation offset
				color: '#FFF', // #rgb or #rrggbb
				speed: 0.9, // Rounds per second
				trail: 50, // Afterglow percentage
				shadow: false, // Whether to render a shadow
				hwaccel: false, // Whether to use hardware acceleration
				className: 'spinner', // The CSS class to assign to the spinner
				zIndex: 2e9, // The z-index (defaults to 2000000000)
				top: 'auto', // Top position relative to parent in px
				left: 'auto' // Left position relative to parent in px
			};
		}
		
		var load_cont = document.createElement("div"); load_cont.className = 'loading-container loading-'+id;
		this.loading_animation_wrapper.appendChild(load_cont);
		load_cont.style.display = "none";
		//document.Tween.addTween("loading_"+id, 0, "f_opacity");
		
		this.loading[id] = [];
		this.loading[id].spinner = new Spinner(opts);
		this.loading[id].target = load_cont;		
	};


	this.startLoading = function(id) {
		//console.log("START:"+id);
		//document.Tween.tween("loading_"+id, "loading_"+id, this.loading[id].target, 1, 0.5);
		this.loading[id].target.style.removeProperty("display");
		this.loading[id].spinner.spin(this.loading[id].target);
	};

	this.stopLoading = function(id) {
		//console.log("START:"+id);
		//document.Tween.tween("loading_"+id, "loading_"+id, this.loading[id].target, 0, 0.5);
		this.loading[id].target.style.display = "none";
		this.loading[id].spinner.stop();
	};
	
	this.musicClick = function(event) {
		self.musicIsPlaying = !self.musicIsPlaying;
		if (self.musicIsPlaying) {
			self.audioElement.play();
		} else {
			self.audioElement.pause();
		}
		self.music.className = (self.musicIsPlaying) ? 'music on' : 'music off';
	}
	
	

	
	this.onDeviceMotionChange = function (event) {
		self.lastOrientation.gamma = event.accelerationIncludingGravity.x * 10;
		self.lastOrientation.beta = event.accelerationIncludingGravity.y * 10;
		
		halfWidth = document.documentElement.clientWidth * 0.5;
		halfHeight = document.documentElement.clientHeight * 0.5;
		if (self.lastOrientation.beta < -halfWidth) self.lastOrientation.beta = -halfWidth;
		if (self.lastOrientation.beta > halfWidth) self.lastOrientation.beta = halfWidth;
		if (self.lastOrientation.gamma < -halfHeight) self.lastOrientation.gamma = -halfHeight;
		if (self.lastOrientation.gamma > halfHeight) self.lastOrientation.gamma = halfHeight;

	}
	
	
	
	this.onDeviceOrientationChange = function(event) {
		self.lastOrientation.gamma = event.gamma * 20;
		self.lastOrientation.beta = event.beta * 20;
		
		halfWidth = document.documentElement.clientWidth * 0.5;
		halfHeight = document.documentElement.clientHeight * 0.5;
		if (self.lastOrientation.beta < -halfWidth) self.lastOrientation.beta = -halfWidth;
		if (self.lastOrientation.beta > halfWidth) self.lastOrientation.beta = halfWidth;
		if (self.lastOrientation.gamma < -halfHeight) self.lastOrientation.gamma = -halfHeight;
		if (self.lastOrientation.gamma > halfHeight) self.lastOrientation.gamma = halfHeight;
	}



	this.refreshOrientation = function(event) {
		switch (window.orientation) {
			case 0:
				var x = (document.documentElement.clientWidth / 2) - self.lastOrientation.gamma;
				var y = (document.documentElement.clientHeight / 2) - self.lastOrientation.beta;
				break;
			case 180:
				var x = (document.documentElement.clientWidth / 2) - self.lastOrientation.gamma * -1;
				var y = (document.documentElement.clientHeight / 2) - self.lastOrientation.beta * -1;
				break;
			case 90:
				var x = (document.documentElement.clientWidth / 2) - self.lastOrientation.beta;
				var y = (document.documentElement.clientHeight / 2) - self.lastOrientation.gamma * -1;
				break;
			case -90:
				var x = (document.documentElement.clientWidth / 2) - self.lastOrientation.beta * -1;
				var y = (document.documentElement.clientHeight / 2) - self.lastOrientation.gamma;
				break;
			default:
				var x = (document.documentElement.clientWidth / 2) - self.lastOrientation.gamma;
				var y = (document.documentElement.clientHeight / 2) - self.lastOrientation.beta;
		}
		
		document.Parallax.mouseMove(x, y);
	}

	

	this.mouseMove = function(event) {
		var event = window.event || event;
		document.Parallax.mouseMove(event.clientX, event.clientY);
		
		if (self.isBuild) {
			self.repere_y = event.clientY;
			self.setGroupVertical(document.Follow.followValue("scrollCol2Position"));
		}

	};
	
	
/*	
	this.cancelRootTouchStart = function(event) {
		var event = window.event || event;
		event.preventDefault();
	};
*/

	
	
		
	
//
//
//
//		SCROLLBAR VERTICAL
//
//
//
	

	
	this.scrollbarVerticalWheel = function(event) {
		
		var event = window.event || event;
		var delta = event.detail ? event.detail * -120/8 : event.wheelDelta/8;
		
		value = document.Follow.followValue("scrollCol2Position") - delta;
		
		if (value < 0) {
			value = 0;
		} else if (value > self.col2Size - document.documentElement.clientHeight) {
			value = self.col2Size - document.documentElement.clientHeight;
		}
		
		self.setVertical(value);
		event.returnValue = false;
	};
	
	
	
	this.scrollbarVerticalRailClick = function(event) {
		var event = window.event || event;
		if (event.stopPropagation) event.stopPropagation();
		event.cancelBubble = true;
		delta = 200;
		
		if (event.clientY - self.scrollbar_vertical_dragger_container_top < self.verticalDraggerPosition) {
			value = document.Follow.followValue("scrollCol2Position") - delta;
			if (value < 0) value = 0;
		} else {
			value = document.Follow.followValue("scrollCol2Position") + delta;
			if (value > self.col2Size - document.documentElement.clientHeight) value = document.documentElement.clientHeight;
		}
		
		self.setVertical(value);
	};
	
	this.scrollbarVerticalClick = function(event) {
		var event = window.event || event;
		if (event.stopPropagation) event.stopPropagation();
		event.cancelBubble = true;
		event.preventDefault();

		self.verticalGrabPoint = event.clientY - self.verticalDraggerPosition - self.scrollbar_vertical_dragger_container_top;
		UneakEvent.addEventListener(document, "mousemove", self.scrollbarVerticalDrag);
		UneakEvent.addEventListener(document, "mouseup", self.scrollbarVerticalStopDrag);
		return false;
	};
	
	
	this.scrollbarVerticalDrag = function(event) {
		var event = window.event || event;
		event.preventDefault();
		var pos = event.clientY - self.scrollbar_vertical_dragger_container_top - self.verticalGrabPoint;
		if (pos < 0) {
			pos = 0;
		} else if (pos > self.scrollbar_vertical_dragger_container_height - self.scrollbar_vertical_dragger_bar_height) {
			pos = (self.scrollbar_vertical_dragger_container_height - self.scrollbar_vertical_dragger_bar_height);
		}
		
		self.setVertical(pos * self.vertical_ratio);
	};
	
	
	this.scrollbarVerticalStopDrag = function(event) {
		var event = window.event || event;
		event.preventDefault();
		UneakEvent.removeEventListener(document, "mousemove", self.scrollbarVerticalDrag);
		UneakEvent.removeEventListener(document, "mouseup", self.scrollbarVerticalStopDrag);
	};


	
	
		
	
	this.setVertical = function(value) {
		if (!this.isTouchDevice) {
			document.Follow.follow("scrollCol2Position", value);
			this.verticalDraggerPosition = value / self.vertical_ratio;	
			document.Tween.tween("verticalDragger", "scrollbar_vertical_dragger_bar", this.scrollbar_vertical_dragger_bar, this.verticalDraggerPosition, 0.5);
		} else {
			this.page_content.scrollTop = value;
		}
		
		this.setGroupVertical(value);
		if (!this.isTouchDevice) this.refreshCol1Position(value);
		
		document.Parallax.offsetChange(-value * 10, 0);
		if (!this.isTouchDevice) document.Tooltip.deactiveTooltipip();
	};


	this.refreshCol1Position = function(value) {
		if (!this.isTouchDevice) {
			var element_col1fore = this.col1fore[this.currentRubrique][this.currentGroup][0];
			var element_col2fore = this.col2fore[this.currentRubrique][this.currentGroup][0];
			if (element_col1fore && element_col2fore) {
				col2_pos = element_col2fore.top - value;
				col1_pos = col2_pos - element_col1fore.top;
				document.Follow.follow("scrollCol1Position", -col1_pos);
			}
		}
	}


	this.setGroupVertical = function(value) {
		var screen_top = value;
		var screen_bottom = value + document.documentElement.clientHeight;
		var repere = value + this.repere_y;
	
		
		var rubrique = -1;
		var group = -1;
		var distance = 99999;
		
		var element;
		for(var r = self.col2fore.length-1; r >= 0 ; r--) { for(var g = self.col2fore[r].length-1; g >= 0 ; g--) { for(var e = self.col2fore[r][g].length-1; e >= 0 ; e--) {
			
			element = self.col2fore[r][g][e];
			element_top = element.top;
			element_bottom = element.top + element.height;
			
			
			if (element_bottom >= screen_top || element_top <= screen_bottom) { 
				
				if (repere >= element_top && repere <= element_bottom) {
					this.setGroup(r, g);
					return;
				}
				
				distance_top = Math.abs(repere - element_top);
				distance_bottom = Math.abs(repere - element_bottom);
				current_distance = (distance_top < distance_bottom) ? distance_top : distance_bottom;
				
				if (current_distance < distance) {
					rubrique = r;
					group = g;
					distance = current_distance;
				} else {
					break;
				}
			}
			
			if (element_top > screen_bottom) break;
			
		} } }
		
		if (rubrique != -1 && group != -1) this.setGroup(rubrique, group);	

	};
//
//
//
//
//



	this.mainContentScroll = function(event) {
		document.Parallax.offsetChange(-self.page_content.scrollTop, 0);
		self.setGroupVertical(self.page_content.scrollTop);
	}







//
//
//
//
//

	
	
	this.checkHeight = function() {
		if (self.clone_col2_foreground_wrapper.offsetHeight != self.col2Size) self.refreshCloneColSize();
	}
	
	
	
	this.refreshCloneColSize = function() {

		if (self.presentCol == 2) {	

			for(var r = 0; r < self.col2fore.length; r++) { for(var g = 0; g < self.col2fore[r].length; g++) { for(var e = 0; e < self.col2fore[r][g].length; e++) {
				
				if (self.col1fore[r][g][e]) {
					self.col1fore[r][g][e].top = self.getTopPosition(self.col1fore[r][g][e].clone, self.col1root);
					self.col1fore[r][g][e].height = self.col1fore[r][g][e].clone.offsetHeight;
					
				}
				
				if (self.col2fore[r][g][e]) {
					if (!self.isTouchDevice) {
						self.col2fore[r][g][e].clone.style.removeProperty("padding-top");
						if (self.col1fore[r][g][e]) self.col2fore[r][g][e].clone.children[0].style.minHeight = self.col1fore[r][g][e].height + "px";
					}
					self.col2fore[r][g][e].top = self.getTopPosition(self.col2fore[r][g][e].clone, self.col2root);
					self.col2fore[r][g][e].height = self.col2fore[r][g][e].clone.offsetHeight;
	
				}
				
			} } }
			
		} else {
		
			
			var padding_col2 = 0;
			for(var r = 0; r < self.col2fore.length; r++) { for(var g = 0; g < self.col2fore[r].length; g++) { for(var e = 0; e < self.col2fore[r][g].length; e++) {
				
				if (self.col1fore[r][g][e]) {
					self.col1fore[r][g][e].top = self.getTopPosition(self.col1fore[r][g][e].clone, self.col1root);
					self.col1fore[r][g][e].height = self.col1fore[r][g][e].clone.offsetHeight;
		
					padding_col2 = self.col1fore[r][g][e].height;
					
				} else {
					padding_col2 = 0;
					
				}
				
				if (self.col2fore[r][g][e]) {
					if (!self.isTouchDevice) {
						self.col2fore[r][g][e].clone.style.paddingTop = padding_col2 + "px";
						if (self.col1fore[r][g][e]) self.col2fore[r][g][e].clone.children[0].style.removeProperty("min-height");
					}
					self.col2fore[r][g][e].top = self.getTopPosition(self.col2fore[r][g][e].clone, self.col2root);
					self.col2fore[r][g][e].height = self.col2fore[r][g][e].clone.offsetHeight;
				}
				
			} } }
			
			
			
		}
		
		self.refreshScrollbarVertical(self.col2root.offsetHeight);
	}
	
	
	
	
	
	
	

	

	this.refreshScrollbarVertical = function(h) {
		
		if (!self.isTouchDevice) {
			var new_pos = (this.col2Size) ? h * (document.Follow.followValue("scrollCol2Position") / this.col2Size) : 0;
			
			this.scrollbar_vertical_dragger_container_top = (document.documentElement.clientHeight * 10 / 100) ;
			this.scrollbar_vertical_dragger_container_height = (document.documentElement.clientHeight * 80 / 100) ;
			this.scrollbar_vertical_dragger_bar_height = ((document.documentElement.clientHeight / h) * this.scrollbar_vertical_dragger_container_height) ;
			this.vertical_ratio = (h - document.documentElement.clientHeight) / (this.scrollbar_vertical_dragger_container_height - this.scrollbar_vertical_dragger_bar_height);
	
			this.scrollbar_vertical_dragger_container.style.top = this.scrollbar_vertical_dragger_container_top + "px";
			this.scrollbar_vertical_dragger_container.style.height = this.scrollbar_vertical_dragger_container_height + "px";
			this.scrollbar_vertical_dragger_bar.style.height = this.scrollbar_vertical_dragger_bar_height + "px";
		} else {
		
			var new_pos = (this.col2Size) ? h * (this.page_content.scrollTop / this.col2Size) : 0;
		}
	
		if (h <= document.documentElement.clientHeight && this.scrollbar_vertical) this.desactivateScroll(); else this.activateScroll();
		this.col2Size = h;		
		this.setVertical(new_pos);
		
	}
	
	
	
	
	
	
	
	this.resfreshResize = function(event) {
		//var event = window.event || event;
		self.presentCol = (document.documentElement.clientWidth >= 800) ? 2 : 1;
		if (self.isTouchDevice) self.repere_y = document.documentElement.clientHeight / 2;
		self.refreshCloneColSize();
		document.Parallax.screenResize(document.documentElement.clientWidth, document.documentElement.clientHeight);
	};




		
	
	


	
	
	
	
		
	
	this.setGroup = function(rubriqueId, groupId) {
		if (this.currentRubrique == rubriqueId && this.currentGroup == groupId) return;

		var selected_col1_foreground = null;
		var selected_col2_foreground = null;
		
		if (this.currentRubrique != rubriqueId) {
			this.currentRubrique = rubriqueId;
			this.currentGroup = -1;
		}
		
		if (this.currentGroup != groupId) {
			this.currentGroup = groupId;
			
			if (!this.isTouchDevice) this.refreshCol1Position(document.Follow.followValue("scrollCol2Position"));
			this.refreshOpacity(this.col1fore, rubriqueId, groupId);
			
			for (var i in this.elementData[rubriqueId][groupId]["layer"]) document.Parallax.setNextLayer(i, this.elementData[rubriqueId][groupId]["layer"][i]);
	
			document.Tween.tween("colorR", "background_sky_color", this.background_sky_color, parseInt(this.elementData[rubriqueId][groupId]["color"].r), 0.1);
			document.Tween.tween("colorG", "background_sky_color", this.background_sky_color, parseInt(this.elementData[rubriqueId][groupId]["color"].g), 0.1);
			document.Tween.tween("colorB", "background_sky_color", this.background_sky_color, parseInt(this.elementData[rubriqueId][groupId]["color"].b), 0.1);
			
		}
		
		
	};
	
	
	
	
	this.refreshOpacity = function(array, rId, gId) {
		for(var r = array.length-1; r >= 0; r--) { for(var g = array[r].length-1; g >= 0; g--) { for(var e = array[r][g].length-1; e >= 0; e--) {
			if (array[r][g][e].clone) {
				if (r == rId && g == gId) {
					array[r][g][e].clone.classList.add('over');
					array[r][g][e].clone.classList.remove('out');
				} else {
					array[r][g][e].clone.classList.remove('over');
					array[r][g][e].clone.classList.add('out');
				}
			}
		} } }
	}
	
	
	
		
	
	this.loadCover = function() {
		// TODO SET COVER
	};
	
	

	
	
	this.onLoadingHide = function(object) {
//		object.element.style.display = "none";
		if (object.complete) object.target.complete(self, object.target);
	};
	

	
	
	
	
	this.animate = function() {
		if (!self.isTouchDevice) document.Follow.refresh();
		document.Tween.refresh();
		document.Parallax.refresh();	
		requestAnimationFrame(self.animate, self);
	};
	
	
	

	this.initNavigation = function() {
		if (window.XMLHttpRequest || window.ActiveXObject) {
			if (window.ActiveXObject) {
				try {
					this.xhr = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {
					this.xhr = new ActiveXObject("Microsoft.XMLHTTP");
				}
			} else {
				this.xhr = new XMLHttpRequest(); 
			}
			
			UneakEvent.addEventListener(self.xhr, 'readystatechange', self.onreadystatechange);
			//	HISTORY		
			History.Adapter.bind(window, 'statechange', self.onUrlStatechange);
			History.Adapter.bind(window, 'anchorchange', self.onUrlAnchorchange);
			
		} else {
			//alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
			//return null;
		}
		
		//return xhr;
	}
	
	this.onLinkClick = function(event) {
		event.preventDefault();
		History.pushState(null, null, this.getAttribute("href"));
	};
	
	this.onUrlStatechange = function(event) {
		var url = History.getState().url;
		console.log("onUrlStatechange:"+url);
		self.loadPage(url);
	};	
		
	this.onUrlAnchorchange = function(event) {
		console.log("onUrlAnchorchange:"+url);
		self.loadPage(History.getHash());
	};		
	
	this.loadPage = function(url) {
		if (!self.isTouchDevice) document.Tooltip.deactiveTooltipip();
		self.currentUrl = url;
		if (self.xhr && self.xhr.readyState != 0) self.xhr.abort();
		self.unBuild();
	};
	
	this.onreadystatechange = function(event) {
		if (self.xhr.readyState == 4 && (self.xhr.status == 200 || self.xhr.status == 0)) {
			self.parseData(self.xhr.responseText);
		}
	};

	this.parseData = function(data) {
		
		if (data.match(new RegExp('<title>((.|\r|\n)*?)</title>'))) document.title = RegExp.$1;
		if (data.match(new RegExp('<!-- BEGIN:TOP_MENU -->((.|\r|\n)*?)<!-- END:TOP_MENU -->'))) this.top_menu_wrapper.innerHTML = RegExp.$1;
		if (data.match(new RegExp('<!-- BEGIN:BREADCRUMB -->((.|\r|\n)*?)<!-- END:BREADCRUMB -->'))) this.breadcrumb_container.innerHTML = RegExp.$1; else this.breadcrumb_container.innerHTML = "";
		if (data.match(new RegExp('<!-- BEGIN:MAIN_CONTENT -->((.|\r|\n)*?)<!-- END:MAIN_CONTENT -->'))) this.main_content_node.innerHTML = RegExp.$1;
		if (data.match(new RegExp('<!-- BEGIN:COVER -->((.|\r|\n)*?)<!-- END:COVER -->'))) this.cover_image.innerHTML = RegExp.$1;
		if (data.match(new RegExp('<!-- BEGIN:TITLE -->((.|\r|\n)*?)<!-- END:TITLE -->'))) this.rubrique_title.innerHTML = RegExp.$1;
		if (data.match(new RegExp('<!-- BEGIN:BASELINE -->((.|\r|\n)*?)<!-- END:BASELINE -->'))) this.rubrique_sous_title.innerHTML = RegExp.$1;
		if (data.match(new RegExp('<!-- BEGIN:FOOTER -->((.|\r|\n)*?)<!-- END:FOOTER -->'))) this.footer_container.innerHTML = RegExp.$1;
		
		if (data.match(new RegExp('<!-- BEGIN:SCRIPT_EVAL -->((.|\r|\n)*?)<!-- END:SCRIPT_EVAL -->'))) {
			var script = RegExp.$1;
			
		}



		// Inform Google Analytics of the change
		var url = History.getState().url;
		if ( typeof window._gaq !== 'undefined' ) {
			relativeUrl = url.replace(History.getRootUrl(),'');
			window._gaq.push(['_trackPageview', relativeUrl]);
		}
		
		
		this.build();
	}
	
	
	
	this.WorkerMessage = function(cmd, parameter) {
		this.cmd = cmd;
		this.p = parameter;
	}	
	


	
	//
	//	UTILS
	//
	
	
	

	this.getTopPosition = function(e, root) {
        var top = 0;
        while (e.offsetParent && e != root) {
			top += (e.offsetTop - e.scrollTop + e.clientTop);
			e = e.offsetParent;
        }
        return top;
	}
	
	
	
	
	this.getImageFromCssID = function(cssID) {
		var loadingAnim = document.createElement("div"); loadingAnim.className = cssID;
		this.background_sky_container.appendChild(loadingAnim);
		var cs = document.defaultView.getComputedStyle(loadingAnim, null);
		var image = new Image();
		image.src = cs.getPropertyValue("background-image").replace(/(^url\()|(\)$)/g, "");
		this.background_sky_container.removeChild(loadingAnim);
		return image;
	};
	
	this.isExternal = function(url) {
		var match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);
		if (typeof match[1] === "string" && match[1].length > 0 && match[1].toLowerCase() !== location.protocol) return true;
		if (typeof match[2] === "string" && match[2].length > 0 && match[2].replace(new RegExp(":("+{"http:":80,"https:":443}[location.protocol]+")?$"), "") !== location.host) return true;
		return false;
	}
	
	this.hasClass = function(ele,cls) {
		return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
	}
	 
	this.addClass = function(ele,cls) {
		if (!this.hasClass(ele,cls)) ele.className += " "+cls;
	}
	 
	this.removeClass = function(ele,cls) {
		if (this.hasClass(ele,cls)) {
			var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
			ele.className=ele.className.replace(reg,' ');
		}
	}
	
	this.getElementByClass = function(ele, cls) {
		if (!ele) return null;
		cls_string = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		var nb = ele.children.length-1;
		for(var k = nb; k >= 0; k--) {
			if (ele.children[k].className.match(cls_string)) return ele.children[k];
		}
		return null;
	}
	
	
	this.boot(theme_path);
};