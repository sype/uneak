
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */


layers = [];

mousePosition = {x:0, y:0};
mouseAnimPosition = {x:0, y:0};
offsetPosition = {x:0, y:0};
offsetAnimPosition = {x:0, y:0};
screenSize = {x:0, y:0};


interval = setInterval(self.transitionMouse, 20);

function transitionMouse(event) {
	var diff = self.mousePosition.x - self.mouseAnimPosition.x;
	if (Math.abs(diff) < 0.1) self.mouseAnimPosition.x = self.mousePosition.x; else self.mouseAnimPosition.x = self.mouseAnimPosition.x + diff * 0.05;
	
	var diff = self.mousePosition.y - self.mouseAnimPosition.y;
	if (Math.abs(diff) < 0.1) self.mouseAnimPosition.y = self.mousePosition.y; else self.mouseAnimPosition.y = self.mouseAnimPosition.y + diff * 0.05;
	
	var diff = self.offsetPosition.x - self.offsetAnimPosition.x;
	if (Math.abs(diff) < 0.1) self.offsetAnimPosition.x = self.offsetPosition.x; else self.offsetAnimPosition.x = self.offsetAnimPosition.x + diff * 0.05;
	
	var diff = self.offsetPosition.y - self.offsetAnimPosition.y;
	if (Math.abs(diff) < 0.1) self.offsetAnimPosition.y = self.offsetPosition.y; else self.offsetAnimPosition.y = self.offsetAnimPosition.y + diff * 0.05;
	
	for( var i = self.layers.length-1; i >= 0; i-- ) self.refreshStyle(self.layers[i]);
}


function messageHandler(event) {
	var m = event.data;

	//this.postMessage(new self.WorkerMessage('console', m.cmd));

	switch (m.cmd) {
			
		case 'mouseMove':
            self.mouseMove(m.p.x, m.p.y);
            break;
			
        case 'offsetChange':
			self.offsetChange(m.p.x, m.p.y);
            break;
		
		case 'screenResize':
			self.screenResize(m.p.x, m.p.y);
			break;
		
		case 'createLayer':
//			this.postMessage(new self.WorkerMessage('console', m.cmd));
			self.createLayer(m.p);
            break;

        case 'setNextLayer':
//			this.postMessage(new self.WorkerMessage('console', m.cmd));
            self.setNextLayer(m.p.id, m.p.data);
            break;
		
		case 'onNextLayerLoaded':
//			this.postMessage(new self.WorkerMessage('console', m.cmd));
            self.onNextLayerLoaded(m.p);
            break;
		
		case 'newLayer':
//			this.postMessage(new self.WorkerMessage('console', m.cmd));
            self.newLayer(m.p);
            break;
			
		case "process":
			self.process();
			break;
			

    }
	
}

function createLayer(parameters) {
	self.layers.push(parameters);
}


function mouseMove(x, y) {
	self.mousePosition.x = x;
	self.mousePosition.y = y;
}

function offsetChange(x, y) {
	self.offsetPosition.x = x;
	self.offsetPosition.y = y;
}

function screenResize(x, y) {
	self.screenSize.x = x;
	self.screenSize.y = y;
	for( var i = self.layers.length-1; i >= 0; i-- ) self.refreshParallaxPosition(self.layers[i]);
}

			
function setNextLayer(id, data) {	// SET NEXT LAYER
	var layer; for( var i = self.layers.length-1; i >= 0; i-- ) { if (self.layers[i].id == id) { layer = self.layers[i]; break; } }

//	this.postMessage(new self.WorkerMessage('console', 'setNextLayer id:'+id));
//	this.postMessage(new self.WorkerMessage('console', layer));

	var dataStringify = JSON.stringify(data);
	if ((layer.next.stringify && layer.next.stringify == dataStringify) || (!layer.next.stringify && layer.stringify == dataStringify)) return;

	layer.next.stringify = dataStringify;
	layer.next.data = data;
	layer.process = false;
	
	this.postMessage(new self.WorkerMessage("loadLayer", {id:id, src:(data.background) ? data.background : null}));
}


function onNextLayerLoaded(id) {	// NEXT LAYER IS LOADING
	var layer; for( var i = self.layers.length-1; i >= 0; i-- ) { if (self.layers[i].id == id) { layer = self.layers[i]; break; } }
	
//	this.postMessage(new self.WorkerMessage('console', 'onNextLayerLoaded id:'+id));
//	this.postMessage(new self.WorkerMessage('console', layer));
	
	layer.process = true;
	self.process();
}


function process() {
	for( var i = self.layers.length-1; i >= 0; i-- ) {
		if (self.layers[i].process == true) {
			self.layers[i].process = false;
			if (self.layers[i].next.data) {
				this.postMessage(new self.WorkerMessage("refreshLayer", self.layers[i].id));
				return;
			}
		}
	}
}


function newLayer(id) {
	var layer; for( var i = self.layers.length-1; i >= 0; i-- ) { if (self.layers[i].id == id) { layer = self.layers[i]; break; } }
	
	layer.stringify = layer.next.stringify;
	layer.data = layer.next.data;
	layer.track = [];
	layer.style = {};
	layer.style["marginLeft"] = "0px";
	layer.style["marginTop"] = "0px";
	layer.style["containerMarginLeft"] = "0px";
	layer.style["containerMarginTop"] = "0px";
	layer.next.stringify = "";
	layer.next.data = null;
	
	if (layer.data.background) {
		var func;
			
		if (layer.data.repeat != "no-repeat") {
			layer.data.type = layer.data.repeat;
			
			if (layer.data.offset == "horizontal") {
				layer.track.update = self.update_offsetHorizontal_byBackground;
			} else if (layer.data.offset == "vertical") {
				layer.track.update = self.update_offsetVertical_byBackground;
			} else {
				layer.track.update = self.update_offsetNone_byBackground;
			}
			
		} else {
			layer.data.type = "img";
			
			if (layer.data.offset == "horizontal") {
				layer.track.update = self.update_offsetHorizontal_byMargin;
			} else if (layer.data.offset == "vertical") {
				layer.track.update = self.update_offsetVertical_byMargin;
			} else {
				layer.track.update = self.update_offsetNone_byMargin;
			}
			
		}
	} else {
		layer.data.type = "none";
		layer.track.update = this.update_layer_container;
	}
	
	this.postMessage(new self.WorkerMessage("newLayer", {id:id, type:layer.data.type}));
	this.postMessage(new self.WorkerMessage("styleLayer", {id:layer.id, style:layer.style}));
	self.refreshParallaxPosition(layer);
};



function refreshParallaxPosition(layer) {
	if (layer.data && layer.data.type != "none") {
		
		var style = {};
		
		var w = layer.data.width;
		var h = layer.data.height;

		var margeX = self.screenSize.x / layer.data.distance;
		var margeY = self.screenSize.y / layer.data.distance;
		
		var half_margeX = ((self.screenSize.x / layer.data.distance)  / 2);
		var half_margeY = ((self.screenSize.y / layer.data.distance)  / 2);
		
		layer.offset.x = 0;
		layer.offset.y = 0;
		
		if (layer.data.size == "cover") {

			var ratioW = self.screenSize.x / w;
			var ratioH = self.screenSize.y / h;
			var ratio = (ratioH < ratioW) ? ratioW : ratioH;
			
			var sizeW = (w * ratio + self.screenSize.x / layer.data.distance) ;
			var sizeH = (h * ratio + self.screenSize.y / layer.data.distance) ;

			if (layer.data.type == "img") {
				style.width = Math.round(sizeW) + "px";
				style.height = Math.round(sizeH) + "px";
				
				if (layer.data.align_vertical == "top") {
					layer.offset.y = -half_margeY;
				} else if (layer.data.align_vertical == "bottom") {
					layer.offset.y = self.screenSize.y - sizeH - half_margeY;
				} else {
					layer.offset.y = Math.round((self.screenSize.y - sizeH) / 2);
				}
				
				if (layer.data.align_horizontal == "left") {
					layer.offset.x = -half_margeX;
				} else if (layer.data.align_horizontal == "right") {
					layer.offset.x = self.screenSize.x - sizeW - half_margeX;
				} else {
					layer.offset.x = Math.round((self.screenSize.x - sizeW) / 2);
				}
			} else {
				style.width = (self.screenSize.x + margeX) + "px";
				style.height = (self.screenSize.y + margeY) + "px";
				layer.offset.x = -half_margeX;
				layer.offset.y = -half_margeY;
				
				valign = "center";
				if (layer.data.align_vertical == "top") {
					valign = "top";
				} else if (layer.data.align_vertical == "bottom") {
					valign = "bottom";
				}
				
				align = "center";
				if (layer.data.align_horizontal == "left") {
					align = "left";
				} else if (layer.data.align_horizontal == "right") {
					align = "right";
				}
				
				style.backgroundSize = Math.round(sizeW)+"px "+Math.round(sizeH)+"px";
				style.backgroundPosition = align+" "+valign;
			}
		} else {
		
			var ratio = 1;
			var sizeW = (w + self.screenSize.x / layer.data.distance) ;
			var sizeH = (h + self.screenSize.y / layer.data.distance) ;
			
			if (layer.data.type == "img") {
				style.width = w + "px";
				style.height = h + "px";
			} else {
				if (layer.data.repeat == "repeat-x") {
					style.width = "100%";
					style.height = Math.round(sizeH) + "px";
				} else if (layer.data.repeat == "repeat-y") {
					style.width = Math.round(sizeW) + "px";
					style.height = "100%";
				}
			}
			
			if (layer.data.align_vertical == "top") {
				layer.offset.y = -half_margeY;
				style.top = "0px";
			} else if (layer.data.align_vertical == "bottom") {
				layer.offset.y = half_margeY;
				style.top = (self.screenSize.y - h) + "px";
			} else {
				style.top = Math.round((self.screenSize.y - h) / 2)  + "px";
			}
			
			if (layer.data.align_horizontal == "left") {
				layer.offset.x = -half_margeX;
				style.left = "0px";
			} else if (layer.data.align_horizontal == "right") {
				layer.offset.x = half_margeX;
				style.left = (self.screenSize.x - w) + "px";
			} else {
				style.left = Math.round((self.screenSize.x - w) / 2)  + "px";
			}
				
		}
		
		
		
		var dispatchedStyle = {};
		
		var change = false;
		for (i in style) {
			if (!layer.style[i] || style[i] != layer.style[i]) {
				layer.style[i] = style[i];
				dispatchedStyle[i] = style[i];
				change = true;
			}
		}
		if (change) this.postMessage(new self.WorkerMessage("styleLayer", {id:layer.id, style:dispatchedStyle}));
	
	}
	
}



function refreshStyle(layer) {
	if (layer.track.update) {
		var mouseX = self.mouseAnimPosition.x - (self.screenSize.x / 2);
		var mouseY = self.mouseAnimPosition.y - (self.screenSize.y / 2);
		var style = layer.track.update(layer, mouseX, mouseY);
		var dispatchedStyle = {};		
		var change = false;
		for (i in style) {
			if (!layer.style[i] || style[i] != layer.style[i]) {
				layer.style[i] = style[i];
				dispatchedStyle[i] = style[i];
				change = true;
			}
		}
		if (change) this.postMessage(new self.WorkerMessage("styleLayer", {id:layer.id, style:dispatchedStyle}));
	}
}



function update_offsetHorizontal_byBackground(layer, mouseX, mouseY) {
	layer_posX = Math.round(layer.offset.x + ((self.offsetAnimPosition.x - mouseX) / layer.data.distance)) ;
	layer_posY = Math.round(layer.offset.y + ((self.offsetAnimPosition.y - mouseY) / layer.data.distance)) ;
	return {backgroundPosition:layer_posX + 'px ' + layer_posY + 'px'};
};

function update_offsetVertical_byBackground(layer, mouseX, mouseY) {
	layer_posX = Math.round(layer.offset.x + ((self.offsetAnimPosition.y - mouseX) / layer.data.distance)) ;
	layer_posY = Math.round(layer.offset.y + ((self.offsetAnimPosition.x - mouseY) / layer.data.distance)) ;
	return {backgroundPosition:layer_posX + 'px ' + layer_posY + 'px'};
};

function update_offsetNone_byBackground(layer, mouseX, mouseY) {
	layer_posX = Math.round(layer.offset.x - (mouseX / layer.data.distance)) ;
	layer_posY = Math.round(layer.offset.y - (mouseY / layer.data.distance)) ;
	return {backgroundPosition:layer_posX + 'px ' + layer_posY + 'px'};
};

function update_offsetHorizontal_byMargin(layer, mouseX, mouseY) {
	layer_posX = Math.round(layer.offset.x + ((self.offsetAnimPosition.x - mouseX) / layer.data.distance)) ;
	layer_posY = Math.round(layer.offset.y + ((self.offsetAnimPosition.y - mouseY) / layer.data.distance)) ;
	return {marginLeft:layer_posX + 'px', marginTop:layer_posY + 'px'};
};

function update_offsetVertical_byMargin(layer, mouseX, mouseY) {
	layer_posX = Math.round(layer.offset.x + ((self.offsetAnimPosition.y - mouseX) / layer.data.distance)) ;
	layer_posY = Math.round(layer.offset.y + ((self.offsetAnimPosition.x - mouseY) / layer.data.distance)) ;
	return {marginLeft:layer_posX + 'px', marginTop:layer_posY + 'px'};
};

function update_offsetNone_byMargin(layer, mouseX, mouseY) {
	layer_posX = Math.round(layer.offset.x - (mouseX / layer.data.distance)) ;
	layer_posY = Math.round(layer.offset.y - (mouseY / layer.data.distance)) ;
	return {marginLeft:layer_posX + 'px', marginTop:layer_posY + 'px'};
};

function update_layer_container(layer, mouseX, mouseY) {
	layer_posX = Math.round(layer.offset.x - (mouseX / layer.data.distance)) ;
	layer_posY = Math.round(layer.offset.y - (mouseY / layer.data.distance)) ;
	return {containerMarginLeft:layer_posX + 'px', containerMarginTop:layer_posY + 'px'};
};




function WorkerMessage(cmd, parameter) {
	this.cmd = cmd;
	this.p = parameter;
}	

//
this.addEventListener('message', messageHandler, false);

this.postMessage(new self.WorkerMessage('ready'));