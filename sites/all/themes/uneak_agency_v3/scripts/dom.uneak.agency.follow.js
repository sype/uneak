
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

function UneakAgencyFollow(theme_path, uneakAgency) {

	var self = this;
	this.isWorkers = Modernizr.webworkers;
	this.uneakAgency = uneakAgency;
	
	this.follow_list = [];
	this.follow_value = [];
	
	
	//
	//
	this.boot = function(theme_path) {
		
		setTimeout( function() {
			if (self.isWorkers) {
				self.followWorker = new Worker(theme_path+"/scripts/dom.uneak.agency.follow.worker.js");
				UneakEvent.addEventListener(self.followWorker, "message", self.followWorkerMessageHandler);
			} else {
				self.followProcess = new UneakAgencyFollowProcess(self);
				self.uneakAgency.followReady();
			}
		},20);
		
		
		
	};
	
	
	this.followWorkerMessageHandler = function(event) {
		var m = event.data;
		
		switch (m.cmd) {
			case 'ready':
				self.uneakAgency.followReady();
				break;
				
			case 'styleLayer':
				self.styleLayer(m.p.target_id, m.p.style);
				break;
				
			case 'console':
				console.log(m.p);
				break;
		}
		
	};
	
	this.styleLayer = function(target_id, style) {
		for( var i = self.follow_list.length-1; i >= 0; i-- ) if (self.follow_list[i].target_id == target_id) {self.follow_list[i].style = UneakEvent.concatObject(self.follow_list[i].style, style); break;}
	};
	
		
	this.addFollow = function(target_id, target, follow_id, init_val, function_id, speed) {
		this.follow_value[follow_id] = init_val;
		this.follow_list.push({target_id:target_id, target:target, value:init_val, style:{}});
		
		if (this.isWorkers) {
			this.followWorker.postMessage(new this.WorkerMessage('addFollow', {target_id:target_id, follow_id:follow_id, init_val:init_val, function_id:function_id, speed:speed}));
		} else {
			this.followProcess.addFollow(target_id, follow_id, init_val, function_id, speed);
		}
	
	};
	
	this.removeFollow = function(target_id) {
		for( var i = self.follow_list.length-1; i >= 0; i-- ) if (self.follow_list[i].target_id == target_id) self.follow_list.splice(i, 1);
		
		if (this.isWorkers) {
			self.followWorker.postMessage(new self.WorkerMessage('removeFollow', target_id));
		} else {
			this.followProcess.removeFollow(target_id);
		}
	};
	
	this.follow = function(follow_id, value) {
		this.follow_value[follow_id] = value;
		
		if (this.isWorkers) {
			this.followWorker.postMessage(new this.WorkerMessage('follow', {follow_id:follow_id, value:value}));
		} else {
			this.followProcess.follow(follow_id, value);
		}
	};
	
	this.followValue = function(follow_id) {
		return this.follow_value[follow_id];
	};
	
	this.refresh = function() {
		for( var i = this.follow_list.length-1; i >= 0; i-- ) {
			for (var style in this.follow_list[i].style) {
//				console.log(this.follow_list[i].target_id + " : " + this.follow_list[i].style[style]);
				this.follow_list[i].target.style[style] = this.follow_list[i].style[style];
			}
			this.follow_list[i].style = {};
		}
	};
	
	this.WorkerMessage = function(cmd, parameter) {
		this.cmd = cmd;
		this.p = parameter;
	}	
	
	this.boot(theme_path);
}; 







/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

function UneakAgencyFollowProcess(uneakAgencyFollow) {

	var self = this;
	this.uneakAgencyFollow = uneakAgencyFollow;
	
	this.follow_list = [];
	this.follow_value = [];
	this.target_value = [];
	this.target_style = {};

	
	this.interval = setInterval(function(){self.transitionFollow()}, 20);
	
	this.transitionFollow = function(event) {
		
		for( var i = self.follow_list.length-1; i >= 0; i-- ) {
			if (self.target_value[self.follow_list[i].target_id] != self.follow_value[self.follow_list[i].follow_id]) {
				diff = self.follow_value[self.follow_list[i].follow_id] - self.target_value[self.follow_list[i].target_id];
				if (Math.abs(diff) < 0.1) {
					self.target_value[self.follow_list[i].target_id] = self.follow_value[self.follow_list[i].follow_id];
				} else {
					self.target_value[self.follow_list[i].target_id] = self.target_value[self.follow_list[i].target_id] + diff * self.follow_list[i].speed;
				}
				self.follow_list[i].func(self.follow_list[i].target_id, self.target_value[self.follow_list[i].target_id]);
			}
		}
		
	}
	
	this.follow = function(follow_id, value) {
		this.follow_value[follow_id] = value;
	}
	
	this.removeFollow = function(target_id) {
		for( var i = this.follow_list.length-1; i >= 0; i-- ) if (this.follow_list[i].target_id == target_id) this.follow_list.splice(i, 1);
	}

	this.addFollow = function(target_id, follow_id, init_val, function_id, speed) {
		self.follow_value[follow_id] = init_val;
		self.target_value[target_id] = init_val;
		self.target_style[target_id] = {};
		self.follow_list.push({target_id:target_id, follow_id:follow_id, func:self[function_id], speed:speed});
	};
	
	
	
	
	//
	this.f_4deg = function(target_id, value) {
		var style = {};
		style.left = -Math.round(value) + 'px';
		style.top = Math.round(value * 0.0676470588235) + 'px';
		self.styleLayer(target_id, style);
	}	
	
	this.f_top_negatif = function(target_id, value) {
		var style = {};
		style.top = -Math.round(value) + 'px';
		self.styleLayer(target_id, style);
	}
	
	this.f_4deg_top_negatif = function(target_id, value) {
		var style = {};
		style.left = '0px';
		style.top = -Math.round(value) + 'px';
		self.styleLayer(target_id, style);
	}	
	
	
	
	this.styleLayer = function(target_id, style) {
		var dispatchedStyle = {};		
		var change = false;
		for (i in style) {
			if (!self.target_style[target_id][i] || style[i] != self.target_style[target_id][i]) {
				self.target_style[target_id][i] = style[i];
				dispatchedStyle[i] = style[i];
				change = true;
			}
		}
		
		if (change) this.uneakAgencyFollow.styleLayer(target_id, dispatchedStyle);
	}	
	
}; 