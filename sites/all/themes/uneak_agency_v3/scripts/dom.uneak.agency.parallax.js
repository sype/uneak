
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

function UneakAgencyParallax(theme_path, uneakAgency) {

	var self = this;
	this.isWorkers = Modernizr.webworkers;
	this.uneakAgency = uneakAgency;
	
	this.layers = [];
	this.layerUID = 0;
	
	
	//
	//
	this.boot = function(theme_path) {
		
		
		setTimeout( function() {
			if (self.isWorkers) {
				self.parallaxWorker = new Worker(theme_path+"/scripts/dom.uneak.agency.parallax.worker.js");
				UneakEvent.addEventListener(self.parallaxWorker, "message", self.parallaxWorkerMessageHandler);
			} else {
				self.parallaxProcess = new UneakAgencyParallaxProcess(self);
				self.uneakAgency.parallaxReady();
			}
		},20);
		
		
		
	};
	
	this.getLayerUID = function() {
		return this.layerUID++;
	}
	
	this.parallaxWorkerMessageHandler = function(event) {
		var m = event.data;
		
//		console.log(m.cmd);
		
		switch (m.cmd) {
			case 'ready':
				self.uneakAgency.parallaxReady();
				break;
				
			case 'loadLayer':
				self.loadLayer(m.p.id, m.p.src);
				break;
				
			case 'refreshLayer':
				self.refreshLayer(m.p);
				break;
				
			case 'newLayer':
//				console.log('newLayer '+m.p.id);
				self.newLayer(m.p.id, m.p.type);
				break;
				
			case 'styleLayer':
				//console.log('styleLayer '+m.p.id);
//				console.log(m.p.style);
				self.styleLayer(m.p.id, m.p.style);
				break;
								
			case 'console':
				console.log(m.p);
				break;
		}
		
	};
	
	
	this.createLayer = function(container, id, loading) {
//		console.log("createLayer");
		// REMOVE EXISTING BAS DOCUMENT HTML LAYER
		var numChild = container.children.length;
		for (var j = numChild-1; j >= 0; j--) {
			if (container.children[j] && container.children[j].className.match(new RegExp('(\\s|^)layer(\\s|$)'))) container.removeChild(container.children[j]);
		}
		
		//	PUSH LAYER DATA
		var index = this.layers.push({id:id, layer_container:container, layer:null, style:{}, tween:[], loading:new Image()}) - 1;
		this.layers[index].loading.id = id;
		this.layers[index].loading.nextSrc = -1;
		this.layers[index].loading.currentSrc = -1;
		document.Tween.addTween("layer_"+id, 0, "f_layer_opacity");

		if (this.isWorkers) {
			this.parallaxWorker.postMessage(new this.WorkerMessage('createLayer', {id:id, data:null, process:false, next:[], track:[], style:{}, offset:{x:0, y:0}, stringify:""}));
		} else {
			this.parallaxProcess.createLayer({id:id, data:null, process:false, next:[], track:[], style:{}, offset:{x:0, y:0}, stringify:""});
		}
		
		this.uneakAgency.createLoading(id, true);
	};
	
	this.setNextLayer = function(id, data) {
//		console.log("setNextLayer "+id);
		if (this.isWorkers) {
			this.parallaxWorker.postMessage(new this.WorkerMessage('setNextLayer', {id:id, data:data}));
		} else {
			this.parallaxProcess.setNextLayer(id, data);
		}
	};
	
	this.loadLayer = function(id, src) {
//		console.log("loadLayer "+id);
		var layer; for( var i = self.layers.length-1; i >= 0; i-- ) { if (self.layers[i].id == id) { layer = self.layers[i]; break; } }
		UneakEvent.removeEventListener(layer.loading, "load", self.onLayerImageLoad);
		
		layer.loading.src = src;
		layer.loading.nextSrc = layer.loading.src;
		
//		console.log("loadLayer id:"+id+" src:"+layer.loading.src);
		if (layer.loading.complete == true) {
			self.uneakAgency.stopLoading(id);
			
			layer.loading.currentSrc = layer.loading.src;
			
			if (self.isWorkers) {
				self.parallaxWorker.postMessage(new self.WorkerMessage('onNextLayerLoaded', id));
			} else {
				self.parallaxProcess.onNextLayerLoaded(id);
			}
			
		} else {
			self.uneakAgency.startLoading(id);
			UneakEvent.addEventListener(layer.loading, "load", self.onLayerImageLoad);
		}
	};
	
	this.onLayerImageLoad = function(event) {
//		console.log("onLayerImageLoad");
		UneakEvent.removeEventListener(this, "load", self.onLayerImageLoad);
		self.uneakAgency.stopLoading(this.id);
		
//		console.log("parallax onLayerImageLoad id:"+this.id+" src:"+this.src);
		
		this.currentSrc = this.src;
		if (self.isWorkers) {
			self.parallaxWorker.postMessage(new self.WorkerMessage('onNextLayerLoaded', this.id));
		} else {
			self.parallaxProcess.onNextLayerLoaded(this.id);
		}
		
		
	};
	
	this.refreshLayer = function(id) {
//		console.log("refreshLayer");
		var layer; for( var i = this.layers.length-1; i >= 0; i-- ) { if (this.layers[i].id == id) { layer = this.layers[i]; break; } }
//		console.log('refreshLayer ' + id);
		if (layer.layer) {
			document.Tween.tween("layer_" + id, layer.layer.uid, layer, 0, 0.1, null, this.removeLayer);
		} else {
			this.removeLayer(layer);
		}
	};
	
	this.removeLayer = function(layer) {
//		console.log("removeLayer");
//		console.log('removeLayer ' + layer.id);
		layer.style = {};
		if (layer.layer && layer.layer.parentNode) layer.layer.parentNode.removeChild(layer.layer);
		layer.layer = null;
		
		if (self.isWorkers) {
			self.parallaxWorker.postMessage(new self.WorkerMessage('newLayer', layer.id));
		} else {
			self.parallaxProcess.newLayer(layer.id);
		}
			
	};
	
	this.newLayer = function(id, type) {
//		console.log("newLayer "+id+ " : "+ type);
		var layer; for( var i = this.layers.length-1; i >= 0; i-- ) { if (this.layers[i].id == id) { layer = this.layers[i]; break; } }
		
		if (type == "none") {
			this.process();
			
		} else {
			if (type == "img") {
				layer.layer = new Image();
				layer.layer.src = layer.loading.src;
				layer.layer.style.maxWidth = "none";
				layer.layer.style.maxHeight = "none";
			} else {
				layer.layer = document.createElement("div");
				layer.layer.style.backgroundImage = "url('"+layer.loading.src+"')";
				layer.layer.style.backgroundRepeat = type;
			}
	
			layer.layer.uid = this.getLayerUID();
			layer.layer.className = 'layer';
			layer.layer.style.position = "absolute";
			layer.layer_container.appendChild(layer.layer);
	
			document.Tween.tweenValue("layer_"+id, 0); layer.layer.style.opacity = 0;
			document.Tween.tween("layer_" + id, layer.layer.uid, layer, 1, 0.1, null, this.process);
			//this.process();
		}
	};
	
	this.process = function() {
//		console.log("process");
		if (self.isWorkers) {
			self.parallaxWorker.postMessage(new self.WorkerMessage('process'));
		} else {
			self.parallaxProcess.process();
		}
	};
	
	this.styleLayer = function(id, style) {
		var layer; for( var i = this.layers.length-1; i >= 0; i-- ) { if (this.layers[i].id == id) { layer = this.layers[i]; break; } }
		layer.style = UneakEvent.concatObject(layer.style, style);
	};
	
		
	
	this.screenResize = function(x, y) {
		if (this.isWorkers) {
			this.parallaxWorker.postMessage(new this.WorkerMessage('screenResize', {x:x, y:y}));
		} else {
			this.parallaxProcess.screenResize(x, y);
		}
	};
	
	this.mouseMove = function(x, y) {
		if (this.isWorkers) {
			this.parallaxWorker.postMessage(new this.WorkerMessage('mouseMove', {x:x, y:y}));
		} else {
			this.parallaxProcess.mouseMove(x, y);
		}
	};

	this.offsetChange = function(x, y) {
		if (this.isWorkers) {
			this.parallaxWorker.postMessage(new this.WorkerMessage('offsetChange', {x:x, y:y}));
		} else {
			this.parallaxProcess.offsetChange(x, y);
		}
	};
	
	
	
	this.refresh = function() {
//		console.log("--------- PARALLAX REFRESH");
		for (var id in this.layers) {
			for (var style in this.layers[id].style) {
				
				if (style != "containerMarginLeft" && style != "containerMarginTop") {
					if (this.layers[id].layer) this.layers[id].layer.style[style] = this.layers[id].style[style];
				} else {
					if (style == "containerMarginLeft") this.layers[id].layer_container.style.marginLeft = this.layers[id].style[style];
					if (style == "containerMarginTop") this.layers[id].layer_container.style.marginTop = this.layers[id].style[style];
				}
				
			}
			
//			if (id == "LAYER-2") {
//				console.log(this.layers[id].style);
//				console.log(this.layers[id].layer);
//			}
			
			this.layers[id].style = {};
		}
	};
	
	
	
	this.WorkerMessage = function(cmd, parameter) {
		this.cmd = cmd;
		this.p = parameter;
	}	
	
	
	this.boot(theme_path);
}; 





/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

function UneakAgencyParallaxProcess(uneakAgencyParallax) {

	var self = this;
	this.uneakAgencyParallax = uneakAgencyParallax;
	
	this.layers = [];
	
	this.mousePosition = {x:0, y:0};
	this.mouseAnimPosition = {x:0, y:0};
	this.offsetPosition = {x:0, y:0};
	this.offsetAnimPosition = {x:0, y:0};
	this.screenSize = {x:0, y:0};
	
	
	this.interval = setInterval(function(){self.transitionMouse()}, 20);
	
	this.transitionMouse = function(event) {
		var diff = self.mousePosition.x - self.mouseAnimPosition.x;
		if (Math.abs(diff) < 0.1) self.mouseAnimPosition.x = self.mousePosition.x; else self.mouseAnimPosition.x = self.mouseAnimPosition.x + diff * 0.05;
		
		var diff = self.mousePosition.y - self.mouseAnimPosition.y;
		if (Math.abs(diff) < 0.1) self.mouseAnimPosition.y = self.mousePosition.y; else self.mouseAnimPosition.y = self.mouseAnimPosition.y + diff * 0.05;
		
		var diff = self.offsetPosition.x - self.offsetAnimPosition.x;
		if (Math.abs(diff) < 0.1) self.offsetAnimPosition.x = self.offsetPosition.x; else self.offsetAnimPosition.x = self.offsetAnimPosition.x + diff * 0.05;
		
		var diff = self.offsetPosition.y - self.offsetAnimPosition.y;
		if (Math.abs(diff) < 0.1) self.offsetAnimPosition.y = self.offsetPosition.y; else self.offsetAnimPosition.y = self.offsetAnimPosition.y + diff * 0.05;
		
		for( var i = self.layers.length-1; i >= 0; i-- ) self.refreshStyle(self.layers[i]);
	}
	
		
	this.mouseMove = function(x, y) {
		self.mousePosition.x = x;
		self.mousePosition.y = y;
	}
	
	this.offsetChange = function(x, y) {
		self.offsetPosition.x = x;
		self.offsetPosition.y = y;
	}
	
	this.screenResize = function(x, y) {
		self.screenSize.x = x;
		self.screenSize.y = y;
		for( var i = self.layers.length-1; i >= 0; i-- ) self.refreshParallaxPosition(self.layers[i]);
	}

	
	this.setNextLayer = function(id, data) {	// SET NEXT LAYER
		var layer; for( var i = self.layers.length-1; i >= 0; i-- ) { if (self.layers[i].id == id) { layer = self.layers[i]; break; } }
	
	//	this.postMessage(new self.WorkerMessage('console', 'setNextLayer id:'+id));
	//	this.postMessage(new self.WorkerMessage('console', layer));
	
		var dataStringify = JSON.stringify(data);
//		console.log("layer.next.stringify:"+layer.next.stringify);
//		console.log("layer.stringify:"+layer.stringify);
//		console.log("dataStringify:"+dataStringify);
		
		if ((layer.next.stringify && layer.next.stringify == dataStringify) || (!layer.next.stringify && layer.stringify == dataStringify)) return;
	
		layer.next.stringify = dataStringify;
		layer.next.data = data;
		layer.process = false;
		
		self.uneakAgencyParallax.loadLayer(id, (data.background) ? data.background : null);
	}
	
	
	this.onNextLayerLoaded = function(id) {	// NEXT LAYER IS LOADING
		var layer; for( var i = this.layers.length-1; i >= 0; i-- ) { if (this.layers[i].id == id) { layer = this.layers[i]; break; } }
		
	//	this.postMessage(new self.WorkerMessage('console', 'onNextLayerLoaded id:'+id));
	//	this.postMessage(new self.WorkerMessage('console', layer));
		
		layer.process = true;
		this.process();
	}
	
	
	this.process = function() {
//		console.log("PARALLAX process");
		for( var i = this.layers.length-1; i >= 0; i-- ) {
			if (this.layers[i].process == true) {
				this.layers[i].process = false;
				if (this.layers[i].next.data) {
					this.uneakAgencyParallax.refreshLayer(this.layers[i].id);
					return;
				}
			}
		}
	}
	
	
	this.createLayer = function(parameters) {
//		console.log("PARALLAX createLayer");
		this.layers.push(parameters);
	}
	

	this.newLayer = function(id) {
		var layer; for( var i = self.layers.length-1; i >= 0; i-- ) { if (self.layers[i].id == id) { layer = self.layers[i]; break; } }
		
		layer.stringify = layer.next.stringify;
		layer.data = layer.next.data;
		layer.track = [];
		layer.style = {};
		layer.style["marginLeft"] = "0px";
		layer.style["marginTop"] = "0px";
		layer.style["containerMarginLeft"] = "0px";
		layer.style["containerMarginTop"] = "0px";
		layer.next.stringify = "";
		layer.next.data = null;
		
		if (layer.data.background) {
			var func;
				
			if (layer.data.repeat != "no-repeat") {
				layer.type = layer.data.repeat;
				
				if (layer.data.offset == "horizontal") {
					layer.track.update = self.update_offsetHorizontal_byBackground;
				} else if (layer.data.offset == "vertical") {
					layer.track.update = self.update_offsetVertical_byBackground;
				} else {
					layer.track.update = self.update_offsetNone_byBackground;
				}
				
			} else {
				layer.type = "img";
				
				if (layer.data.offset == "horizontal") {
					layer.track.update = self.update_offsetHorizontal_byMargin;
				} else if (layer.data.offset == "vertical") {
					layer.track.update = self.update_offsetVertical_byMargin;
				} else {
					layer.track.update = self.update_offsetNone_byMargin;
				}
				
			}
		} else {
			layer.type = "none";
			layer.track.update = this.update_layer_container;
		}
		
		self.uneakAgencyParallax.newLayer(id, layer.type);
		self.uneakAgencyParallax.styleLayer(id, layer.style);
		self.refreshParallaxPosition(layer);
	};
	
	
	
	this.refreshParallaxPosition = function(layer) {
		if (layer.data && layer.type != "none") {
			
			var style = {};
			
			var w = layer.data.width;
			var h = layer.data.height;
	
			var margeX = self.screenSize.x / layer.data.distance;
			var margeY = self.screenSize.y / layer.data.distance;
			
			var half_margeX = ((self.screenSize.x / layer.data.distance)  / 2);
			var half_margeY = ((self.screenSize.y / layer.data.distance)  / 2);
			
			layer.offset.x = 0;
			layer.offset.y = 0;
			
			if (layer.data.size == "cover") {
	
				var ratioW = self.screenSize.x / w;
				var ratioH = self.screenSize.y / h;
				var ratio = (ratioH < ratioW) ? ratioW : ratioH;
				
				var sizeW = (w * ratio + self.screenSize.x / layer.data.distance) ;
				var sizeH = (h * ratio + self.screenSize.y / layer.data.distance) ;
	
				if (layer.type == "img") {
					style.width = Math.round(sizeW) + "px";
					style.height = Math.round(sizeH) + "px";
					
					if (layer.data.align_vertical == "top") {
						layer.offset.y = -half_margeY;
					} else if (layer.data.align_vertical == "bottom") {
						layer.offset.y = self.screenSize.y - sizeH - half_margeY;
					} else {
						layer.offset.y = Math.round((self.screenSize.y - sizeH) / 2);
					}
					
					if (layer.data.align_horizontal == "left") {
						layer.offset.x = -half_margeX;
					} else if (layer.data.align_horizontal == "right") {
						layer.offset.x = self.screenSize.x - sizeW - half_margeX;
					} else {
						layer.offset.x = Math.round((self.screenSize.x - sizeW) / 2);
					}
				} else {
					style.width = (self.screenSize.x + margeX) + "px";
					style.height = (self.screenSize.y + margeY) + "px";
					layer.offset.x = -half_margeX;
					layer.offset.y = -half_margeY;
					
					valign = "center";
					if (layer.data.align_vertical == "top") {
						valign = "top";
					} else if (layer.data.align_vertical == "bottom") {
						valign = "bottom";
					}
					
					align = "center";
					if (layer.data.align_horizontal == "left") {
						align = "left";
					} else if (layer.data.align_horizontal == "right") {
						align = "right";
					}
					
					style.backgroundSize = Math.round(sizeW)+"px "+Math.round(sizeH)+"px";
					style.backgroundPosition = align+" "+valign;
				}
			} else {
			
				var ratio = 1;
				var sizeW = (w + self.screenSize.x / layer.data.distance) ;
				var sizeH = (h + self.screenSize.y / layer.data.distance) ;
				
				if (layer.type == "img") {
					style.width = w + "px";
					style.height = h + "px";
				} else {
					if (layer.data.repeat == "repeat-x") {
						style.width = "100%";
						style.height = Math.round(sizeH) + "px";
					} else if (layer.data.repeat == "repeat-y") {
						style.width = Math.round(sizeW) + "px";
						style.height = "100%";
					}
				}
				
				if (layer.data.align_vertical == "top") {
					layer.offset.y = -half_margeY;
					style.top = "0px";
				} else if (layer.data.align_vertical == "bottom") {
					layer.offset.y = half_margeY;
					style.top = (self.screenSize.y - h) + "px";
				} else {
					style.top = Math.round((self.screenSize.y - h) / 2)  + "px";
				}
				
				if (layer.data.align_horizontal == "left") {
					layer.offset.x = -half_margeX;
					style.left = "0px";
				} else if (layer.data.align_horizontal == "right") {
					layer.offset.x = half_margeX;
					style.left = (self.screenSize.x - w) + "px";
				} else {
					style.left = Math.round((self.screenSize.x - w) / 2)  + "px";
				}
					
			}
			
			
			
			var dispatchedStyle = {};
			
			var change = false;
			for (i in style) {
				if (!layer.style[i] || style[i] != layer.style[i]) {
					layer.style[i] = style[i];
					dispatchedStyle[i] = style[i];
					change = true;
				}
			}
			if (change) self.uneakAgencyParallax.styleLayer(layer.id, dispatchedStyle);
		
		}
		
	}
	
	
	
	this.refreshStyle = function(layer) {
		if (layer.track.update) {
			var mouseX = self.mouseAnimPosition.x - (self.screenSize.x / 2);
			var mouseY = self.mouseAnimPosition.y - (self.screenSize.y / 2);
			var style = layer.track.update(layer, mouseX, mouseY);
			var dispatchedStyle = {};		
			var change = false;
			for (i in style) {
				if (!layer.style[i] || style[i] != layer.style[i]) {
					layer.style[i] = style[i];
					dispatchedStyle[i] = style[i];
					change = true;
				}
			}
			if (change) self.uneakAgencyParallax.styleLayer(layer.id, dispatchedStyle);
		}
	}
	
	
	
	this.update_offsetHorizontal_byBackground = function(layer, mouseX, mouseY) {
		layer_posX = Math.round(layer.offset.x + ((self.offsetAnimPosition.x - mouseX) / layer.data.distance)) ;
		layer_posY = Math.round(layer.offset.y + ((self.offsetAnimPosition.y - mouseY) / layer.data.distance)) ;
		return {backgroundPosition:layer_posX + 'px ' + layer_posY + 'px'};
	};
	
	this.update_offsetVertical_byBackground = function(layer, mouseX, mouseY) {
		layer_posX = Math.round(layer.offset.x + ((self.offsetAnimPosition.y - mouseX) / layer.data.distance)) ;
		layer_posY = Math.round(layer.offset.y + ((self.offsetAnimPosition.x - mouseY) / layer.data.distance)) ;
		return {backgroundPosition:layer_posX + 'px ' + layer_posY + 'px'};
	};
	
	this.update_offsetNone_byBackground = function(layer, mouseX, mouseY) {
		layer_posX = Math.round(layer.offset.x - (mouseX / layer.data.distance)) ;
		layer_posY = Math.round(layer.offset.y - (mouseY / layer.data.distance)) ;
		return {backgroundPosition:layer_posX + 'px ' + layer_posY + 'px'};
	};
	
	this.update_offsetHorizontal_byMargin = function(layer, mouseX, mouseY) {
		layer_posX = Math.round(layer.offset.x + ((self.offsetAnimPosition.x - mouseX) / layer.data.distance)) ;
		layer_posY = Math.round(layer.offset.y + ((self.offsetAnimPosition.y - mouseY) / layer.data.distance)) ;
		return {marginLeft:layer_posX + 'px', marginTop:layer_posY + 'px'};
	};
	
	this.update_offsetVertical_byMargin = function(layer, mouseX, mouseY) {
		layer_posX = Math.round(layer.offset.x + ((self.offsetAnimPosition.y - mouseX) / layer.data.distance)) ;
		layer_posY = Math.round(layer.offset.y + ((self.offsetAnimPosition.x - mouseY) / layer.data.distance)) ;
		return {marginLeft:layer_posX + 'px', marginTop:layer_posY + 'px'};
	};
	
	this.update_offsetNone_byMargin = function(layer, mouseX, mouseY) {
		layer_posX = Math.round(layer.offset.x - (mouseX / layer.data.distance)) ;
		layer_posY = Math.round(layer.offset.y - (mouseY / layer.data.distance)) ;
		return {marginLeft:layer_posX + 'px', marginTop:layer_posY + 'px'};
	};
	
	this.update_layer_container = function(layer, mouseX, mouseY) {
		layer_posX = Math.round(layer.offset.x - (mouseX / layer.data.distance)) ;
		layer_posY = Math.round(layer.offset.y - (mouseY / layer.data.distance)) ;
		return {containerMarginLeft:layer_posX + 'px', containerMarginTop:layer_posY + 'px'};
	};
	
}
