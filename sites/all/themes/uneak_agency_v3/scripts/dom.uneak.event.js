
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

var UneakEventClass = {};
UneakEventClass = function() { };

UneakEventClass.prototype = {
	
	addEventListener:function(elem, eventType, handler) {
		if (elem.addEventListener) {
			elem.addEventListener(eventType, handler, false);
		} else if (elem.attachEvent) {
			elem.attachEvent ('on'+eventType, handler); 
		} else {
			elem["on"+eventType] = handler;  
		}
	},
	
	removeEventListener:function(elem, eventType, handler) {
		if (elem.removeEventListener) {
			elem.removeEventListener(eventType, handler, false);
		} else if (elem.detachEvent) {
			elem.detachEvent ('on'+eventType, handler); 
		} else {
			elem["on"+eventType] = null;  
		}
	},
	
	dispatchEvent:function(elem, eventType, param) {
		if (document.createEventObject) {   // IE before version 9
			var event = document.createEventObject();
			event = this.concatObject(event, param);
			elem.fireEvent(eventType, event);
		} else {
			var event = document.createEvent("HTMLEvents");
			event = this.concatObject(event, param);
			elem.dispatchEvent(event);

		}
	},
	
	concatObject:function(obj1, obj2) {
		for (var i in obj2) obj1[i] = obj2[i];
		return obj1;
	}
	
};
var UneakEvent = new UneakEventClass();