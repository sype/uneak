<?php


class Layer {
	var $id;
	var $background;
	var $distance;
	var $repeat;
	var $size;
	var $offset;
	var $align_horizontal;
	var $align_vertical;
	var $width;
	var $height;
	
	function __construct($item = NULL) {
		if (isset($item)) $this->parse($item);
    }
	
	function parse($item) {
		
		$this->id = $item->field_layer_id['und'][0]['value'];
		if (isset($item->field_image['und'][0]['uri'])) $this->background = file_create_url($item->field_image['und'][0]['uri']);
		if (isset($item->field_distance['und'][0]['value'])) $this->distance = $item->field_distance['und'][0]['value'];
		if (isset($item->field_repeat['und'][0]['value'])) $this->repeat = $item->field_repeat['und'][0]['value'];
		if (isset($item->field_background_size['und'][0]['value'])) $this->size = $item->field_background_size['und'][0]['value'];
		if (isset($item->field_offset['und'][0]['value'])) $this->offset = $item->field_offset['und'][0]['value'];
		if (isset($item->field_align_horizontal['und'][0]['value'])) $this->align_horizontal = $item->field_align_horizontal['und'][0]['value'];
		if (isset($item->field_align_vertical['und'][0]['value'])) $this->align_vertical = $item->field_align_vertical['und'][0]['value'];
		if (isset($item->field_width['und'][0]['value'])) {
			$this->width = $item->field_width['und'][0]['value'];
		} else if (isset($item->field_image['und'][0]['image_dimensions']['width'])) {
			$this->width = $item->field_image['und'][0]['image_dimensions']['width'];
		}
		if (isset($item->field_height['und'][0]['value'])) {
			$this->height = $item->field_height['und'][0]['value'];
		} else if (isset($item->field_image['und'][0]['image_dimensions']['height'])) {
			$this->height = $item->field_image['und'][0]['image_dimensions']['height'];
		}
		
	}
	
	function getArray() {
		$ret = array();
		if ($this->background) $ret["background"] = $this->background;
		if ($this->distance) $ret["distance"] = $this->distance;
		if ($this->repeat) $ret["repeat"] = $this->repeat;
		if ($this->size) $ret["size"] = $this->size;
		if ($this->offset) $ret["offset"] = $this->offset;
		if ($this->align_horizontal) $ret["align_horizontal"] = $this->align_horizontal;
		if ($this->align_vertical) $ret["align_vertical"] = $this->align_vertical;
		if ($this->width) $ret["width"] = $this->width;
		if ($this->height) $ret["height"] = $this->height;
		return $ret;	
	}
	
	function cloneMe() {
		$clone = new Layer();
		$clone->id = $this->id;
		$clone->background = $this->background;
		$clone->distance = $this->distance;
		$clone->repeat = $this->repeat;
		$clone->size = $this->size;
		$clone->offset = $this->offset;
		$clone->align_horizontal = $this->align_horizontal;
		$clone->align_vertical = $this->align_vertical;
		$clone->width = $this->width;
		$clone->height = $this->height;
		return $clone;
	}
	
	function buildLayer() {
		$render = '';
		if ($this->background) {
			if (!$this->repeat) $this->repeat = "no-repeat";
			
			if ($this->repeat != "no-repeat" || $this->size == "cover") {
				$render .= '<div class="layer" style="';
				$render .= 'background-image:url('.$this->background.');';
				$render .= 'background-repeat:'.$this->repeat.';';
				if ($this->size == "cover") {
					$render .= 'background-size:cover;';
					$render .= 'width:100%;';
					$render .= 'height:100%;';
				} else {
					if ($this->repeat == "repeat-x") {
						$render .= 'width:100%;';
						$render .= 'height:'.$this->height.'px;';
					} else if ($this->repeat == "repeat-y") {
						$render .= 'width:'.$this->width.'px;';
						$render .= 'height:100%;';
					} else {
						$render .= 'width:'.$this->width.'px;';
						$render .= 'height:'.$this->height.'px;';
					}
					
					if ($this->align_vertical == "top") {
						$render .= 'top:0px;';
						$render .= 'position:absolute;';
					} else if ($this->align_vertical == "bottom") {
						$render .= 'bottom:0px;';
						$render .= 'position:absolute;';
					} else {
						$render .= 'margin-top:-'.($this->height/2).'px;';
						$render .= 'top:50%;';
					}
										
					if ($this->align_horizontal == "left") {
						$render .= 'left:0px;';
						$render .= 'position:absolute;';
					} else if ($this->align_horizontal == "right") {
						$render .= 'right:0px;';
						$render .= 'position:absolute;';
					} else {
						$render .= 'display:block;';
						$render .= 'margin-left:auto;';
						$render .= 'margin-right:auto;';
						$render .= 'position:relative;';
					}
				}
				
				$render .= '"></div>';
			} else {
				$render .= '<img class="layer" src="'.$this->background.'" style="';
				$render .= 'max-width:none;';
				$render .= 'max-height:none;';
				
				$render .= 'width:'.$this->width.'px;';
				$render .= 'height:'.$this->height.'px;';
					
				if ($this->align_vertical == "top") {
					$render .= 'top:0px;';
					$render .= 'position:absolute;';
				} else if ($this->align_vertical == "bottom") {
					$render .= 'bottom:0px;';
					$render .= 'position:absolute;';
				} else {
					$render .= 'margin-top:-'.($this->height/2).'px;';
					$render .= 'top:50%;';
				}
									
				if ($this->align_horizontal == "left") {
					$render .= 'left:0px;';
					$render .= 'position:absolute;';
				} else if ($this->align_horizontal == "right") {
					$render .= 'right:0px;';
					$render .= 'position:absolute;';
				} else {
					$render .= 'display:block;';
					$render .= 'margin-left:auto;';
					$render .= 'margin-right:auto;';
					$render .= 'position:relative;';
				}
				
				
				$render .= '" />';
			}
		}
	
		return $render;
	}

}

class Layers {
	var $layer;
	
	function __construct($field_layer = NULL) {
        $this->layer = array();
		
		if (isset($field_layer)) $this->parse($field_layer);
    }
	
	function parse($field_layer) {
		if (count($field_layer)) {
			foreach($field_layer['und'] as $itemid) {
				$item = entity_load('field_collection_item', $itemid);
				if ($item[$itemid["value"]]->field_layer_id && $item[$itemid["value"]]->field_layer_id['und'][0]['value']) $this->addLayer(new Layer($item[$itemid["value"]]));
			}
		}
	}
	
	function getLayerById($id) {
		for($i = 0; $i < count($this->layer); $i++) {
			if ($this->layer[$i]->id == $id) return $this->layer[$i];
		}
		return NULL;
	}
	
	function addLayer($layer) {
		array_push($this->layer, $layer);
	}
	
	function merge($layers) {
		for($i = 0; $i < count($layers->layer); $i++) {
			$found = -1;
			for($j = 0; $j < count($this->layer); $j++) {
				if ($layers->layer[$i]->id == $this->layer[$j]->id) {
					$found = $j;
					break;
				}
			}
			if ($found != -1) array_splice($this->layer, $found, 1);
			$this->addLayer($layers->layer[$i]->cloneMe());
		}
	}
	
	function cloneMe() {
		$clone = new Layers();
		for($i = 0; $i < count($this->layer); $i++) $clone->addLayer($this->layer[$i]->cloneMe());
		return $clone;
	}
	
	function getArray() {
		$ret = array();
		for($i = 0; $i < count($this->layer); $i++) $ret[$this->layer[$i]->id] = $this->layer[$i]->getArray();
		return $ret;
	}
	
	function getJSON() {
		return json_encode($this->getArray());
	}
	
	function getProperty() {
		if (count($this->layer)) {
			return " data-layer='".json_encode($this->getArray())."'";
		} else {
			return "";
		}
	}
	
	
	
	function buildLayers() {
		$render = '';
		$render .= '<div id="background-sky-4" class="background-sky">'.$this->getLayerById("LAYER-4")->buildLayer().'</div>';
		$render .= '<div id="background-sky-3" class="background-sky">'.$this->getLayerById("LAYER-3")->buildLayer().'</div>';
		$render .= '<div id="background-sky-2" class="background-sky">'.$this->getLayerById("LAYER-2")->buildLayer().'</div>';
		$render .= '<div id="background-sky-1" class="background-sky">'.$this->getLayerById("LAYER-1")->buildLayer().'</div>';
		$render .= '<div id="background-sky-0" class="background-sky">'.$this->getLayerById("LAYER-0")->buildLayer().'</div>';
		
		return $render;
	}

}


class HeaderInfo {
	var $title;
	var $baseline;
	var $layers;
	var $cover;
	var $color;
	
	
	function render() {
		$render = '';
		if (isset($this->title)) $render .= ' data-title="'.$this->title.'"';
		if (isset($this->baseline)) $render .= ' data-baseline="'.$this->baseline.'"';
		if (isset($this->color)) {
			$rgb = $this->html2rgb($this->color);
			
			$render .= ' data-color-r="'.$rgb[0].'"';
			$render .= ' data-color-g="'.$rgb[1].'"';
			$render .= ' data-color-b="'.$rgb[2].'"';
		}
		if (isset($this->layers)) $render .= $this->layers->getProperty();
		if (isset($this->cover)) $render .= ' data-cover="'.$this->cover.'"';
		return $render;
	}
	
	function getColor() {
		$render = 'rgb(';
		$rgb = $this->html2rgb($this->color);
		$render .= ' '.$rgb[0].',';
		$render .= ' '.$rgb[1].',';
		$render .= ' '.$rgb[2];
		$render .= ')';
		return $render;
	}
	
	function html2rgb($color) {
		if ($color[0] == '#')
			$color = substr($color, 1);
	
		if (strlen($color) == 6)
			list($r, $g, $b) = array($color[0].$color[1],
									 $color[2].$color[3],
									 $color[4].$color[5]);
		elseif (strlen($color) == 3)
			list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
		else
			return false;
	
		$r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
	
		return array($r, $g, $b);
	}
}

        
            


class GroupElement {
	var $color;		//	white, black, transparent, transparent-white, transparent-black, verre
	var $content;
	var $align;
	var $class;
	
	function __construct($color, $align, $class = "") {
        $this->content = array();
		$this->color = $color;
		$this->align = $align;
		$this->class = $class;
    }
	
	function addContent($str) {
		array_push($this->content, $str);
	}
	
	function unshiftContent($str) {
		array_unshift($this->content, $str);
	}	
	
	function render() {
		if (count($this->content) == 0) return "";
		
		$render = '';
		$render .= '<div class="node extend-block-'.$this->color.''.$this->class.'">';
		$render .= '<div class="node-header node-data node-block-full">';
		$render .= '<div class="node-info" style="text-align:'.$this->align.';">';
		for($i = 0; $i < count($this->content); $i++) $render .= $this->content[$i];
		$render .= '</div>';
		$render .= '</div>';
		$render .= '</div>';
		
		return $render;
	}
}


class Group extends HeaderInfo {
	var $col1;
	var $col2;
	var $col3;
	var $row;
	var $margin;
	var $class;
	
	function __construct($class = "") {
    	$this->margin = "none";
		$this->class = $class;
	}
	
	function addCol1($groupElement) {
		$this->col1 = $groupElement;
	}
	function addCol2($groupElement) {
		$this->col2 = $groupElement;
	}
	function addRow($groupElement) {
		$this->row = $groupElement;
	}
	function addCol3($groupElement) {
		$this->col3 = $groupElement;
	}
	
	function render($class = "") {
		$render = '';
		if (isset($this->col1) || isset($this->col2) || isset($this->col3)) {
			$render .= '<div class="group margin-separator-'.$this->margin.''.$class.''.$this->class.'"'.parent::render().'>';
			if (isset($this->col1) || isset($this->col2)) {
				$render .= '<div class="group-content">';
				if (isset($this->col1)) $render .= '<div class="group-col group-col-1'.(!isset($this->col2) ? " col-solo" : "").'">'.$this->col1->render().'</div>';
				if (isset($this->col2)) {
					$render .= '<div class="group-col group-col-2'.(!isset($this->col1) ? " col-solo" : "").'">'.$this->col2->render().'</div>';
					$render .= '<div class="group-col-2-background node extend-block-'.$this->col2->color.'"></div>';
				}
				if (isset($this->row)) $render .= '<div class="group-row'.(isset($this->col2) ? " hasCol2" : "").'">'.$this->row->render().'</div>';
				$render .= '</div>';
			}
			if (isset($this->col3)) {
				$render .= '<div class="group-link">';
				$render .= '<div class="group-col group-col-3">'.$this->col3->render().'</div>';
				$render .= '</div>';
			}
		
			$render .= '</div>';
		}
		return $render;
	}
}


class Rubrique extends HeaderInfo {
	var $group;
	var $margin;
	var $separator;
	var $class;
	
	function __construct($class = "") {
		$this->group = array();
    	$this->margin = "none";
		$this->separator = "";
		$this->class = $class;
    }
	
	function addGroup($group) {
		array_push($this->group, $group);
	}

	function render($class = "") {
		
		$render = '';
		
		
		if ($this->separator) {
			$group = new Group();
			$col2 = new GroupElement("black", "left");
			$col2->addContent('<h2 class="block-title"><span>'.$this->separator.'</span></h2>');
			$group->addCol2($col2);
			
			$render = '';
			$render .= '<div class="rubrique margin-separator-maxi"'.parent::render().'>';
			$render .= $group->render();
			$render .= '</div>';
		}
	
	
		$render .= '<div class="rubrique margin-separator-'.$this->margin.''.$class.''.$this->class.'"'.parent::render().'>';
		for($i = 0; $i < count($this->group); $i++) {
			
			if ($this->baseline && $this->group[$i]->col1) $this->group[$i]->col1->unshiftContent('<div class="block-title"><span>'.$this->baseline.'</span></div>');

			$group_class = "";
			if ($i == 0) $group_class .= " first";
			if ($i == count($this->group)-1) $group_class .= " last";
			if ($i % 2 === 0) $group_class .= " even"; else $group_class .= " odd";
			$render .= $this->group[$i]->render($group_class);
		}
		$render .= '</div>';
		return $render;
	}
}



class MainContent extends HeaderInfo {
	var $rubrique;
	
	function __construct() {
		$this->rubrique = array();
    }
	
	function addRubrique($rubrique) {
		array_push($this->rubrique, $rubrique);
	}

	function buildLayers() {
		$render = '';
		$render .= '<div id="background-sky-color" class="background-sky" style="background-color:'.$this->getColor().';"></div>';
//		$render .= '<div id="background-sky-gradient" class="background-sky"></div>';
		$render .= $this->layers->buildLayers();
		return $render;
	}
	
	function render() {
		$render = '';
		$render .= '<div id="main-content-node-wrapper"'.$this->render_header_info().'>';
		for($i = 0; $i < count($this->rubrique); $i++) {		
			$rub_class = "";
			if ($i == 0) $rub_class .= " first";
			if ($i == count($this->rubrique)-1) {
				$rub_class .= " last";
				$this->rubrique[$i]->margin = "footer";
			}
			if ($i % 2 === 0) $rub_class .= " even"; else $rub_class .= " odd";
			$render .= $this->rubrique[$i]->render($rub_class);
		}
		$render .= '</div>';

		return $render;
	}
	
	function render_header_info() {
		return parent::render();
	}
	
	
}
	
                                






function get_rubrique_entity_reference($content, $margin = "none", $layers = false) {
	$cmpt = 0;
	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	$rubrique->baseline = $content['#title'];
	while(isset($content[$cmpt])) {
		$rubrique->addGroup(get_group_node(reset($content[$cmpt]["node"]), $margin, $layers, true));
		$cmpt++;
	}
	
	return $rubrique;
}



function get_rubrique_media($content) {
	
	$rubrique = new Rubrique();

	if (isset($content['field_video'])) $rubrique->addGroup(get_group_field($content['field_video']));
	if (isset($content['field_gallery'])) $rubrique->addGroup(get_group_field($content['field_gallery']));
	
	return $rubrique;
}



function get_rubrique_technique($content) {
	
	$rubrique = new Rubrique();

	$group = new Group();
	
	//
	//	COL1
	//
	$col1 = new GroupElement("invisible", "left");
	
	$image = "";
//	if ($showCover) $image = file_create_url($content['field_image'][0]["#item"]['uri']);
	$baseline = "";
//	if (isset($content['field_type'])) $baseline .= render($content['field_type']);
//	if (isset($content['field_level'])) $baseline .= render($content['field_level']);
	$created = "";
//	if (isset($content['#node']->created)) $created = format_date($content['#node']->created, 'long');
	$extra = "";
//	if (isset($content['field_link'])) $extra .= render($content['field_link']);
	
	$col1->addContent(get_content_col1("", $image, t("Spécifications techniques"), $baseline, $created, $extra));
	$group->addCol1($col1);

	//
	//	COL2
	//
	$col2 = new GroupElement("white", "left");
	if (isset($content['field_client'])) $col2->addContent(render($content['field_client']));
	if (isset($content['field_agency'])) $col2->addContent(render($content['field_agency']));
	if (isset($content['field_technology'])) $col2->addContent(render($content['field_technology']));
	if (isset($content['field_logiciel'])) $col2->addContent(render($content['field_logiciel']));
	if (isset($content['field_etiquettes'])) $col2->addContent(render($content['field_etiquettes']));
	if (isset($content['field_award'])) $col2->addContent(render($content['field_award']));
	$group->addCol2($col2);
	
	$rubrique->addGroup($group);
	
	return $rubrique;
}




function get_group_field($content) {
	
	$group = new Group();
	$group->baseline = $content['#title'];
	
	//
	//	COL1
	//
	$col1 = new GroupElement("invisible", "left");
	
	$image = "";
//	if ($showCover) $image = file_create_url($content['field_image'][0]["#item"]['uri']);
	$baseline = "";
//	if (isset($content['field_type'])) $baseline .= render($content['field_type']);
//	if (isset($content['field_level'])) $baseline .= render($content['field_level']);
	$created = "";
//	if (isset($content['#node']->created)) $created = format_date($content['#node']->created, 'long');
	$extra = "";
//	if (isset($content['field_link'])) $extra .= render($content['field_link']);
	
	$col1->addContent(get_content_col1("", $image, $content['#title'], $baseline, $created, $extra));
	
	$group->addCol1($col1);

	//
	//	COL2
	//
	$col2 = new GroupElement("invisible", "left");
	$col2->addContent(render($content));
	$group->addCol2($col2);
	
	return $group;
}





function get_rubrique_node($content, $margin = "none", $layers = false, $chapter = false, $created = false) {
	
	$rubrique = new Rubrique();

	if ($layers) {
		global $default_layers;
		$clone = $default_layers->cloneMe();
		if (isset($content['#node']->field_layer)) $clone->merge(new Layers($content['#node']->field_layer));
		
		$rubrique->layers = $clone;
		if (isset($content["#node"]->field_color['und'])) $rubrique->color = $content["#node"]->field_color['und'][0]['jquery_colorpicker'];
	}
		
	$rubrique->cover = file_create_url($content['field_image']['#items'][0]['uri']); //image_style_url("reference_image_3d", $content['field_image']['#items'][0]['uri']);
	$rubrique->baseline = "";
	
	$rubrique->addGroup(get_group_node($content, $margin, false, false, $created));
	
	if ($chapter && isset($content['field_chapter']) && count($content['field_chapter']['#items'])) {
		foreach($content['field_chapter']['#items'] as $itemid) $rubrique->addGroup(get_group_chapter($itemid, $margin));
	}
	
	return $rubrique;
}




function get_group_node($content, $margin = "none", $layers = false, $showCover = false, $showDate = false) {
	$group = new Group();
	
	$group->margin = $margin;
	$group->baseline = $content['#node']->title;
	
	if ($layers) {
		$group->layers = new Layers($content['#node']->field_layer);
		if (isset($content['#node']->field_color['und'])) $group->color = $content["#node"]->field_color['und'][0]['jquery_colorpicker'];
	}
	
	//
	//	COL1
	//
	$col1 = new GroupElement("invisible", "left");
	
	$image = "";
	if ($showCover) $image = file_create_url($content['field_image'][0]["#item"]['uri']);
	$baseline = "";
	if (isset($content['field_type'])) $baseline .= render($content['field_type']);
	if (isset($content['field_level'])) $baseline .= render($content['field_level']);
	$extra = "";
	if (isset($content['field_link'])) $extra .= render($content['field_link']);
	$created = "";
	if ($showDate && isset($content['#node']->created)) $created = format_date($content['#node']->created, 'long');
	$link = "";
	if (isset($content['links']['node']['#links']['node-readmore'])) $link = $content['links']['node']['#links']['node-readmore'];
	
	
	$col1->addContent(get_content_col1($link, $image, $content['#node']->title, $baseline, $created, $extra));
	
	$group->addCol1($col1);

	//
	//	COL2
	//
	if (isset($content['body'])) {
		$col2 = new GroupElement("white", "left");
		if (isset($content['body']['#object']->field_client['und'])) {
			foreach ($content['body']['#object']->field_client['und'] as $field_client) {
				$term = taxonomy_term_load($field_client["tid"]);
				$col2->addContent(render(taxonomy_term_view($term, 'image')));
			}
		}
		$col2->addContent(render($content['body']));
		$group->addCol2($col2);
	}
	
	//
	//	COL3
	//
	if (count($content['links']['node']['#links'])) {
		$col3 = new GroupElement("white-light", "left");
		$col3->addContent(render($content['links']['node']));
		$group->addCol3($col3);
	}
	
	return $group;
}


function get_content_col1($link, $image, $title, $baseline, $created, $extra) {
	$return = "";

	if ($image != "") {
		if ($link) {
			$return .= '<div class="cover-div-background"><a href="/'.drupal_lookup_path('alias',$link["href"]).'" class="image-cover" style="background-image:url(\''.$image.'\');">&nbsp;</a></div>';
		} else {
			$return .= '<div class="cover-div-background"><div class="image-cover" style="background-image:url(\''.$image.'\');">&nbsp;</div></div>';
		}
	}
	$return .= '<div class="block-node-title">';
	if ($created != "") $return .= '<span class="text-format-mini">'.$created.'</span>';
	$return .= '<h2 class="text-format-h2">'.$title.'</h2>';
	$return .= '<span class="text-format-mini">'.$baseline.'</span>';
	$return .= '</div>';
	$return .= $extra;
	
	return $return;
}


function get_rubrique_chapter($content, $margin = "none") {
	$rubrique_chapter = new Rubrique();
	$rubrique_chapter->cover = "null";
	foreach($content as $itemid) $rubrique_chapter->addGroup(get_group_chapter($itemid, $margin));
	return $rubrique_chapter;
}



function get_group_chapter($itemid, $margin = "none") {

	$item = entity_load('field_collection_item', $itemid);
	$item_view = entity_view('field_collection_item', $item);
	$field_set = $item_view['field_collection_item'][$itemid['value']];	
	
	$group = new Group();
	$group->margin = $margin;
	
	//
	//	COL1
	//
	$col1 = new GroupElement("invisible", "left");

	$image = "";
	if (isset($field_set['field_image'])) $image = file_create_url($field_set['field_image'][0]["#item"]['uri']);
	$baseline = "";
//	if (isset($content['field_type'])) $baseline .= render($content['field_type']);
//	if (isset($content['field_level'])) $baseline .= render($content['field_level']);
	$created = "";
//	if (isset($content['#node']->created)) $created = format_date($content['#node']->created, 'long');
	$extra = "";
	if (isset($field_set['field_link'])) $extra .= render($field_set['field_link']);

	$col1->addContent(get_content_col1("", $image, render($field_set['field_title']), $baseline, $created, $extra));
	
								
	$group->addCol1($col1);
	
	//
	//	COL2
	//
	$col2 = new GroupElement("white", "left");
	$col2->addContent(render($field_set['field_body']));
	
	$group->addCol2($col2);
	
	return $group;
}







function get_rubrique_client($content) {
	$client = reset($content['#items']);
	
	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	$rubrique->addGroup(get_group_client($content));
	return $rubrique;
}

function get_group_client($content) {
	$client = reset($content['#items']);
	$group = new Group();

	$col1 = new GroupElement("invisible", "left");
	
	$image = "";
	//if ($showCover) $image = file_create_url($content['field_image'][0]["#item"]['uri']);
	$baseline = "";
//	if (isset($content['field_type'])) $baseline .= render($content['field_type']);
//	if (isset($content['field_level'])) $baseline .= render($content['field_level']);
	$created = "";
//	if (isset($content['#node']->created)) $created = format_date($content['#node']->created, 'long');
	$extra = "";
	//if (isset($content['field_link'])) $extra .= render($content['field_link']);
	
	$col1->addContent(get_content_col1("", $image, $client["title"], $baseline, $created, $extra));
			
	$group->addCol1($col1);
	
	$col2 = new GroupElement("white", "left");
	$col2->addContent('<div class="client-body"><span>'.$client["body"].'</span></div>');
	$col2->addContent(render($content));
	$group->addCol2($col2);
	
	return $group;
}





function get_rubrique_ref_filter($content) {
	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	
	$group = new Group();
		
	//
	//	COL1
	//
	
	if (!isset($content['node_result']['no_content'])) {
		$col1 = new GroupElement("invisible", "left");
		
		$image = "";
		//if ($showCover) $image = file_create_url($content['field_image'][0]["#item"]['uri']);
		$baseline = "";
	//	if (isset($content['field_type'])) $baseline .= render($content['field_type']);
	//	if (isset($content['field_level'])) $baseline .= render($content['field_level']);
		$created = "";
//		if (isset($content['#node']->created)) $created = format_date($content['#node']->created, 'long');
		$extra = "";
		//if (isset($content['field_link'])) $extra .= render($content['field_link']);
		
		$col1->addContent(get_content_col1("", $image, t("Filtre"), $baseline, $created, $extra));
		$group->addCol1($col1);
	}


	//
	//	COL2
	//
	$col2 = new GroupElement("white-light", "left");
	$col2->addContent(render($content['term_filter']));
	$group->addCol2($col2);

	
	
	$rubrique->addGroup($group);
	return $rubrique;
}




function get_rubrique_ref_result_count($content) {
	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	
	$group = new Group();
		
	//
	//	COL1
	//
	
	if (!isset($content['node_result']['no_content'])) {
		$col1 = new GroupElement("invisible", "left");
		
		$image = "";
		//if ($showCover) $image = file_create_url($content['field_image'][0]["#item"]['uri']);
		$baseline = "";
	//	if (isset($content['field_type'])) $baseline .= render($content['field_type']);
	//	if (isset($content['field_level'])) $baseline .= render($content['field_level']);
		$created = "";
//		if (isset($content['#node']->created)) $created = format_date($content['#node']->created, 'long');
		$extra = "";
		//if (isset($content['field_link'])) $extra .= render($content['field_link']);
		
		$col1->addContent(get_content_col1("", $image, render($content['node_result']['node_count']), render($content['node_result']['current_page']), $created, $extra));
		$group->addCol1($col1);
	}


	//
	//	COL2
	//
	$col2 = new GroupElement("white-light", "left");
	$col2 = new GroupElement("black", "left");
	$col2->addContent(render($content['node_result']['pager']));
	$group->addCol2($col2);

	
	
	$rubrique->addGroup($group);
	return $rubrique;
}




function get_rubrique_ref_entete($content) {
	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	
	$group = new Group();

	//
	//	COL2
	//
	$col2 = new GroupElement("white", "left");
	$col2->addContent(render($content));
	$group->addCol2($col2);	
	
	$rubrique->addGroup($group);
	return $rubrique;
}




function get_rubrique_ref_node($content, $margin = "none") {
	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	foreach($content as $node) $rubrique->addGroup(get_group_node($node, $margin, true, true));
	return $rubrique;
}

function get_rubrique_ref_no_content($content) {
	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	
	$group = new Group();
		
	//
	//	COL2
	//
	$col2 = new GroupElement("black", "left");
	$col2->addContent(render($content));
	$group->addCol2($col2);
	
	$rubrique->addGroup($group);
	
	return $rubrique;
}



function get_rubrique_ref_pager($content) {
	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	
	$group = new Group();
		
	//
	//	COL1
	//
	$col1 = new GroupElement("invisible", "left");
	
	$image = "";
	//if ($showCover) $image = file_create_url($content['field_image'][0]["#item"]['uri']);
	$baseline = "";
//	if (isset($content['field_type'])) $baseline .= render($content['field_type']);
//	if (isset($content['field_level'])) $baseline .= render($content['field_level']);
	$created = "";
	//if (isset($content['#node']->created)) $created = format_date($content['#node']->created, 'long');
	$extra = "";
	//if (isset($content['field_link'])) $extra .= render($content['field_link']);
	
	$col1->addContent(get_content_col1("", $image, render($content['node_result']['current_page']), render($content['node_result']['node_count']), $created, $extra));
	
	$group->addCol1($col1);

	//
	//	COL2
	//
	$col2 = new GroupElement("black", "left");
	$col2->addContent(render($content['node_result']['pager']));
	$group->addCol2($col2);
	
	$rubrique->addGroup($group);
	
	return $rubrique;
}





//
function get_rollover_image($name, $url, $content) {
	
	$field_image = reset($content['field_image']['#items']);
	
	$image_out = array(
		'item' => array(
			'uri' => $field_image['uri'],
			'alt' => $name,
			'title' => $name,
			'attributes' => array (
				'data-description' => render($content['description']),
				'class' => "state-over",
			),
		),
		'path' => array (
			'path' => $url,
			'options' => array(
				'html' => true,
			),
		),
		'image_style' => "taxonomy_icon",
	);
	
	$image_off = array(
		'item' => array(
			'uri' => $field_image['uri'],
			'alt' => $name,
			'attributes' => array (
				'class' => "state-out",
			),
		),
		'image_style' => "taxonomy_icon_bw",
	);
	
	$image_state = '<div class="image-state taxonomy-icon">';
	$image_state .= render(theme('image_formatter', $image_off));
	$image_state .= render(theme('image_formatter', $image_out));
	$image_state .= '</div>';
	
	return $image_state;

}


?>