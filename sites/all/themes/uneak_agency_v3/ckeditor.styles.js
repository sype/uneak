/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
 * This file is used/requested by the 'Styles' button.
 * The 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */
if(typeof(CKEDITOR) !== 'undefined') {
    CKEDITOR.addStylesSet( 'drupal',
    [
            /* Block Styles */

            // These styles are already available in the "Format" drop-down list, so they are
            // not needed here by default. You may enable them to avoid placing the
            // "Format" drop-down list in the toolbar, maintaining the same features.
            /*
            { name : 'Paragraph'		, element : 'p' },
            { name : 'Heading 1'		, element : 'h1' },
            { name : 'Heading 2'		, element : 'h2' },
            { name : 'Heading 3'		, element : 'h3' },
            { name : 'Heading 4'		, element : 'h4' },
            { name : 'Heading 5'		, element : 'h5' },
            { name : 'Heading 6'		, element : 'h6' },
            { name : 'Preformatted Text', element : 'pre' },
            { name : 'Address'			, element : 'address' },
            */




			{ name : 'format-h1'						, element : 'span'		, attributes : {class : 'text-format-h1'} },
			{ name : 'format-h2'						, element : 'span'		, attributes : {class : 'text-format-h2'} },
			{ name : 'format-h3'						, element : 'span'		, attributes : {class : 'text-format-h3'} },
			{ name : 'format-h3-bold'					, element : 'span'		, attributes : {class : 'text-format-h3-bold'} },
			{ name : 'format-h4'						, element : 'span'		, attributes : {class : 'text-format-h4'} },
			{ name : 'format-normal'					, element : 'span'		, attributes : {class : 'text-format-normal'} },
			{ name : 'format-normal-arial'				, element : 'span'		, attributes : {class : 'text-format-normal-arial'} },
			{ name : 'format-mini'						, element : 'span'		, attributes : {class : 'text-format-mini'} },
			{ name : 'format-mini-bold'					, element : 'span'		, attributes : {class : 'text-format-mini-bold'} },
			{ name : 'format-uppercase'					, element : 'span'		, attributes : {class : 'text-format-uppercase'} },


			{ name : 'size-h1'							, element : 'span'		, attributes : {class : 'h1'} },            
			{ name : 'size-h2'							, element : 'span'		, attributes : {class : 'h2'} },
			{ name : 'size-h3'							, element : 'span'		, attributes : {class : 'h3'} },
			{ name : 'size-h4'							, element : 'span'		, attributes : {class : 'h4'} },
			{ name : 'size-normal'						, element : 'span'		, attributes : {class : 'font-size-normal'} },
			{ name : 'size-mini'						, element : 'span'		, attributes : {class : 'font-size-mini'} },
			
			{ name : 'font-arial'						, element : 'span'		, attributes : {class : 'font-arial'} },
			{ name : 'font-regular'						, element : 'span'		, attributes : {class : 'font-regular'} },
			{ name : 'font-light'						, element : 'span'		, attributes : {class : 'font-light'} },
            { name : 'font-bold'						, element : 'span'		, attributes : {class : 'font-bold'} },

			{ name : 'color-black-light'			, element : 'span'		, attributes : {class : 'text-color-black-light'} }, 
			{ name : 'color-black'					, element : 'span'		, attributes : {class : 'text-color-black'} },
			{ name : 'color-black-dark'				, element : 'span'		, attributes : {class : 'text-color-black-dark'} }, 
			{ name : 'color-white-light'			, element : 'span'		, attributes : {class : 'text-color-white-light'} },
			{ name : 'color-white'					, element : 'span'		, attributes : {class : 'text-color-white'} },    
			{ name : 'color-white-dark'				, element : 'span'		, attributes : {class : 'text-color-white-dark'} },  
			{ name : 'color-sky'					, element : 'span'		, attributes : {class : 'text-color-sky'} },    
			
			


            /* Inline Styles */

            // These are core styles available as toolbar buttons. You may opt enabling
            // some of them in the "Styles" drop-down list, removing them from the toolbar.
            /*
            { name : 'Strong'			, element : 'strong', overrides : 'b' },
            { name : 'Emphasis'			, element : 'em'	, overrides : 'i' },
            { name : 'Underline'		, element : 'u' },
            { name : 'Strikethrough'	, element : 'strike' },
            { name : 'Subscript'		, element : 'sub' },
            { name : 'Superscript'		, element : 'sup' },
            */

            { name : 'Marker: Yellow'	, element : 'span', styles : { 'background-color' : 'Yellow' } },
            { name : 'Marker: Green'	, element : 'span', styles : { 'background-color' : 'Lime' } },

            { name : 'Big'				, element : 'big' },
            { name : 'Small'			, element : 'small' },
            { name : 'Typewriter'		, element : 'tt' },

            { name : 'Computer Code'	, element : 'code' },
            { name : 'Keyboard Phrase'	, element : 'kbd' },
            { name : 'Sample Text'		, element : 'samp' },
            { name : 'Variable'			, element : 'var' },

            { name : 'Deleted Text'		, element : 'del' },
            { name : 'Inserted Text'	, element : 'ins' },

            { name : 'Cited Work'		, element : 'cite' },
            { name : 'Inline Quotation'	, element : 'q' },

            { name : 'Language: RTL'	, element : 'span', attributes : { 'dir' : 'rtl' } },
            { name : 'Language: LTR'	, element : 'span', attributes : { 'dir' : 'ltr' } },

            /* Object Styles */

            {
                    name : 'Image on Left',
                    element : 'img',
                    attributes :
                    {
                            'style' : 'padding: 5px; margin-right: 5px',
                            'border' : '2',
                            'align' : 'left'
                    }
            },

            {
                    name : 'Image on Right',
                    element : 'img',
                    attributes :
                    {
                            'style' : 'padding: 5px; margin-left: 5px',
                            'border' : '2',
                            'align' : 'right'
                    }
            }
    ]);
}