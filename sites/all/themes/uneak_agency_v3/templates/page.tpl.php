<?php
global $default_layers, $main_content;
$system_main = $page['content']['system_main'];

if (arg(0) == 'references' || arg(0) == 'taxonomy') {
	$node = node_load(124);
} else if (arg(0) == 'node') {
	$node = node_load(arg(1));
}



//print_r($node);

if (isset($node->nid)) $node_content = $system_main['nodes'][$node->nid]; else $node_content = array();

//print_r($system_main);

/*
$layers = new Layers($node_content['#node']->field_layer);
$clone = $default_layers->cloneMe();
$clone->merge($layers);
$main_content->layers = $clone;
$main_content->color = $node_content["#node"]->field_color['und'][0]['jquery_colorpicker'];
*/

//print_r($page['content']['metatags']);
//render($page['content']['metatags']);


//
//
//	MESSAGE AND ERROR
//
//

$highlighted = render($page['highlighted']);
$content_aside = render($page['content_aside']);
$help = render($page['help']);
$secondary_content = render($page['secondary_content']);
$action_links = render($action_links);

if(!$is_admin) {
$messages = "";
}

if ($help || $secondary_content || $messages) {

	$rubrique_message = new Rubrique();
	$rubrique_message->margin = "maxi";
	$rubrique_message->cover = "null";
	
		$group = new Group();
		$group->fullscreen = true;
		
			//
			//	COL2
			//
			if ($help || $secondary_content || $messages) {
				$col2 = new GroupElement("black", "left");
				$col2->addContent($help);
				$col2->addContent($secondary_content);
				$col2->addContent($messages);
				
				$group->addCol2($col2);
			}
		$rubrique_message->addGroup($group);
	
	$main_content->addRubrique($rubrique_message);
}




if ($highlighted || $content_aside || $primary_local_tasks || $secondary_local_tasks || $action_links) {

	$rubrique_message = new Rubrique();
	$rubrique_message->margin = "none";
	$rubrique_message->cover = "null";
	
		$group = new Group();
		$group->fullscreen = true;
			//
			//	COL2
			//
			
			if ($primary_local_tasks || $secondary_local_tasks || $action_links = render($action_links)) {
				$col2 = new GroupElement("white", "left", " stop-ajax-link");
				
				$col2->addContent('<div id="tasks">');
				if ($primary_local_tasks) $col2->addContent('<ul class="tabs primary clearfix">'.render($primary_local_tasks).'</ul>');
				if ($secondary_local_tasks) $col2->addContent('<ul class="tabs secondary clearfix">'.render($secondary_local_tasks).'</ul>');
				if ($action_links) $col2->addContent('<ul class="action-links clearfix">'.$action_links.'</ul>');
				$col2->addContent('</div>');
				
				$col2->addContent($highlighted);
				$col2->addContent($content_aside);
				
				$group->addCol2($col2);
			}
			
		
		$rubrique_message->addGroup($group);
	
	$main_content->addRubrique($rubrique_message);
}




//
//
//	MAIN NODE
//
//



$main_node = get_rubrique_node($node_content, "mini", true, true);
$main_node->margin = "maxi";
$main_content->cover = $main_node->cover;
$main_content->addRubrique($main_node);
$main_content->layers = $main_node->layers->cloneMe();
if ($main_node->color) $main_content->color = $main_node->color;
$main_content->title = $title;


$node_type = "";
if (isset($node->type)) $node_type = $node->type;




if ($node_type == "page") {		// MERGE LES LAYERS DU NODE PRINCIPAL VERS LA RACINES


} else if ($node_type == "agency_accueil") {

	
	
	if (isset($node_content['field_projet'])) {
		$field_projet = get_rubrique_entity_reference($node_content['field_projet'], "normal", false);
		$field_projet->separator = t("Featured project");
		$field_projet->margin = "maxi";
		$main_content->addRubrique($field_projet);
	}
	if (isset($node_content['field_acualite'])) {
		$field_acualite = get_rubrique_entity_reference($node_content['field_acualite'], "normal", false);
		$field_acualite->separator = t("News");
		$field_acualite->margin = "maxi";
		$main_content->addRubrique($field_acualite);
	}
	
	if (isset($node_content['field_client_direct'])) {
		$field_client = get_rubrique_client($node_content['field_client_direct']);
		$field_client->separator = t("Clients");
		$main_content->addRubrique($field_client);
	}
	
} else if ($node_type == "agency_clients") {
	
	
	$field_client_agence = get_rubrique_client($node_content['field_client_agence']);
	$field_client_agence->margin = "normal";
	$main_content->addRubrique($field_client_agence);
	
	$field_client_client = get_rubrique_client($node_content['field_client_client']);
	$main_content->addRubrique($field_client_client);

} else if ($node_type == "projet" || $node_type == "article" || $node_type == "tutoriel") {
	
	
	$main_node->baseline = format_date($node->created, 'long');
	
	
	if (isset($node_content['field_video']) || isset($node_content['field_gallery'])) {
		$rub_media = get_rubrique_media($node_content, "none");
		$rub_media->margin = "maxi";
		$main_content->addRubrique($rub_media);
	}
	
	$rub_technique = get_rubrique_technique($node_content);
	$rub_technique->margin = "maxi";
	$main_content->addRubrique($rub_technique);
	
	
//	$rub_social = get_rubrique_social($node_content);
//	$main_content->addRubrique($rub_social);
	

} else {

	$rubrique = new Rubrique();
	$rubrique->cover = "null";
	
	
	$group = new Group();
	$group->fullscreen = true;
	//
	//	COL1
	//
	$col1 = new GroupElement("black", "left");
	$col1->addContent(get_content_col1("", "", $title, "", "", ""));
	$group->addCol1($col1);

	//
	//	COL2
	//
	$col2 = new GroupElement("white", "left");
	$col2->addContent(render($page['content']));
	$group->addCol2($col2);
	
	$rubrique->addGroup($group);
		
	$main_content->addRubrique($rubrique);
	
}


if (isset($node->nid) && $node->nid == 124) {

	$main_node->margin = "none";

	$ref_entete = get_rubrique_ref_entete($system_main['term_entete']);
	$ref_entete->margin = "none";
	$main_content->addRubrique($ref_entete);
	
	if (isset($system_main['node_result']['no_content'])) {
		$node_result = get_rubrique_ref_no_content($system_main['node_result']['no_content']);
		$node_result->margin = "none";
		$main_content->addRubrique($node_result);
		
		$ref_filter = get_rubrique_ref_filter($system_main);
		$ref_filter->margin = "maxi";
		$main_content->addRubrique($ref_filter);
		
	} else {

		$ref_filter = get_rubrique_ref_filter($system_main);
		$ref_filter->margin = "maxi";
		$main_content->addRubrique($ref_filter);
		
		$ref_result_count = get_rubrique_ref_result_count($system_main);
		$ref_result_count->margin = "normal";
		$main_content->addRubrique($ref_result_count);

		if (isset($system_main['node_result']['nodes'])) {
			$node_result = get_rubrique_ref_node($system_main['node_result']['nodes'], "normal");
			$node_result->margin = "normal";
			$main_content->addRubrique($node_result);
		}

		if (isset($system_main['node_result']['pager']) && render($system_main['node_result']['pager'])) $main_content->addRubrique(get_rubrique_ref_pager($system_main));

	}
	
	
}

//print_r($node_content);


//print_r($node_content['field_projet']);
//$node_content['field_acualite']['#items']


/*
$node->type == "agency_accueil"
$node->type == "page"
$node->type == "agency_clients"
$node->nid == 124
*/






?>
<div id="page" class="<?php print $classes; ?>">

    <div id="background-sky-container"><?php print $main_content->buildLayers(); ?></div>

	<div id="page-content">
        <div id="page-content-wrapper">
        
            <div id="page-header">
                <div id="page-header-wrapper">
                    <?php print render($page['leaderboard']); ?>
    
                    <header id="header" class="layout-block clearfix" role="banner">
                        <div id="header-wrapper">
                            <?php if ($site_logo || $site_name || $site_slogan): ?>
                                <div id="branding" class="branding-elements">
                                    <?php if ($site_logo): ?><div id="logo"><?php print $site_logo; ?></div><?php endif; ?>
                                    <?php if ($site_name || $site_slogan): ?>
                                        <hgroup id="name-and-slogan"<?php print $hgroup_attributes; ?>>
                                            <?php
                                            	if ($site_name):
                                            	$site_frontpage = variable_get('site_frontpage', url());
											 ?>
                                                <a href="/<?php print $site_frontpage; ?>"><h1 <?php print $site_name_attributes; ?>><span class="text-format-h3">Uneak</span><?php print variable_get('site_name'); ?></h1></a>
											<?php endif; ?>
                                            
											<?php if ($site_slogan): ?><h2 <?php print $site_slogan_attributes; ?>><?php print $site_slogan; ?></h2><?php endif; ?>
                                        </hgroup>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <?php print render($page['header']); ?>
                        </div>
                    </header>
                    
                    <div id="navigation" class="layout-block">
                    	<div id="navigation-wrapper">
                            <header<?php print $content_header_attributes; ?>>
                            	<div id="breadcrumb-container"><!-- BEGIN:BREADCRUMB --><?php if ($breadcrumb): print $breadcrumb; endif; ?><!-- END:BREADCRUMB --></div>
                                <h1 id="rubrique-title"><!-- BEGIN:TITLE --><span class="rubrique-title"><?php print $main_content->title; ?></span><!-- END:TITLE --></h1>
                                <div id="rubrique-sous-title"><!-- BEGIN:BASELINE --><span class="rubrique-sous-title"><?php print $main_content->baseline; ?></span><!-- END:BASELINE --></div>
                            </header>
                            
                            <div id="loading-animation">
                                <div id="loading-animation-wrapper"></div>
                            </div>
                            <div id="top-menu">
                                <div id="top-menu-wrapper">
                                    <!-- BEGIN:TOP_MENU -->
                                    <?php print render($page['menu_bar']); ?>
                                    <?php if ($primary_navigation): print $primary_navigation; endif; ?>
                                    <?php if ($secondary_navigation): print $secondary_navigation; endif; ?>
                                    <!-- END:TOP_MENU -->
                                </div>
                            </div>
						</div>
                    </div>
                    
                    <?php
                        $cover_image = array(
                            'item' => array(
                                //'uri' => $field_image['uri'],
                                'alt' => $main_content->title,
                                'attributes' => array (
                                    'class' => "state-out",
                                ),
                            ),
                            'image_style' => "reference_image",
                        );
                        
                        
                        //$image_state .= render(theme('image_formatter', $cover_image));
                    ?>              
                    <div id="cover" class="layout-block">
                    	<div id="cover-wrapper">
                        	<div id="cover-image"><!-- BEGIN:COVER --><?php if ($main_content->cover && $main_content->cover != 'null') { ?><div id="cover-div-background" class="image-cover" style="background-image:url('<?php print $main_content->cover; ?>');">&nbsp;</div><?php } ?> <!-- END:COVER --></div>
						</div>
                    </div>
                </div>
            </div>
            
    
            <?php render($page['content']); ?>
            <<?php print $tag; ?> id="main-content-node"><!-- BEGIN:MAIN_CONTENT --><?php print $main_content->render(); ?><!-- END:MAIN_CONTENT --></<?php print $tag; ?>>
        
        </div>
    </div>
	
    <footer<?php print $footer_attributes; ?>><div id="footer-container"><!-- BEGIN:FOOTER --><?php print render($page['footer']); ?><!-- END:FOOTER --></div><?php if ($main_content->layers->getLayerById("FOOTER")->background) print $main_content->layers->getLayerById("FOOTER")->buildLayer(); ?></footer>
    
    
</div>

<script language="javascript">
	var ua = new UneakAgency("/<?php print path_to_theme(); ?>");
</script>
