<?php
/**
 * @file
 * Adaptivetheme implementation to display a node.
 *
 * Adaptivetheme variables:
 * AT Core sets special time and date variables for use in templates:
 * - $submitted: Submission information created from $name and $date during
 *   adaptivetheme_preprocess_node(), uses the $publication_date variable.
 * - $datetime: datetime stamp formatted correctly to ISO8601.
 * - $publication_date: publication date, formatted with time element and
 *   pubdate attribute.
 * - $datetime_updated: datetime stamp formatted correctly to ISO8601.
 * - $last_update: last updated date/time, formatted with time element and
 *   pubdate attribute.
 * - $custom_date_and_time: date time string used in $last_update.
 * - $header_attributes: attributes such as classes to apply to the header element.
 * - $footer_attributes: attributes such as classes to apply to the footer element.
 * - $links_attributes: attributes such as classes to apply to the nav element.
 * - $is_mobile: Bool, requires the Browscap module to return TRUE for mobile
 *   devices. Use to test for a mobile context.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 * @see adaptivetheme_preprocess_node()
 * @see adaptivetheme_process_node()
 */

/**
 * Hiding Content and Printing it Separately
 *
 * Use the hide() function to hide fields and other content, you can render it
 * later using the render() function. Install the Devel module and use
 * <?php print dsm($content); ?> to find variable names to hide() or render().
 */



	hide($content['comments']);
	hide($content['links']);
	hide($content['body']);
	
	hide($content['field_link']);
	hide($content['field_image']);
	
	hide($content['field_video']);
	hide($content['field_gallery']);
	
	hide($content['field_client']);
	hide($content['field_agency']);
	hide($content['field_type']);
	hide($content['field_technology']);
	hide($content['field_logiciel']);
	hide($content['field_etiquettes']);

	render($content);
	
	

if ($view_mode == "teaser") {

?>
    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    	<?php
	
			print render($title_prefix);
			
			$render_content = "";
			
			$render_content .= "<div class='chapter-col-1 clearfix'>";
				
				$render_content .= "<div class='chapter-cover'>";
					
					$render_content .= "<div class='chapter-cover-wrapper'>";
						
						$render_content .= "<a href='".$node_url."' class='chapter-cover-image'>";
						
							$render_content .= '<div class="image-cover" style="background-image:url(\''.file_create_url($content['field_image']['#items'][0]['uri']).'\');">&nbsp;</div>';
							
						$render_content .= "</a>";
					
					$render_content .= "</div>";
					
				$render_content .= "</div>";
	
/*			
				$render_content .= "<div class='chapter-links'>";
	
				$link_item = array();
				foreach ($content['links'] as $delta => $link_type) {
					if (substr($delta, 0, 1) != "#") {
						foreach ($link_type['#links'] as $link) {
							$link_item[] = array( 'data' => l($link['title'], $link['href'], $link));
						}
					}
				}
				if (count($link_item) != 0) {
					$type = 'ul';
					$attributes = array('class' => 'chapter-link-items');
					$render_content .= render(theme('block_item_list', array('items' => $link_item, 'title' => '', 'type' => $type, 'attributes' => $attributes)));
				}
				$render_content .= "</div>";
*/				
			$render_content .= "</div>";
				
			
			$render_content .= "<div class='chapter-col-2 clearfix'>";
				$render_content .= "<div class='chapter-more'>";
				
					$render_content .= "<div class='chapter-title'>";
					if ($title && !$page):
                    	$render_content .= "<header".$header_attributes.">";
						$f_created = format_date($created, 'long');
						$render_content .= '<span class="text-format-mini">'.$f_created.'</span>';
						if ($title) $render_content .= '<h2 class="text-format-h3-bold"><a href="'.$node_url.'" rel="bookmark">'.$title.'</a></h2>';
						$render_content .= "</header>";
					endif;
					$render_content .= "</div>";
				
				$render_content .= "</div>";
				
				$render_content .= "<div class='chapter-desc'>";
					$render_content .= "<div class='chapter-body'>";
						$render_content .= render($content['body']);
					$render_content .= "</div>";
				$render_content .= "</div>";
				
			$render_content .= "</div>";
			
			print $render_content;		
	 	?>
	 
    
      <?php print render($title_suffix); ?>
    </article>
<?php

} else {
		
?>
    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
		<?php print render($title_prefix); ?>
        
        <?php print render($content['field_chapter']); ?>
        
		<?php print render($content['field_video']); ?>
        <?php print render($content['field_gallery']); ?>
        <?php
		 	$spec_tech = "";
			$spec_tech .= render($content['field_client']);
			$spec_tech .= render($content['field_agency']);
			$spec_tech .= render($content['field_technology']);
			$spec_tech .= render($content['field_logiciel']);
			$spec_tech .= render($content['field_etiquettes']);
		 	if ($spec_tech) { 
		 ?>
        <div class="spec-tech">
        	<?php print $spec_tech; ?>
        </div>
        <?php } ?>
        <?php //print render($content['comments']); ?>
        
        <?php print render($title_suffix); ?>
    </article>
<?php
}
?>