<?php
/**
 * @file
 * Adaptivetheme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * Adaptivetheme supplied variables:
 * - $site_logo: Themed logo - linked to front with alt attribute.
 * - $site_name: Site name linked to the homepage.
 * - $site_name_unlinked: Site name without any link.
 * - $hide_site_name: Toggles the visibility of the site name.
 * - $visibility: Holds the class .element-invisible or is empty.
 * - $primary_navigation: Themed Main menu.
 * - $secondary_navigation: Themed Secondary/user menu.
 * - $primary_local_tasks: Split local tasks - primary.
 * - $secondary_local_tasks: Split local tasks - secondary.
 * - $tag: Prints the wrapper element for the main content.
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see adaptivetheme_preprocess_page()
 * @see adaptivetheme_process_page()
 */



class Layer {
	var $id;
	var $background;
	var $distance;
	var $repeat;
	var $size;
	var $offset;
	var $align_horizontal;
	var $align_vertical;
	var $width;
	var $height;
	
	function __construct($item = NULL) {
		if (isset($item)) $this->parse($item);
    }
	
	function parse($item) {
		
		$this->id = $item->field_layer_id['und'][0]['value'];
		if (isset($item->field_image['und'][0]['uri'])) $this->background = file_create_url($item->field_image['und'][0]['uri']);
		if (isset($item->field_distance['und'][0]['value'])) $this->distance = $item->field_distance['und'][0]['value'];
		if (isset($item->field_repeat['und'][0]['value'])) $this->repeat = $item->field_repeat['und'][0]['value'];
		if (isset($item->field_background_size['und'][0]['value'])) $this->size = $item->field_background_size['und'][0]['value'];
		if (isset($item->field_offset['und'][0]['value'])) $this->offset = $item->field_offset['und'][0]['value'];
		if (isset($item->field_align_horizontal['und'][0]['value'])) $this->align_horizontal = $item->field_align_horizontal['und'][0]['value'];
		if (isset($item->field_align_vertical['und'][0]['value'])) $this->align_vertical = $item->field_align_vertical['und'][0]['value'];
		if (isset($item->field_width['und'][0]['value'])) {
			$this->width = $item->field_width['und'][0]['value'];
		} else if (isset($item->field_image['und'][0]['image_dimensions']['width'])) {
			$this->width = $item->field_image['und'][0]['image_dimensions']['width'];
		}
		if (isset($item->field_height['und'][0]['value'])) {
			$this->height = $item->field_height['und'][0]['value'];
		} else if (isset($item->field_image['und'][0]['image_dimensions']['height'])) {
			$this->height = $item->field_image['und'][0]['image_dimensions']['height'];
		}
		
	}
	
	function getArray() {
		$ret = array();
		if ($this->background) $ret["background"] = $this->background;
		if ($this->distance) $ret["distance"] = $this->distance;
		if ($this->repeat) $ret["repeat"] = $this->repeat;
		if ($this->size) $ret["size"] = $this->size;
		if ($this->offset) $ret["offset"] = $this->offset;
		if ($this->align_horizontal) $ret["align_horizontal"] = $this->align_horizontal;
		if ($this->align_vertical) $ret["align_vertical"] = $this->align_vertical;
		if ($this->width) $ret["width"] = $this->width;
		if ($this->height) $ret["height"] = $this->height;
		return $ret;	
	}
	
	function cloneMe() {
		$clone = new Layer();
		$clone->id = $this->id;
		$clone->background = $this->background;
		$clone->distance = $this->distance;
		$clone->repeat = $this->repeat;
		$clone->size = $this->size;
		$clone->offset = $this->offset;
		$clone->align_horizontal = $this->align_horizontal;
		$clone->align_vertical = $this->align_vertical;
		$clone->width = $this->width;
		$clone->height = $this->height;
		return $clone;
	}
	
	function buildLayer() {
		$render = '';
		if ($this->background) {
			if (!$this->repeat) $this->repeat = "no-repeat";
			
			if ($this->repeat != "no-repeat" || $this->size == "cover") {
				$render .= '<div class="layer" style="';
				$render .= 'background-image:url('.$this->background.');';
				$render .= 'background-repeat:'.$this->repeat.';';
				if ($this->size == "cover") {
					$render .= 'background-size:cover;';
					$render .= 'width:100%;';
					$render .= 'height:100%;';
				} else {
					if ($this->repeat == "repeat-x") {
						$render .= 'width:100%;';
						$render .= 'height:'.$this->height.'px;';
					} else if ($this->repeat == "repeat-y") {
						$render .= 'width:'.$this->width.'px;';
						$render .= 'height:100%;';
					} else {
						$render .= 'width:'.$this->width.'px;';
						$render .= 'height:'.$this->height.'px;';
					}
					
					if ($this->align_vertical == "top") {
						$render .= 'top:0px;';
						$render .= 'position:absolute;';
					} else if ($this->align_vertical == "bottom") {
						$render .= 'bottom:0px;';
						$render .= 'position:absolute;';
					} else {
						$render .= 'margin-top:-'.($this->height/2).'px;';
						$render .= 'top:50%;';
					}
										
					if ($this->align_horizontal == "left") {
						$render .= 'left:0px;';
						$render .= 'position:absolute;';
					} else if ($this->align_horizontal == "right") {
						$render .= 'right:0px;';
						$render .= 'position:absolute;';
					} else {
						$render .= 'display:block;';
						$render .= 'margin-left:auto;';
						$render .= 'margin-right:auto;';
						$render .= 'position:relative;';
					}
				}
				
				$render .= '"></div>';
			} else {
				$render .= '<img class="layer" src="'.$this->background.'" style="';
				$render .= 'max-width:none;';
				$render .= 'max-height:none;';
				
				$render .= 'width:'.$this->width.'px;';
				$render .= 'height:'.$this->height.'px;';
					
				if ($this->align_vertical == "top") {
					$render .= 'top:0px;';
					$render .= 'position:absolute;';
				} else if ($this->align_vertical == "bottom") {
					$render .= 'bottom:0px;';
					$render .= 'position:absolute;';
				} else {
					$render .= 'margin-top:-'.($this->height/2).'px;';
					$render .= 'top:50%;';
				}
									
				if ($this->align_horizontal == "left") {
					$render .= 'left:0px;';
					$render .= 'position:absolute;';
				} else if ($this->align_horizontal == "right") {
					$render .= 'right:0px;';
					$render .= 'position:absolute;';
				} else {
					$render .= 'display:block;';
					$render .= 'margin-left:auto;';
					$render .= 'margin-right:auto;';
					$render .= 'position:relative;';
				}
				
				
				$render .= '" />';
			}
		}
	
		return $render;
	}

}

class Layers {
	var $layer;
	
	function __construct($field_layer = NULL) {
        $this->layer = array();
		
		if (isset($field_layer)) $this->parse($field_layer);
    }
	
	function parse($field_layer) {
		if (count($field_layer)) {
			foreach($field_layer['und'] as $itemid) {
				$item = entity_load('field_collection_item', $itemid);
				//print_r($item);
				if ($item[$itemid["value"]]->field_layer_id && $item[$itemid["value"]]->field_layer_id['und'][0]['value']) $this->addLayer(new Layer($item[$itemid["value"]]));
			}
		}
	}
	
	function getLayerById($id) {
		for($i = 0; $i < count($this->layer); $i++) {
			if ($this->layer[$i]->id == $id) return $this->layer[$i];
		}
		return NULL;
	}
	
	function addLayer($layer) {
		array_push($this->layer, $layer);
	}
	
	function merge($layers) {
		for($i = 0; $i < count($layers->layer); $i++) {
			$found = -1;
			for($j = 0; $j < count($this->layer); $j++) {
				if ($layers->layer[$i]->id == $this->layer[$j]->id) {
					$found = $j;
					break;
				}
			}
			if ($found != -1) array_splice($this->layer, $found, 1);
			$this->addLayer($layers->layer[$i]->cloneMe());
		}
	}
	
	function cloneMe() {
		$clone = new Layers();
		for($i = 0; $i < count($this->layer); $i++) $clone->addLayer($this->layer[$i]->cloneMe());
		return $clone;
	}
	
	function getArray() {
		$ret = array();
		for($i = 0; $i < count($this->layer); $i++) $ret[$this->layer[$i]->id] = $this->layer[$i]->getArray();
		return $ret;
	}
	
	function getJSON() {
		return json_encode($this->getArray());
	}
	
	function getProperty() {
		if (count($this->layer)) {
			return " data-layer='".json_encode($this->getArray())."'";
		} else {
			return "";
		}
	}
	
	
	
	function buildLayers() {
		$render = '';
		$render .= '<div id="background-sky-4" class="background-sky">'.$this->getLayerById("LAYER-4")->buildLayer().'</div>';
		$render .= '<div id="background-sky-3" class="background-sky">'.$this->getLayerById("LAYER-3")->buildLayer().'</div>';
		$render .= '<div id="background-sky-2" class="background-sky">'.$this->getLayerById("LAYER-2")->buildLayer().'</div>';
		$render .= '<div id="background-sky-1" class="background-sky">'.$this->getLayerById("LAYER-1")->buildLayer().'</div>';
		$render .= '<div id="background-sky-0" class="background-sky">'.$this->getLayerById("LAYER-0")->buildLayer().'</div>';
		
		return $render;
	}

}


class HeaderInfo {
	var $layers;
	var $color;
	
	function buildLayers() {
		$render = '';
		$render .= '<div id="background-sky-color" class="background-sky" style="background-color:'.$this->getColor().';"></div>';
		$render .= $this->layers->buildLayers();
		return $render;
	}


	function getColor() {
		$render = 'rgb(';
		$rgb = $this->html2rgb($this->color);
		$render .= ' '.$rgb[0].',';
		$render .= ' '.$rgb[1].',';
		$render .= ' '.$rgb[2];
		$render .= ')';
		return $render;
	}
	
	function html2rgb($color) {
		if ($color[0] == '#')
			$color = substr($color, 1);
	
		if (strlen($color) == 6)
			list($r, $g, $b) = array($color[0].$color[1],
									 $color[2].$color[3],
									 $color[4].$color[5]);
		elseif (strlen($color) == 3)
			list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
		else
			return false;
	
		$r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
	
		return array($r, $g, $b);
	}
}

        


$default_layers = new Layers();



$default_layer_0 = new Layer();
$default_layer_0->id = 'LAYER-0';
$default_layer_0->background = '/'.path_to_theme().'/images/background-sky-1.png';
$default_layer_0->distance = 50;
$default_layer_0->repeat = 'repeat-x';
$default_layer_0->size = 'none';
$default_layer_0->offset = "horizontal";
$default_layer_0->align_horizontal = "left";
$default_layer_0->align_vertical = "bottom";
$default_layer_0->width = 1600;
$default_layer_0->height = 325;
$default_layers->addLayer($default_layer_0);

$default_layer_1 = new Layer();
$default_layer_1->id = 'LAYER-1';
$default_layer_1->background = '/'.path_to_theme().'/images/background-sky-2.png';
$default_layer_1->distance = 40;
$default_layer_1->repeat = 'repeat-x';
$default_layer_1->size = 'none';
$default_layer_1->offset = "horizontal";
$default_layer_1->align_horizontal = "left";
$default_layer_1->align_vertical = "bottom";
$default_layer_1->width = 1600;
$default_layer_1->height = 567;
$default_layers->addLayer($default_layer_1);
 
$default_layer_2 = new Layer();
$default_layer_2->id = 'LAYER-2';	//BACKGROUND
$default_layer_2->background = '';
$default_layer_2->distance = 30;
$default_layer_2->repeat = 'repeat-x';
$default_layer_2->size = 'none';
$default_layer_2->offset = "horizontal";
$default_layer_2->align_horizontal = "left";
$default_layer_2->align_vertical = "bottom";
$default_layers->addLayer($default_layer_2);
 
$default_layer_3 = new Layer();
$default_layer_3->id = 'LAYER-3';
$default_layer_3->background = '/'.path_to_theme().'/images/background-sky-3.png';
$default_layer_3->distance = 20;
$default_layer_3->repeat = 'repeat-x';
$default_layer_3->size = 'none';
$default_layer_3->offset = "horizontal";
$default_layer_3->align_horizontal = "left";
$default_layer_3->align_vertical = "bottom";
$default_layer_3->width = 1610;
$default_layer_3->height = 650;
$default_layers->addLayer($default_layer_3);

$default_layer_4 = new Layer();
$default_layer_4->id = 'LAYER-4';
$default_layer_4->background = '/'.path_to_theme().'/images/background-sky-star.png';
$default_layer_4->distance = 10;
$default_layer_4->repeat = 'repeat-x';
$default_layer_4->size = 'none';
$default_layer_4->offset = "horizontal";
$default_layer_4->align_horizontal = "left";
$default_layer_4->align_vertical = "top";
$default_layer_4->width = 1600;
$default_layer_4->height = 460;
$default_layers->addLayer($default_layer_4);
//
//



$clone = $default_layers->cloneMe();
$clone->merge(new Layers($node->field_layer));
$header_info = new HeaderInfo();
$header_info->layers = $clone;
if (isset($node->field_color)) $header_info->color = $node->field_color['und'][0]['jquery_colorpicker']; else $header_info->color = "246ca0";




?>
<div id="page" class="<?php print $classes; ?>">
    <div id="background-sky-container">
    	<?php print $header_info->buildLayers(); ?>
	
        <div id="col-1" class="col">
            <div class="col-1-wrapper">
                <!-- region: Leaderboard -->
                <?php print render($page['leaderboard']); ?>
                
                <header id="header" class="clearfix" role="banner">
                    <?php if ($site_logo || $site_name || $site_slogan): ?>
                        <div id="branding" class="branding-elements clearfix">
                            <?php if ($site_logo): ?><div id="logo"><?php print $site_logo; ?></div><?php endif; ?>
                            <?php if ($site_name || $site_slogan): ?>
                                <hgroup id="name-and-slogan"<?php print $hgroup_attributes; ?>>
                                    <?php if ($site_name): ?><h1 id="site-name"<?php print $site_name_attributes; ?>><?php print $site_name; ?></h1><?php endif; ?>
                                    <?php if ($site_slogan): ?><h2 id="site-slogan"<?php print $site_slogan_attributes; ?>><?php print $site_slogan; ?></h2><?php endif; ?>
                                </hgroup>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php print render($page['header']); ?>
                </header>
        
                <?php print render($page['menu_bar']); ?>
                <nav class="profile-menu">
                    <?php print profile_menu(1, false); ?>
                </nav>
                <?php if ($primary_navigation): print $primary_navigation; endif; ?>
                <?php if ($secondary_navigation): print $secondary_navigation; endif; ?>
                
                <?php $sidebar_first = render($page['sidebar_first']); print $sidebar_first; ?>
            </div>    
        </div>

	</div>
    <div id="col-2" class="col">
        <header id="header-title">
            <div class="header-title-wrapper">
                <?php if ($breadcrumb): print $breadcrumb; endif; ?>
                <?php if ($title): ?><h1 id="page-title"<?php print $attributes; ?>><?php print $title; ?></h1><?php endif; ?>
            </div>
        </header>
        <div class="col-2-wrapper">
            <!-- Messages and Help -->
            <?php print $messages; ?>
            <?php print render($page['help']); ?>
            
            <!-- region: Secondary Content -->
            <?php print render($page['secondary_content']); ?>
              
            <!-- region: Highlighted -->
            <?php print render($page['highlighted']); ?>
            
            <<?php print $tag; ?> id="main-content">
            
                <?php print render($title_prefix); // Does nothing by default in D7 core ?>
                
                <?php if ($primary_local_tasks || $secondary_local_tasks || $action_links = render($action_links)): ?>
                    <div id="tasks">
                        <?php if ($primary_local_tasks): ?><ul class="tabs primary clearfix"><?php print render($primary_local_tasks); ?></ul><?php endif; ?>
                        <?php if ($secondary_local_tasks): ?><ul class="tabs secondary clearfix"><?php print render($secondary_local_tasks); ?></ul><?php endif; ?>
                        <?php if ($action_links = render($action_links)): ?><ul class="action-links clearfix"><?php print $action_links; ?></ul><?php endif; ?>
                    </div>
                <?php endif; ?>

                
                <!-- region: Main Content -->
                <?php if ($content = render($page['content'])): ?>
                    <div id="content">
                        <?php print $content; ?>
                    </div>
                <?php endif; ?>
                
                <?php //print $feed_icons; ?>
                
                <?php print render($title_suffix); // Prints page level contextual links ?>
                
            </<?php print $tag; ?>><!-- /end #main-content -->
                
            <!-- region: Content Aside -->
            <?php print render($page['content_aside']); ?>

            <?php $sidebar_second = render($page['sidebar_second']); print $sidebar_second; ?>
            
            <?php print render($page['tertiary_content']); ?>
            
        </div>
    </div>

	<?php if ($page['footer']): ?><footer id="footer" class="clearfix" role="contentinfo"><?php print render($page['footer']); ?></footer><?php endif; ?>
   
</div>
<script language="javascript">
	(function ($) {

		$(document).ready(function(){
			
			$("#col-1").mCustomScrollbar({
				scrollInertia:0,
				scrollButtons:{
					enable:false
				}
			});
			
			$("#col-2").mCustomScrollbar({
				advanced:{
					updateOnContentResize: true
				},
				scrollButtons:{
					enable:false
				}
			});
		
		});
		
	
	}(jQuery));
</script>