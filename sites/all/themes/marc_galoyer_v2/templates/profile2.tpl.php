<?php

/**
 * @file
 * Default theme implementation for profiles.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) profile type label.
 * - $url: The URL to view the current profile.
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - profile-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 
 	$profile_page = arg(0);
	$uid = arg(1);
	$user = user_load($uid);
 							
?>
<article class="profile node <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="node-data node-block node-block-full clearfix">
        <div class="node-info">
            <div class="content"<?php print $content_attributes; ?>>
                <?php print render($content); ?>
            </div>
			<?php 
                if ($profile_page == "profile-contact") {
                    form_load_include($form_state, 'inc', 'contact', 'contact.pages');
                    $form = drupal_get_form('contact_personal_form', $user);
                    $output = drupal_render($form);
                    print '<div class="node-contact">'.$output.'</div>';
                }
            ?>
        </div>
	</div>
    
</article>
