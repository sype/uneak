
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

(function($) {
	
	
	var methods = {
		
		init : function( options ) {
			return this.each(function(){
				var $this = $(this);

				$this.data({
					"sky_background":$this,
					"background-sky-color":$this.find(".background-sky-color"),
					"backgroundColor":$this.find(".background-sky-color").css("background-color"),
					"currentBackgroundColor":$this.find(".background-sky-color").css("background-color"),
					"background-sky-2":$this.find(".background-sky-2"),
					"background-sky-1":$this.find(".background-sky-1"),
					"background-sky-0":$this.find(".background-sky-0"),
					"background-sky-image":$this.find(".background-sky-image"),
					"background-sky-star":$this.find(".background-sky-star"),
					"page_image":$this.find("#cover"),
					"pX":0,
					"pY":0,
					"offsetX":0,
					"offsetY":0,
					"screenWidth":$this.find(".background-sky-star").width(),
					"screenHeight":$this.find(".background-sky-star").height()
				});
				
				$(window).bind('resize.parallaxBackground', {thisScope:$this}, methods.screenResize);
				$(document).bind('mousemove.parallaxBackground', {thisScope:$this}, methods.mousemove);
				
				$this.parallaxBackground("refresh");
			});
		},
		
		
		destroy : function( ) {
			return this.each(function(){
				var $this = $(this);
			
				$(window).unbind('.parallaxBackground');
				$(document).unbind('.parallaxBackground');
			});
	
		},
		
		screenResize : function( event ) {
			var $this = event.data.thisScope;
			$this.data("screenWidth", $this.data("background-sky-star").width());
			$this.data("screenHeight", $this.data("background-sky-star").height());
			
			var $image = $this.data("background_image");
			if ($image) $image.css("marginTop", Math.round(($this.data("screenHeight") - $image.height()) / 2) + "px");
			
			$this.parallaxBackground("refresh");
		},
		
		mousemove : function( event ) { 
			var $this = event.data.thisScope;
			$this.data("pX", event.pageX);
			$this.data("pY", event.pageY);
			$this.parallaxBackground("refresh");
		},
		
		
		setOffset : function( valueX, valueY ) {
			var $this = $(this);
			$this.data("offsetX", valueX);
			$this.data("offsetY", valueY);
			$this.parallaxBackground("refresh");
			return $this;
		},
		
		offsetX : function( value ) {
			var $this = $(this);
			if (value) {
				$this.data("offsetX", value);
				$this.parallaxBackground("refresh");
				return $this;
			} else {
				return $this.data("offsetX");
			}
		},
		
		offsetY : function( value ) {
			var $this = $(this);
			if (value) {
				$this.data("offsetY", value);
				$this.parallaxBackground("refresh");
				return $this;
			} else {
				return $this.data("offsetY");
			}
		},
		
		
		color : function() {
			var $this = $(this);
			return $this.data("currentBackgroundColor");
		},
		
		setColor : function( value ) {
			var $this = $(this);
			$this.data("background-sky-color").animate({backgroundColor: value}, {duration: 1000, queue: false, easing:"easeOutExpo"});
			$this.find("#top_menu .top_menu_wrapper .nav li a:link").animate({color: value}, {duration: 1000, queue: false, easing:"easeOutExpo"});		
			$this.find("#top_menu .top_menu_wrapper .nav li a:visited").animate({color: value}, {duration: 1000, queue: false, easing:"easeOutExpo"});
			$this.data("currentBackgroundColor", value);
			return $this;
		},
		
		resetColor : function() {
			var $this = $(this);
			$this.parallaxBackground("setColor", $this.data("backgroundColor"));
			return $this;
		},

		setBackgroundImage : function( background_path ) {
			var $this = $(this);
			var background_sky_image = $this.data("background-sky-image");
			
			var background_image = $this.data("background_image");
			
			if (background_image) {
				
				var $sky_height = background_sky_image.height();
				background_sky_image.stop().animate({top: $sky_height + 'px'}, {duration:2000, easing:"easeOutExpo", complete: function() {
					background_image.remove();
					if (background_path) $this.parallaxBackground("addBackgroundImage", background_path);
				}});			
	
			} else {
				if (background_path) $this.parallaxBackground("addBackgroundImage", background_path);
			}
			
		},
		
		
		
		addBackgroundImage : function( background_path ) {
			var $this = $(this);
			
			var image = new Image();
			image.onload = function () {
				var $image = $(image);
				$this.data("background_image", $image);
				
				var background_sky_image = $this.data("background-sky-image");
				var $sky_height = background_sky_image.height();
				background_sky_image.css({top: $sky_height + 'px'});
				
				var background_sky_image = $this.data("background-sky-image");
				background_sky_image.prepend($image);

				$image.css("marginTop", Math.round(($this.data("screenHeight") - $image.height()) / 2) + "px");
				background_sky_image.stop().animate({top: '0px'}, {duration:2000, easing:"easeOutExpo"});
			}
			image.src = background_path;
		
		},
		
		
		
		refresh : function( ) { 
			var $this = $(this);
			var pX = $this.data("pX");
			var pY = $this.data("pY");
			var offsetX = $this.data("offsetX");
			var offsetY = $this.data("offsetY");
			var screenWidth = $this.data("screenWidth");
			var screenHeight = $this.data("screenHeight");
	
			var mouseOffsetX = pX - (screenWidth/2);
			var mouseOffsetY = pY - (screenHeight/2);
	
			var globalOffsetX = offsetX - mouseOffsetX;
			var globalOffsetY = offsetY - mouseOffsetY;
	
			
			var nuage_0_height = 133;
			var nuage_1_height = 325;
			var nuage_2_height = 567;
			
			var nuage_0_Y = screenHeight - nuage_0_height;
			var nuage_1_Y = screenHeight - nuage_1_height;
			var nuage_2_Y = screenHeight - nuage_2_height;
			
			
	
			var pos_cloud_0_x = globalOffsetX/256;
				
			var pos_cloud_1_x = globalOffsetX/127;

			var pos_cloud_2_x = globalOffsetX/64;

			var pos_stars_x = globalOffsetX/32;
	

			$this.data("background-sky-2").animate({backgroundPositionX: pos_cloud_2_x}, {duration: 1000, queue: false, easing:"easeOutExpo"});
			$this.data("background-sky-1").animate({backgroundPositionX: pos_cloud_1_x}, {duration: 1000, queue: false, easing:"easeOutExpo"});
			$this.data("background-sky-0").animate({backgroundPositionX: pos_cloud_0_x}, {duration: 1000, queue: false, easing:"easeOutExpo"});
			$this.data("background-sky-star").animate({backgroundPositionX: pos_stars_x}, {duration: 1000, queue: false, easing:"easeOutExpo"});
		
			return $this;
		}

	};
		
  
  
	$.fn.parallaxBackground = function( method ) {
	
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.parallaxBackground' );
		}    
	
	};
  
  
})(jQuery);