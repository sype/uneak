<?php


/**
 * Implements hook_field_info().
 */
function uneak_taxo_note_field_info() {
	return array(
		'uneak_taxo_note_field' => array(
			'label' => t('Taxonomy Note'),
			'description' => t('Note de taxonomy.'),
			'settings' => array(
				'tid' => 0,
				'note' => "0",
			),
			'instance_settings' => array(
				'tid' => 0,
				'note' => "0",
			),
			'default_widget' => 'uneak_taxo_note_field',
			'default_formatter' => 'uneak_taxo_note_default',
			// Support hook_entity_property_info() from contrib "Entity API".
			'property_type' => 'field_item_uneak_taxo_note',
			'property_callbacks' => array('uneak_taxo_note_field_property_info_callback'),
		),
	);
}




/**
 * Implements hook_field_instance_settings_form().
 */
function uneak_taxo_note_field_instance_settings_form($field, $instance) {
	$form = array(
		'#element_validate' => array('uneak_taxo_note_field_settings_form_validate'),
	);
	
	$v = "technologie";
	$term_select = array();
	$term_select[0] = t("Selectionnez une technologie");
	
	$vocab = taxonomy_vocabulary_machine_name_load($v);
	$terms = taxonomy_get_tree($vocab->vid, 0);
	foreach($terms as $term) {
		$term_select[$term->tid] = $term->name;
	}
	
	$form['tid'] = array(
		'#type' => 'select',
		'#title' => t('Taxonomy'),
		'#default_value' => isset($instance['settings']['tid']) ? intval($instance['settings']['tid']) : 0,
		'#options' => $term_select,
		
	);
	
	$form['note'] = array(
		'#type' => 'textfield',
		'#title' => t('Note'),
		'#default_value' => isset($instance['settings']['note']) ? intval($instance['settings']['note']) : "0",
		'#field_suffix' => ' / 100',
		'#maxlength' => 3,
    	'#size' => 3,
	);
	
	
	return $form;
}



/**
 * Implement hook_field_is_empty().
 */
function uneak_taxo_note_field_is_empty($item, $field) {
	return (empty($item['note']) && intval($item['note']) == 0 && intval($item['tid']) == 0);
}



/**
 * Validate the field settings form.
 */
function uneak_taxo_note_field_settings_form_validate($element, &$form_state, $complete_form) {
	if (empty($form_state['values']['instance']['settings']['note']) || intval($form_state['values']['instance']['settings']['note']) == 0) {
		form_set_error('Note', t('A note must be provided.'));
	} elseif ($form_state['values']['instance']['settings']['note'] > 100) {
		form_set_error('Note', t('The note cannot be greater than 100.'));
  	} elseif ($form_state['values']['instance']['settings']['note'] < 0) {
		form_set_error('Note', t('The note cannot be negative.'));
  	}
	
	if (intval($form_state['values']['instance']['settings']['tid']) == 0) {
		form_set_error('Note', t('A taxonomy must be provided.'));
	}
}




/**
 * Implements hook_field_validate().
 */
function uneak_taxo_note_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
	if (intval($instance['settings']['note']) == 0) {
		form_set_error($field['field_name'] .'][0][note', t('At least one note must be entered.'));
	}
	if (intval($instance['settings']['tid']) == 0) {
		form_set_error($field['field_name'] .'][0][tid', t('At least one note must be entered.'));
	}
}


/**
 * Implements hook_field_widget_info().
 */
function uneak_taxo_note_field_widget_info() {
  return array(
    'uneak_taxo_note_field' => array(
      'label' => 'Uneak Taxonomy note',
      'field types' => array('uneak_taxo_note_field'),
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
    ),
  );
}


/**
 * Implements hook_field_widget_form().
 */
 
function uneak_taxo_note_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
	$element += array(
	'#type' => $instance['widget']['type'],
	'#default_value' => isset($items[$delta]) ? $items[$delta] : '',
	);
	return $element;
}





/**
 * Implements hook_theme().
 */
function uneak_taxo_note_theme() {
  return array(
    'formatter_uneak_taxo_note_default' => array(
      'variables' => array('element' => NULL),
    ),
	'uneak_taxo_note_field' => array(
      'render element' => 'element',
    ),
  );
}




/**
 * FAPI theme for an individual text elements.
 */
function theme_uneak_taxo_note_field($vars) {
	$element = $vars['element'];
	
	$output = '';
	$output .= '<div class="uneak-taxo-note-field-subrow clearfix">';
	if (isset($element['tid'])) { $output .= '<div class="uneak-taxo-note-field-tid uneak-taxo-note-field-column">'. drupal_render($element['tid']) .'</div>'; }
	if (isset($element['note'])) { $output .= '<div class="uneak-taxo-note-field-note uneak-taxo-note-field-column">'. drupal_render($element['note']) .'</div>'; }
	$output .= '</div>';
	
	return $output;
}


/**
 * Implements hook_element_info().
 */
function uneak_taxo_note_element_info() {
	$elements = array();
	$elements['uneak_taxo_note_field'] =  array(
		'#input' => TRUE,
		'#process' => array('uneak_taxo_note_field_process'),
		'#theme' => 'uneak_taxo_note_field',
		'#theme_wrappers' => array('form_element'),
	);
	return $elements;
}



/**
 * Process the link type element before displaying the field.
 *
 * Build the form element. When creating a form using FAPI #process,
 * note that $element['#value'] is already set.
 *
 * The $fields array is in $complete_form['#field_info'][$element['#field_name']].
 */
function uneak_taxo_note_field_process($element, $form_state, $complete_form) {
	$instance = field_widget_instance($element, $form_state);
	$settings = $instance['settings'];

	$v = "technologie";
	$term_select = array();
	$term_select[0] = t("Selectionnez une technologie");
	
	$vocab = taxonomy_vocabulary_machine_name_load($v);
	$terms = taxonomy_get_tree($vocab->vid, 0);
	foreach($terms as $term) {
		$term_select[$term->tid] = $term->name;
	}

	$element['tid'] = array(
		'#type' => 'select',
		'#title' => t('Taxonomy'),
		'#options' => $term_select,
		'#required' => TRUE,
		'#default_value' => isset($element['#value']['tid']) ? intval($element['#value']['tid']) : 0,
	);

	$element['note'] = array(
		'#type' => 'textfield',
		'#title' => t('Note'),
		'#required' => TRUE,
		'#default_value' => isset($element['#value']['note']) ? intval($element['#value']['note']) : "0",
		'#field_suffix' => ' / 100',
		'#maxlength' => 3,
    	'#size' => 3,
	);
	
	

	// To prevent an extra required indicator, disable the required flag on the
	// base element since all the sub-fields are already required if desired.
	$element['#required'] = FALSE;
	
	return $element;
}




/**
 * Implementation of hook_field_formatter_info().
 */
function uneak_taxo_note_field_formatter_info() {
	return array(
		'uneak_taxo_note_default' => array(
			'label' => t('Uneak Taxonomy Note'),
			'field types' => array('uneak_taxo_note_field'),
			'multiple values' => FIELD_BEHAVIOR_DEFAULT,
		),
	
	);
}





/**
 * Implements hook_field_formatter_view().
 */
function uneak_taxo_note_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $elements = array();
  foreach ($items as $delta => $item) {
    $elements[$delta] = array(
      '#markup' => theme('formatter_'.$display['type'], array('element' => $item, 'field' => $instance)),
    );
  }
  return $elements;
}




/**
 * Theme function for 'default' text field formatter.
 */
function theme_formatter_uneak_taxo_note_default($vars) {
	$element = $vars['element'];
	
	
	if (isset($element['note']) && intval($element['note']) != 0) {
		$fivestar = array(
			'#attributes' => array (
				"class" => array("clearfix", "fivestar-user-stars"),
			),
			'#theme' => "fivestar_formatter_default",
			'#rating' => $element['note'],
			'#instance_settings' => array(
				"stars" => 10,
				"allow_clear" => 0,
				"target" => "none",
			),
			'#display_settings' => array(
				"widget" => array("fivestar_widget" => "sites/all/modules/fivestar/widgets/minimal/minimal.css"),
				"style" => "user",
				"text" => "none",
				"expose" => 1,
			),
			'#widget' => array(
				"name" => "minimal",
				"css" => "sites/all/modules/fivestar/widgets/minimal/minimal.css",
			),
			'#description' => "",
		);
	}
	
	if (isset($element['tid']) && intval($element['tid']) != 0) {
		$term_path = "";
		$term = taxonomy_term_load(intval($element['tid']));
		$parent_tids = taxonomy_get_parents($term->tid);
		foreach ($parent_tids as $p_tid) {
			$term_path .= $p_tid->name.' / ';
		}
		$term_path .= $term->name;
	}
	
	$output = '';
	if (isset($term) || isset($fivestar)) {
		$output .= '<div class="uneak-taxo-note clearfix">';
		if (isset($term->field_image['und'])) $output .= '<img class="uneak-taxo-note-note uneak-taxo-image" src="'.image_style_url('taxonomy_icon_mini', $term->field_image['und'][0]['uri']).'" alt="'.$term->name.'" />';
		if (isset($term_path)) $output .= '<h3 class="uneak-taxo-note-note uneak-taxo-title">'. $term_path .'</h3>';
		if (isset($fivestar)) $output .= '<div class="uneak-taxo-note-note uneak-taxo-note">'. drupal_render($fivestar) .'</div>';
		if (isset($term->description)) $output .= '<div class="uneak-taxo-note-tid uneak-taxo-description">'.$term->description.'</div>';
		$output .= '</div>';
	}
	
	return $output;
}



/**
 * Implements hook_field_settings_form().
 */
function uneak_taxo_note_field_settings_form() {
  return array();
}




/**
 * Additional callback to adapt the property info of uneak_taxo_note fields.
 * @see entity_metadata_field_entity_property_info().
 */
function uneak_taxo_note_field_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
	$property = &$info[$entity_type]['bundles'][$instance['bundle']]['properties'][$field['field_name']];

	$property['getter callback'] = 'entity_metadata_field_verbatim_get';
	$property['setter callback'] = 'entity_metadata_field_verbatim_set';
	
	$property['auto creation'] = '_uneak_taxo_note_field_item_create';

	$property['property info'] = array(
		'tid' => array(
			'type' => 'int',
			'label' => t('Taxonomy'),
			'setter callback' => 'entity_property_verbatim_set',
			'required' => TRUE,
		),
		'note' => array(
			'type' => 'int',
			'label' => t('Note'),
			'setter callback' => 'entity_property_verbatim_set',
			'required' => TRUE,
		)
	);
 
	unset($property['query callback']);
}





/**
 * Callback for creating a new, empty field item.
 *
 * @see link_field_property_info_callback()
 */
function _uneak_taxo_note_field_item_create() {
  return array('tid' => 0, 'note' => 0);
}


