
/*
 *	UNEAK (c)
 *	Marc Galoyer
 */

(function($) {
	
	
	var methods = {
		
		init : function( options ) {
			
			return this.each(function(){
				var $this = $(this);
				
				$this.data({
					"onOver-Callback":function(){},
					"onOut-Callback":function(){}
				});
				
				if (options) {
					$this.data("onOver-Callback", options.onOver);
					$this.data("onOut-Callback", options.onOut);
				}
				
				img_top_x = $this.width() - 1585;
				img_bottom_x = 0;
			
				$this.find('.image-clap-mask-top').css({backgroundPositionX: img_top_x});
				$this.find('.image-clap-mask-bottom').css({backgroundPositionX: img_bottom_x});
			
			
				$this.bind('mouseenter.imageClap', {thisScope:$this}, methods.mouseenter);
				$this.bind('mouseleave.imageClap', {thisScope:$this}, methods.mouseleave);
				$this.bind('click.imageClap', {thisScope:$this}, methods.click);
				
			});
		},
		
		
		destroy : function( ) {
			return this.each(function(){
				var $this = $(this);
				$this.unbind('.imageClap');
			});
		},
		
		
		click : function( event ) {
			var $this = event.data.thisScope;
			//window.open($this.find(".project_text a").attr('href'), '_self');
			return this;		
		},
		
		
		mouseenter : function( event ) {
			var $this = event.data.thisScope;
			
			img_top_x = $this.width() - 1585;
			img_bottom_x = 0;
			
			$this.find('img').stop().animate({"opacity": 0.5}, {duration: 2000, easing:"easeOutExpo"});
			$this.find('.image-clap-mask-top').stop().animate({backgroundPositionX: img_top_x + 200}, {duration: 500, easing:"easeOutExpo"});
			$this.find('.image-clap-mask-bottom').stop().animate({backgroundPositionX: img_bottom_x - 200}, {duration: 500, easing:"easeOutExpo"});
		
			$this.data("onOver-Callback").apply( $this );
		},
		
		mouseleave : function( event ) {
			var $this = event.data.thisScope;

			img_top_x = $this.width() - 1585;
			img_bottom_x = 0;
			
			$this.find('img').stop().animate({"opacity": 1}, {duration: 2000, easing:"easeOutExpo"});
			$this.find('.image-clap-mask-top').stop().animate({backgroundPositionX: img_top_x}, {duration: 500, easing:"easeOutExpo"});
			$this.find('.image-clap-mask-bottom').stop().animate({backgroundPositionX: img_bottom_x}, {duration: 500, easing:"easeOutExpo"});
	
			$this.data("onOut-Callback").apply( $this );
		},
		
		

	};
		
  
  
	$.fn.imageClap = function( method ) {
	
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.imageClap' );
		}    
	
	};
  
  
})(jQuery);